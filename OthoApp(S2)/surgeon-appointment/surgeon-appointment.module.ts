
/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 *            most part of the code here is the template from previous work
 */

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonAppointmentPage } from './surgeon-appointment';

@NgModule({
  declarations: [
    SurgeonAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonAppointmentPage),
  ],
})
export class SurgeonAppointmentPageModule {}