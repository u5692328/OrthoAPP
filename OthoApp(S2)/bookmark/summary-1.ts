import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Content} from 'ionic-angular';
import { MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-summary-1',
  templateUrl: 'summary-1.html',
  
})
export class Summary1Page {
  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Summary_1Page');
  }

  toYourHospital(){
    let test = document.getElementById("toYourHospital").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toMedicialTests(){
    let test = document.getElementById("toMedicialTests").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toTestBeforeSurgery(){
    let test = document.getElementById("toTestBeforeSurgery").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toPreparing(){
    let test = document.getElementById("toPreparing").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toEquipment(){
    let test = document.getElementById("toEquipment").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toSafeEnv(){
    let test = document.getElementById("toSafeEnv").offsetTop;
    this.content.scrollTo(0, test, 200);
  }
  toSupportAfterDischarge(){
    let test = document.getElementById("toSupportAfterDischarge").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toMinRisk(){
    let test = document.getElementById("toMinRisk").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

  toHosAdmin(){
    let test = document.getElementById("toHosAdmin").offsetTop;
    this.content.scrollTo(0, test, 200);
  }

}
