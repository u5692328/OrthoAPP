import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Summary1Page } from './summary-1';

@NgModule({
  declarations: [
    Summary1Page,
  ],
  imports: [
    IonicPageModule.forChild(Summary1Page),
  ],
})
export class Summary1Module {}
