## **OrthoApp Code Documentation** 


**1.components***

​	Every ts file should have a constructor with the following modular** 

​	`constructor(`

​	`public rest :RestProvider,`//retrieving data from firebase

​	`private afAuth: AngularFireAuth, `//get the current user id

​	`private afDatabase: AngularFireDatabase, `//firebase link

​	`public navCtrl: NavController, `//router within the app

​	`public navParams: NavParams`

​	`) {}`



​	**import the following links** 

​	`import { IonicPage, NavController, NavParams } from 'ionic-angular';`

​	`import {AngularFireAuth} from "angularfire2/auth";`

​	`import {RestProvider} from "../../providers/rest/rest";`



**2. read and write data to firebase**

​	//create a new record

​	`this.afDatabase.object( database saving path  ).set(   data need to be saved );`

​	//updating an existing record	

​	`this.afDatabase.object(  database saving path  ).update(   data need to be saved );`

​	//read data 

​	`var temp = this.afDatabase.database.ref( database path );`

​	`temp.on('value', function(data){localVariable = data.val()});`







