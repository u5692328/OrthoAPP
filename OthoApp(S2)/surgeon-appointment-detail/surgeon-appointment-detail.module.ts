import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonAppointmentDetailPage } from './surgeon-appointment-detail';

//reference: The basic code strucure is from ionic framework
//most part of the code here is the template from previous work

@NgModule({
  declarations: [
    SurgeonAppointmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonAppointmentDetailPage),
  ],
})
export class SurgeonAppointmentDetailPageModule {}
