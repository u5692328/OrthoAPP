import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import {AngularFireModule} from "angularfire2";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFireDatabaseModule} from "angularfire2/database";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {MenuPage} from "../pages/menu/menu";
import {TabsPage} from "../pages/tabs/tabs";
import {HomePage} from "../pages/home/home";

import {StepCounterPage} from "../pages/stepCounter/stepCounter";
import {ContactPage} from "../pages/painLevel/contact";
import {HttpModule} from "@angular/http";
import {BeforeSurgeryTabsPage} from "../pages/before-surgery-tabs/before-surgery-tabs";
import {FIREBASE_CONFIG} from "./app.firebase.config";
import {RegisterPage} from "../pages/register/register";
import {SurgeonMenuPage} from "../pages/surgeon-menu/surgeon-menu";
import {SurgeonHomePage} from "../pages/surgeon-home/surgeon-home";
import {SurgeonHomeTabsPage} from "../pages/surgeon-home-tabs/surgeon-home-tabs";
import {ChartPage} from "../pages/chart/chart";
import {LoadingPage} from "../pages/loading/loading";
import {ProfilePageModule} from "../pages/profile/profile.module";
import {TodoPage} from "../pages/todolist/todolist";
import {AfterSurgeryPage} from "../pages/after-surgery/after-surgery";
// import {InHospitalPage} from "../pages/in-hospital/in-hospital";

import {PipesModule} from "../pipes/pipes.module";
import {BeforeSurgeryPageModule} from "../pages/before-surgery/before-surgery.module";
import {TodoPageModule} from "../pages/todolist/todolist.module";
import { RestProvider } from '../providers/rest/rest';
import {InHospitalPageModule} from "../pages/in-hospital/in-hospital.module";
import {AfterSurgeryPageModule} from "../pages/after-surgery/after-surgery.module";
import {BeforeSurgeryPage} from "../pages/before-surgery/before-surgery";
import {NavSummaryModule} from "../pages/nav-summary/nav-summary.module";
import {NavSummaryDetailsModule} from "../pages/nav-summary-details/nav-summary-details.module";
import {NavSummaryPage} from "../pages/nav-summary/nav-summary";
import {NavSummaryDetailsPage} from "../pages/nav-summary-details/nav-summary-details";
import {SummaryModule} from "../pages/summary/summary.module";
import {SummaryPage} from "../pages/summary/summary";
import {Summary1Module} from "../pages/summary-1/summary-1.module";
import {Summary1Page} from "../pages/summary-1/summary-1";
import {Summary2Module} from "../pages/summary-2/summary-2.module";
import {Summary3Module} from "../pages/summary-3/summary-3.module";
import {Summary4Module} from "../pages/summary-4/summary-4.module";
import {Summary2Page} from "../pages/summary-2/summary-2";
import {Summary3Page} from "../pages/summary-3/summary-3";
import {Summary4Page} from "../pages/summary-4/summary-4";

//------------------------
import {AppointmentPageModule} from "../pages/appointment/appointment.module";
import {AppointmentPage} from "../pages/appointment/appointment";

import {MessageModule} from "../pages/message/message.module";
import {Message} from "../pages/message/message";

import {KneePageModule} from "../pages/knee/knee.module";
import {KneePage} from "../pages/knee/knee"; 

import { NgCalendarModule  } from 'ionic2-calendar';

import {SurgeonAppointmentPage} from "../pages/surgeon-appointment/surgeon-appointment";
import {SurgeonAppointmentPageModule} from "../pages/surgeon-appointment/surgeon-appointment.module";

import {AppointmentListPageModule} from "../pages/appointment_list/appointment_list.module";
import {AppointmentListPage} from "../pages/appointment_list/appointment_list"; 

import {SurgeonAppointmentDetailPage} from "../pages/surgeon-appointment-detail/surgeon-appointment-detail";
import {SurgeonAppointmentDetailPageModule} from "../pages/surgeon-appointment-detail/surgeon-appointment-detail.module";
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { MonthViewComponent } from 'ionic2-calendar/monthview';
import { WeekViewComponent } from 'ionic2-calendar/weekview';
import { DayViewComponent } from 'ionic2-calendar/dayview';
import {Info} from "../pages/info/info";
import {InfoModule} from "../pages/info/info.module";
import {Monitor} from "../pages/monitor/monitor";
import {MonitorModule} from "../pages/monitor/monitor.module";
import {StatisticsPage} from "../pages/statistics/statistics";


//------------------------

@NgModule({
  declarations: [
      MyApp,
      MenuPage,
      TabsPage,
      HomePage,
      ContactPage,
      StepCounterPage,
      BeforeSurgeryTabsPage,
      RegisterPage,
      SurgeonMenuPage,
      SurgeonHomePage,
      SurgeonHomeTabsPage,
      ChartPage,
      LoadingPage,
      StatisticsPage,

      //BeforeSurgeryPage
      // TodoPage,
      // AfterSurgeryPage,
      // InHospitalPage
  ],
  imports: [
      BrowserModule,
      IonicModule.forRoot(MyApp),
      HttpModule,
      AngularFireModule.initializeApp(FIREBASE_CONFIG),
      AngularFireAuthModule,
      AngularFireDatabaseModule,
      ProfilePageModule,
      PipesModule,
      BeforeSurgeryPageModule,
      TodoPageModule,
      InHospitalPageModule,
      AfterSurgeryPageModule,
      NavSummaryModule,
      NavSummaryDetailsModule,
      SummaryModule,
      Summary1Module,
      Summary2Module,
      Summary3Module,
      Summary4Module,
      //-----------------
      AppointmentPageModule,
      KneePageModule,
      NgCalendarModule,
      SurgeonAppointmentPageModule,
      AppointmentListPageModule,
      SurgeonAppointmentDetailPageModule,
      InfoModule,
      MonitorModule,
      MessageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      MenuPage,
      TabsPage,
      HomePage,
      ContactPage,
      StepCounterPage,
      BeforeSurgeryTabsPage,
      RegisterPage,
      SurgeonMenuPage,
      SurgeonHomePage,
      SurgeonHomeTabsPage,
      ChartPage,
      LoadingPage,
      TodoPage,
      AfterSurgeryPage,
      NavSummaryPage,
      NavSummaryDetailsPage,
      SummaryPage,
      Summary1Page,
      Summary2Page,
      Summary3Page,
      Summary4Page,
      // InHospitalPage
      //------------------------
      AppointmentPage,
      KneePage,
      SurgeonAppointmentPage,
      AppointmentListPage,
      SurgeonAppointmentDetailPage,
      Info,
      Monitor,
      StatisticsPage,
      Message,
  ],
  providers: [
      StatusBar,
      SplashScreen,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    //CustomEventTitleFormatterProvider,
    //CustomDateFormatterProvider,
    ]
})
export class AppModule {}
