import { Component } from '@angular/core';
import {MenuController, NavController, ToastController} from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";
import {Profile} from "../../model/profile";
import {User} from "../../model/user";
import {ProfilePage} from "../profile/profile";
import 'rxjs/add/operator/map'

import {TodoPage} from "../todolist/todolist";

import {API_ADDR} from "../../app/apiAddr";
import {RestProvider} from "../../providers/rest/rest";
import {LoginPage} from "../login/login";

import { App } from 'ionic-angular';
import {Summary2Page} from "../summary-2/summary-2";
import {Summary3Page} from "../summary-3/summary-3";
import {MenuPage} from "../menu/menu";
import {Summary1Page} from "../summary-1/summary-1";
import {SummaryPage} from "../summary/summary";
import {BeforeSurgeryPage} from "../before-surgery/before-surgery";
import {InHospitalPage} from "../in-hospital/in-hospital";
import {AfterSurgeryPage} from "../after-surgery/after-surgery";
import {StepCounterPage} from "../stepCounter/stepCounter";
import {ContactPage} from "../painLevel/contact";

//--------------------
import {AppointmentPage} from "../appointment/appointment";
import {KneePage} from "../knee/knee";
import {AppointmentListPage} from "../appointment_list/appointment_list";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  profileData: any;
  profile  = {} as Profile;
  user = {} as User;
  userid :any;
  data: any =null;
  URL : String = API_ADDR.firebaseAPI;
  subscribeData : any;

  constructor(public app:App,public rest: RestProvider,private afAuth:AngularFireAuth,private afDatabase: AngularFireDatabase,private toast:ToastController, public navCtrl: NavController, public menu: MenuController) {
      this.menu.enable(true);
  }

    //--------------------
    calendarPage(){
        this.navCtrl.push(AppointmentListPage);
    }

    kneePage(){
        this.navCtrl.push(KneePage);
    }


    //--------------------
    editProfile() {
        this.navCtrl.push(ProfilePage);
    }

    infosummary() {
      this.navCtrl.push(SummaryPage);
    }

    beforeSurgeryinfo() {
        this.navCtrl.push(Summary1Page);
    }

    beforeSurgeryvideo() {
      this.navCtrl.push(BeforeSurgeryPage);
    }

    inhospitalinfo() {
      this.navCtrl.push(Summary2Page);
    }

    inhospitalvideo() {
      this.navCtrl.push(InHospitalPage);
    }

    afterSurgeryinfo() {
        this.navCtrl.push(Summary3Page);
    }

    aftersurgeryvideo() {
      this.navCtrl.push(AfterSurgeryPage);
    }

    todoList() {
        this.navCtrl.push(TodoPage);
    }

    stepcounter() {
      this.navCtrl.push(StepCounterPage);
    }

    painlevel() {
      this.navCtrl.push(ContactPage);
    }


  ionViewDidLoad() {

      this.userid = this.afAuth.auth.currentUser.uid;
      if (this.rest.getProfileCache() == null) {
          this.afAuth.auth.currentUser.getIdToken(true).then(idToken => {
              this.subscribeData = this.rest.getData(this.userid, idToken, '/profile/user/').subscribe(data => {
                  this.profileData = data;
                  this.rest.setProfileCache(data);
              });
              this.rest.setToken(idToken);
          });
      }
      else {
          console.log('use cache');
          this.profileData = this.rest.getProfileCache();
      }
  }
    logout(){
      if(this.rest.getProfileCache() == null){
          this.subscribeData.unsubscribe();
      }else{
          this.rest.emptyProfileCache();
      }
      this.afAuth.auth.signOut().then(()=>this.app.getRootNav().setRoot(LoginPage));
  }

  showtoggle(){
      this.navCtrl.setRoot(MenuPage);
  }

}
