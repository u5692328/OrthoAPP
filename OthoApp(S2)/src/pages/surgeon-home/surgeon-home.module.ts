import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonHomePage } from './surgeon-home';

@NgModule({
  declarations: [
    SurgeonHomePage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonHomePage),
  ],
})
export class SurgeonHomePageModule {}
