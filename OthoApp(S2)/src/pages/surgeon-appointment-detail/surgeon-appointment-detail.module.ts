import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonAppointmentDetailPage } from './surgeon-appointment-detail';

//reference: The basic code strucure is from ionic framework

@NgModule({
  declarations: [
    SurgeonAppointmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonAppointmentDetailPage),
  ],
})
export class SurgeonAppointmentDetailPageModule {}
