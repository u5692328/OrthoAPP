import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import { ToastController } from 'ionic-angular';
import {AngularFireDatabase, FirebaseObjectObservable} from "angularfire2/database";
import {AppointmentRequest} from "../../model/appointment";
import {SurgeonAppointmentPage} from "../surgeon-appointment/surgeon-appointment";
import * as moment from 'moment';
import {Chart} from "chart.js";
/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 */

@IonicPage()
@Component({
  selector: 'page-SurgeonAppointmentDetail',
  templateUrl: 'surgeon-appointment-detail.html',
})
export class SurgeonAppointmentDetailPage {
  appointment = {} as AppointmentRequest;
  userid: any;

  constructor(public toastCtrl: ToastController, public rest :RestProvider,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {
    
    this.userid = this.navParams.get('userid');
    console.log("data: "  + this.userid);


  }

  ionViewDidLoad() {

    let temp = this.afDatabase.database.ref(`appointment_request/${this.userid}`);

    temp.on("value", people=>{
      let patientArray = [];
      people.forEach(user => {
        patientArray.push(user.val());
        return false;
      });
      console.log("people: " + JSON.stringify(patientArray));

      localStorage.setItem("name", patientArray[2]);
      localStorage.setItem("confirm", patientArray[0]);
      localStorage.setItem("description", patientArray[1]);
      localStorage.setItem("surgeon", patientArray[3]);
      localStorage.setItem("time", patientArray[4]);
      localStorage.setItem("title", patientArray[5]);

    });

    this.appointment.name = localStorage.getItem("name");
    this.appointment.confirm = localStorage.getItem("confirm");
    this.appointment.description = localStorage.getItem("description");
    this.appointment.surgeon = localStorage.getItem("surgeon");
    this.appointment.time = localStorage.getItem("time");
    this.appointment.title = localStorage.getItem("title");
    console.log("appointment: " + JSON.stringify(this.appointment));
  
  }

  confirmed(){

    let confirmed = "Yes";
    this.afDatabase.object(`appointment_request/${this.userid}/confirm`).set(confirmed);
    this.navCtrl.push(SurgeonAppointmentPage);

  }

  cancel(){
    let confirmed = "No";
    this.afDatabase.object(`appointment_request/${this.userid}/confirm`).set(confirmed);
    this.navCtrl.push(SurgeonAppointmentPage);
  }

  delete(){
    this.afDatabase.object(`appointment_request/${this.userid}`).remove();
    this.navCtrl.push(SurgeonAppointmentPage);
  }

  back(){
    this.navCtrl.push(SurgeonAppointmentPage);
  }
}