import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnaesthetistFaqPage } from './anaesthetist-faq';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    AnaesthetistFaqPage,
  ],
  imports: [
    IonicPageModule.forChild(AnaesthetistFaqPage),
    PipesModule
  ],
})
export class AnaesthetistFaqPageModule {}
