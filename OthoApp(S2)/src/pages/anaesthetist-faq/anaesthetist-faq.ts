import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AnaesthetistFaqPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-anaesthetist-faq',
  templateUrl: 'anaesthetist-faq.html',
})
export class AnaesthetistFaqPage {

    videos_faq: any[] = [

        {
            title: 'Pre operative anaesthetist FAQ',
            video: 'https://www.youtube.com/embed/w5KfGNH-70A?rel=0',
        },
    ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnaesthetistFaqPage');
  }

}
