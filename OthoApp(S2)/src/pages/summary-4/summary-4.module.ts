import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {Summary4Page} from "./summary-4";

@NgModule({
    declarations: [
        Summary4Page,
    ],
    imports: [
        IonicPageModule.forChild(Summary4Page),
    ],
})
export class Summary4Module {}
