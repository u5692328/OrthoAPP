import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-summary-4',
    templateUrl: 'summary-4.html',
})
export class Summary4Page {
    infoId = null;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.infoId = this.navParams.get('infoId');
    }

    goBack() {
        this.navCtrl.pop();
    }

}