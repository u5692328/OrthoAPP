import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Profile} from "../../model/profile";
import {AngularFireDatabase, FirebaseObjectObservable} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {SurgeonMenuPage} from "../surgeon-menu/surgeon-menu";
import {MenuPage} from "../menu/menu";
import { LoadingController } from 'ionic-angular';
import {ProfilePage} from "../profile/profile";
import {App} from "ionic-angular";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the LoadingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
})
export class LoadingPage {

    profile = {} as Profile;
    profileRef$: FirebaseObjectObservable<Profile>;
    subscribeData: any;

    constructor(public app:App,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController) {

        this.presentLoading();

    }

    presentLoading() {
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 1000
        });
        loader.present();
    }

    ionViewDidLoad(){
        let userID = this.afAuth.auth.currentUser;

        this.afAuth.authState.take(1).subscribe(auth => {
            this.afDatabase.object(`emails/${userID.uid}`).set(userID.email);
          })

        //upload token for notification
        let token = localStorage.getItem("token");

        this.afDatabase.object(`profile/user/${userID.uid}/token`).set(token);


        console.log(userID.uid);
        this.profileRef$ = this.afDatabase.object(`profile/user/${userID.uid}`);
        this.subscribeData = this.profileRef$.subscribe(data => {
            if ((data.firstName != null) && (data.lastName != null) && (data.Birthday != null)) {
                if (data.Status) {
                    this.navCtrl.setRoot(SurgeonMenuPage);
                }
                else {
                    this.navCtrl.setRoot(MenuPage).catch((e) => {
                        let error = e.errorMessage;
                        console.log(error);
                    });
                }
            }
            else {
                this.navCtrl.setRoot(ProfilePage).catch((e)=>{
                    console.log(e.message)
                })
            }
        })
    }
    ngOnDestroy() {
        this.subscribeData.unsubscribe();
    }

}
