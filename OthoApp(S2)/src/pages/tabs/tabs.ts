import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import {MenuController, NavParams} from "ionic-angular";
import {SummaryPage} from "../summary/summary";
import {Summary1Page} from "../summary-1/summary-1";
import {Summary2Page} from "../summary-2/summary-2";
import {Summary3Page} from "../summary-3/summary-3";
import {Summary4Page} from "../summary-4/summary-4";
import {Info} from "../info/info";
import {Monitor} from "../monitor/monitor";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  //tab2Root = StepCounterPage;
  //tab3Root = ContactPage;
  info = Info;
  monitor = Monitor;
  //tab4Root = SummaryPage;
  myIndex: number;

  constructor(navParams: NavParams, public menu: MenuController) {
    this.menu.enable(true);
    this.myIndex = 0;

  }
}
