import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonAppointmentPage } from './surgeon-appointment';

@NgModule({
  declarations: [
    SurgeonAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonAppointmentPage),
  ],
})
export class SurgeonAppointmentPageModule {}