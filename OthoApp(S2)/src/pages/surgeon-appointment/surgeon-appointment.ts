import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AngularFireDatabase, FirebaseObjectObservable} from "angularfire2/database";
import {Profile} from "../../model/profile";
import {AngularFireAuth} from "angularfire2/auth";
import {ContactPage} from "../painLevel/contact";
import {LoginPage} from "../login/login";
import { database } from '../../../node_modules/firebase/app';
import {SurgeonAppointmentDetailPage} from "../surgeon-appointment-detail/surgeon-appointment-detail"


@IonicPage()
@Component({
  selector: 'page-surgeon-appointment',
  templateUrl: 'surgeon-appointment.html',
})
export class SurgeonAppointmentPage {


  name:any;
  public userRef: any;
  public People:Array<any>;
  public UID: any;
  public loadedPatientList:Array<any>;
  profileRef$: FirebaseObjectObservable<Profile>;
  subscribeData : any;
  

  constructor(public app:App,private afAuth: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams,public afDatabase: AngularFireDatabase) {
  
    let currentUser =afAuth.auth.currentUser;
    let name;

    this.userRef = afDatabase.database.ref(`appointment_request`);
    
    this.profileRef$ = this.afDatabase.object(`profile/user/${currentUser.uid}`);

    this.subscribeData = this.profileRef$.subscribe(data=>{
        name = data.Name;

        console.log("data.name" + data.Name);

        this.userRef.on('value', People => {
            let patientArray = [];
            People.forEach( user => {
                if(user.val().surgeon == name) {
                    console.log(user.val());
                    patientArray.push(user.val());
                    return false;
                }

            });
            this.People = patientArray;
            this.loadedPatientList = patientArray;
            console.log("People: " + JSON.stringify(this.People));
            console.log("loadedPetientList: " + JSON.stringify(this.loadedPatientList));
        });
    });
  }

  ionViewDidLoad(){

    /** 
    let temp = this.afDatabase.database.ref(`appointment_request`);
    temp.on("value", data => {
        let patientArray = [];
        data.forEach(  user => {
          if(user.val().surgeon == this.name){
            patientArray.push(user.val());
            return false;
          }
        });

    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
    });

    console.log("people : " + this.People);

    */

  }

  initializeItems(): void {
    this.People = this.loadedPatientList;
}

  getItems(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();

    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;


    // if the value is an empty string don't filter the items
    if (!q) {
        return;
    }

    this.People = this.People.filter((v) => {
        if(q) {
            if (v.Name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                return true;
            }
            return false;
        }

    });

    console.log(q, this.People.length);

}


showProfile(patient){

    this.userRef.on('value', People => {
        let Id;
        People.forEach( user => {
            if(user.val().firstName == patient.firstName && user.val().lastName == patient.lastName && user.val().Birthday == patient.Birthday) {
                let key = user.key;
                Id = key;
                return false;
            }

        });
        this.UID = Id;
        this.navCtrl.push(SurgeonAppointmentDetailPage,{
            userid : this.UID
        });

    });

}

  logout(){
    this.afAuth.auth.signOut().then(()=>this.app.getRootNav().setRoot(LoginPage));
}

}
