import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-before-surgery',
  templateUrl: 'before-surgery.html',
})
export class BeforeSurgeryPage {

  videos: any[] = [
    {
      title: 'Meet your physiotherapist',
      video: 'https://www.youtube.com/embed/_5TMhzq-oQ4?rel=0',
    },
    {
      title: 'Pre operative Hip Exercises',
      video: 'https://www.youtube.com/embed/NpcV2WFpm-w?rel=0',
    },
    {
      title: 'Pre operative Knee Exercises',
      video: 'https://www.youtube.com/embed/CBlovyJy_BE?rel=0',
    },
  ]


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BeforeSurgeryPage');
  }

}
