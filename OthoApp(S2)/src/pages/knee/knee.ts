import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {AngularFireDatabase} from "angularfire2/database";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import { ToastController } from 'ionic-angular';
import {KneeCounter} from "../../model/knee";
import * as moment from 'moment';
import {Chart} from "chart.js";
import { IonicSwipeAllModule } from 'ionic-swipe-all';

/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 */

@IonicPage()
@Component({
  selector: 'page-knee',
  templateUrl: 'knee.html',
})
export class KneePage {
  @ViewChild('knees_barChart') barCanvas_knee;
  barChart_knee: any;
  knee_time: any;
  knees: any;
  user_knee:any;
  swipe: number = 0;

  images:string[] = Array("../../assets/image/0d.png", 
                          "../../assets/image/15d.png",
                          "../../assets/image/30d.png",
                          "../../assets/image/45d.png",
                          "../../assets/image/60d.png",
                          "../../assets/image/75d.png",
                          "../../assets/image/90d.png",
                          "../../assets/image/105d.png",
                          "../../assets/image/120d.png");
  pointer:number = 0;
  picToView:string="../../assets/image/0d.png";

  degree:number = 0;
  

  degreeUp:number = 0;
  degreeDown:number = 0;
  today: number;

  knee_counter = {} as KneeCounter;

  constructor(public toastCtrl: ToastController, public rest :RestProvider,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {
    let dateTime = new Date();
    dateTime.setHours(0);
    dateTime.setMinutes(0);
    dateTime.setSeconds(0);
    dateTime.setMilliseconds(0);
    const timestamp = Math.floor(dateTime.getTime() / 1000);
    this.today = timestamp;
  }

  swipeEvent(e) {
    this.swipe++
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad kneePage');

    /*
    this.afAuth.authState.take(1).subscribe(auth => {

      var retrieve= this.afDatabase.database.ref('knee/' + auth.uid);
      retrieve.on('value', function(data){
        console.log(" knee data: " + data.val());
        localStorage.setItem('degree', data.val());
      })
    })

    this.degree = parseInt(localStorage.getItem('degree')); 
    this.pointer = this.degree/15;

    console.log(this.pointer);

    this.picToView = this.images[this.pointer];

    */

    //---------------------------

    let userID = this.afAuth.auth.currentUser.uid;
    this.user_knee = this.afDatabase.database.ref(`knee/${userID}`);

    this.user_knee.on('value', data => {
        let timeArray = [];
        let kneeArray = [];
        data.forEach(data => {
            let key = data.key;
            let key_time = moment(key * 1000).format('DD/MM/YYYY');
            timeArray.push(key_time);
            kneeArray.push(data.val().downdegree - data.val().updegree);
            return false;
        });
        this.knee_time = timeArray;
        this.knees = kneeArray;
        this.barChart_knee = new Chart(this.barCanvas_knee.nativeElement,{
            type: 'bar',
            options: {
              title: {
                display: true,
                text: 'Motion Range'
            },
                legend: {
                    display: false
                },
                scales: {
                  yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        steps: 8,
                        stepValue: 15,
                        max: 120
                    }
                }]
                }
            },
            data: {
                datasets: [
                    {
                        fillColor : "rgba(220,220,220,0.5)",
                        strokeColor : "rgba(220,220,220,1)",
                        data: this.knees,
                    }
                ],
                labels: this.knee_time
            }
        });

    });
  }

  Up(){
    
    this.pointer--;
    this.degree-=15;
    if(this.pointer<0){
      this.pointer = 8;
      this.degree = 120;
    }
    this.picToView= this.images[this.pointer];
  }

  UpdegreeUp(){
    
    this.pointer--;
    this.degree-=15;
    this.degreeUp = this.degree;
    if(this.pointer<0){
      this.pointer = 8;
      this.degree = 120;
      this.degreeUp = this.degree;
    }
    this.picToView= this.images[this.pointer];
  }

  UpdegreeDown(){
    
    this.pointer--;
    this.degree-=15;
    this.degreeDown = this.degree
    if(this.pointer<0){
      this.pointer = 8;
      this.degree = 120;
      this.degreeDown = this.degree;
    }
    this.picToView= this.images[this.pointer];
  }

  Down(){

    this.pointer++;
    this.degree+=15;
    if(this.pointer >8){
      this.pointer = 0;
      this.degree = 0;
    }
    this.picToView= this.images[this.pointer];

  }

  DowndegreeUp(){

    this.pointer++;
    this.degree+=15;
    this.degreeUp = this.degree;
    if(this.pointer >8){
      this.pointer = 0;
      this.degree = 0;
      this.degreeUp = this.degree;
    }
    this.picToView= this.images[this.pointer];

  }

  DowndegreeDown(){

    this.pointer++;
    this.degree+=15;
    this.degreeDown = this.degree;
    if(this.pointer >8){
      this.pointer = 0;
      this.degree = 0;
      this.degreeDown = this.degree;
    }
    this.picToView= this.images[this.pointer];

  }

  upload(){

    this.afAuth.authState.take(1).subscribe(auth => {
      const date = this.today;
      this.knee_counter.updegree = this.degreeUp;
      this.knee_counter.downdegree = this.degreeDown;
      this.afDatabase.object(`knee/${auth.uid}/${date}`).set(this.knee_counter);
  });

    const toast = this.toastCtrl.create({
      message: 'Your data has been submited, thank you.',
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }


  uploadUp(){

    this.afAuth.authState.take(1).subscribe(auth => {
      const date = this.today;
      this.knee_counter.updegree = this.degreeUp;
      this.afDatabase.object(`knee/${auth.uid}/${date}`).update(this.knee_counter);
  });

    const toast = this.toastCtrl.create({
      message: 'Your data has been submited, thank you.',
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }


  uploadDown(){

    this.afAuth.authState.take(1).subscribe(auth => {
      const date = this.today;
      this.knee_counter.downdegree = this.degreeDown;
      this.afDatabase.object(`knee/${auth.uid}/${date}`).update(this.knee_counter);
  });

    const toast = this.toastCtrl.create({
      message: 'Your data has been submited, thank you.',
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }

  swipeUp(event: any): any {
    console.log('Swipe Up', event);
    this.Up();
}

swipeDown(event: any): any {
    console.log('Swipe Down', event);
    this.Down();
}
}
