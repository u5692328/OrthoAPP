import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KneePage } from './knee';
import { IonicSwipeAllModule } from 'ionic-swipe-all';

//reference: The basic code strucure is from ionic framework

@NgModule({
  declarations: [
    KneePage,
  ],
  imports: [
    IonicPageModule.forChild(KneePage),
    IonicSwipeAllModule,
  ],
})
export class KneePageModule {}
