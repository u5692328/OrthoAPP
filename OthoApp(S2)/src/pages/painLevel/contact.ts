import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {PainLevel} from "../../model/PainLevel";
import {AngularFireDatabase, FirebaseListObservable} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {Chart} from "chart.js"
import {Medication} from "../../model/Medication";
import * as moment from 'moment';
import {RestProvider} from "../../providers/rest/rest";
import {Exercise} from "../../model/exercise";
import { AlertController } from 'ionic-angular';
import {StepCounter} from "../../model/stepCounter";


@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html'
})
export class ContactPage {

    @ViewChild('painLevel') lineCanvas_pain;
    @ViewChild('medic') lineCanvas_medic;
    @ViewChild('steps_barChart') barCanvas_step;
    @ViewChild('exercise_chart') HistCanvas;
    @ViewChild('knees_barChart') barCanvas_knee;

    lineChart_pain: any;
    lineChart_medic: any;
    histChart_exercise:any;

    painlevel ={} as PainLevel;
    medication={} as Medication;
    // feedBack={} as Feedback;
    exercise={} as Exercise;
    step_counter = {} as StepCounter;

    Pain :FirebaseListObservable<any>;
    Medic :FirebaseListObservable<any>;
    Exe: FirebaseListObservable<any>;
    // FeedBack: FirebaseObjectObservable<any>;

    Time_exe : any;
    Time_pain: any;
    Time_medic:any;
    Time_on_chart: any;
    Time_on_chart_exe: any;

    userRef_pain : any;
    userRef_medic: any;
    userRef_exe:any;

    data    : any;
    paindata: any;
    medicdata: any;
    exedata :any;

    surgeonCheck : boolean;
    userID   :any;
    shownGroup = null;
    showMe = false;
    token : String;
    exercise_Name:any;
    today:any;

    barChart_step: any;
    barChart_knee: any;
    step_time: any;
    knee_time: any;
    steps: any;
    knees: any;
    user_step: any;
    user_knee: any;
    data_viewed:any = "7";
    time_format: string = 'DD/MM/YY HH:mm';

    period :any = 2;
    exercise_times = [0,1,2,3,4,5];





    constructor(private alertCtrl: AlertController,public rest:RestProvider,private afAuth:AngularFireAuth,private afDatabase:AngularFireDatabase,public navCtrl: NavController,public narParams :NavParams) {
        this.medication.Take = false;
        this.token = this.rest.getToken();
        this.data = this.narParams.get('userid');// get the user data passed from the surgeon page
        console.log("painLevel page" + this.data);

        let currentUser = this.afAuth.auth.currentUser;
        if (this.data == null) {                        // judge whether the current user is surgeon or patient
            this.userID = currentUser.uid;
            this.surgeonCheck = false;
        } else {
            this.userID = this.data;
            this.surgeonCheck = true;
        }
        console.log(this.userID);

        window.addEventListener('orientationchange',this.detectOrient);
        //16 Sep added setpCounter
        let dateTime = new Date();
        dateTime.setHours(0);
        dateTime.setMinutes(0);
        dateTime.setSeconds(0);
        dateTime.setMilliseconds(0);
        const timestamp = Math.floor(dateTime.getTime() / 1000);
        this.today = timestamp;
    }

    submitExercise(){
        //this.histChart_exercise.destroy();
        this.afAuth.authState.take(1).subscribe(auth=>
        {
            let dateTime = new Date();
            dateTime.setSeconds(0);
            dateTime.setMilliseconds(0);
            dateTime.setMinutes(0);
            dateTime.setHours(0);
            let timestamp = Math.floor(dateTime.getTime() / 1000);
            let date = timestamp;
            this.afDatabase.object(`Exercise/${this.userID}/${date}`).set(this.exercise);
        });


    }


    submit(){  

        //previous record
        /** 

        this.afAuth.authState.take(1).subscribe(auth =>{

            var temp = this.afDatabase.database.ref(`PainLevel/${auth.uid}`);
            temp.on('value', function(data){

                let timeArray = [];
                let painArray = [];
                data.forEach(data => {

                let key = data.key;
                let key_time = moment(parseInt(key) * 1000).format('DD/MM/YYYY');
                console.log("time:  "  + key_time);
                painArray.push(data.val().painLevel);
                timeArray.push(data.key);
                return false;
               })



               let dateTime = new Date();
               //dateTime.setSeconds(0);
               //dateTime.setMilliseconds(0);
               console.log(dateTime);
               let timestamp = Math.floor(dateTime.getTime() / 1000);
               //console.log("last time: " + timeArray[timeArray.length-1]);
               //console.log("current time: " + timestamp);


               let duration = timestamp - parseInt(timeArray[timeArray.length-2]);
               console.log("duration not formated : " + duration);


               if(painArray[painArray.length-1]>=7 && duration>7200){
                   localStorage.setItem("alert", "true");
               }else{
                localStorage.setItem("alert", "false");
               }
            })
        });

        if(localStorage.getItem("alert")==="true"){
            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: 'Please contact your surgeon immediately',
                buttons: ['close']
              });
              alert.present();
        }
        
        */
        
  
        // allow user to submit the painLevel and medicine
        //this.lineChart_pain.destroy();
        //this.lineChart_medic.destroy();
        this.afAuth.authState.take(1).subscribe(auth =>{
            let dateTime = new Date();
            //dateTime.setSeconds(0);
            //dateTime.setMilliseconds(0);
            console.log(dateTime);
            let timestamp = Math.floor(dateTime.getTime() / 1000);
            let date = timestamp;
            if (this.medication.medicalName == null){
                this.medication.medicalName = 'None';
            }
            this.afDatabase.object(`PainLevel/${auth.uid}/${date}`).set(this.painlevel);
            this.afDatabase.object(`Medication/${auth.uid}/${date}`).set(this.medication);//write data into firebase
        });
    }

    // uploadFeedback(){
    //     this.afAuth.authState.take(1).subscribe(auth=>{
    //         this.afDatabase.object(`Feedback/${this.data}`).set(this.feedBack).then(()=>alert("Uploaded !"));
    //     })
    // }

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    };
    isGroupShown(group) {
        return this.shownGroup === group;
    };

    hide() {
        if(this.showMe == true){
            this.showMe = false;
        }
        else{
            this.showMe = true;
        }
    }

    createChart(type){
        this.userRef_pain = this.afDatabase.database.ref(`PainLevel/${this.userID}`);
        this.userRef_medic = this.afDatabase.database.ref(`Medication/${this.userID}`);
        this.userRef_exe = this.afDatabase.database.ref(`Exercise/${this.userID}`);

        this.Pain = this.afDatabase.list(`PainLevel/${this.userID}`);
        this.Medic = this.afDatabase.list(`Medication/${this.userID}`);
        this.Exe = this.afDatabase.list(`Exercise/${this.userID}`);

        // this.FeedBack = this.afDatabase.object(`Feedback/${this.userID}`);

        this.userRef_pain.on('value', data => {                       //read data from firebase
            let Array = [];
            let paindata = [];
            let timeArray = [];
            let seven_arrary = [];
            let seven_pain=[];
            let seven_time=[];
            let thirty_arrary=[];
            let thirty_pain= [];
            let thirty_time= [];
            data.forEach(data => {

                let key = data.key;

                this.get_week_data(data,key,seven_arrary,seven_time,seven_pain,5);
                this.get_month_data(data,key,thirty_arrary,thirty_time,thirty_pain,5);

                let key_time = moment.unix(key).format(this.time_format);
                console.log(key_time);
                console.log(key * 1000);
                Array.push(key);
                timeArray.push(key_time);
                paindata.push(data.val().painLevel);
                return false;
            });

            if(type == "7"){
                this.Time_pain = seven_arrary;
                this.paindata = seven_pain;
                this.Time_on_chart = seven_time;
            }else if(type == "30"){
                this.Time_pain = thirty_arrary;
                this.paindata = thirty_pain;
                this.Time_on_chart = thirty_time;
            }else{
                this.Time_pain = Array;
                this.paindata = paindata;
                this.Time_on_chart = timeArray;
            }


            console.log("paindata =" + paindata);

            console.log("success here!");
            console.log(this.lineCanvas_pain);
            this.lineChart_pain = new Chart(this.lineCanvas_pain.nativeElement, {// show the data in the chat
                type: 'bar',
                options: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Pain Level',
                        fontSize: 18
                    },
                    scales:
                        {
                            xAxes:[{
                                display:this.detectmob() || this.detectOrient()
                            }]
                        ,
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 1,
                                    max: 10
                                }
                            }]
                        }
                },
                data: {
                    datasets: [
                        {
                            fillColor : "rgba(220,220,220,0.5)",
                            strokeColor : "rgba(220,220,220,1)",
                            data: this.paindata,
                        }
                    ],
                    labels: this.Time_on_chart

                }

            });

        });

        this.userRef_medic.on('value', data => {                       //read data from firebase
            let medicdata = [];
            let time = [];
            let seven_Array =[];
            let seven_Time=[];
            let thirty_Array=[];
            let thirty_Time=[];
            data.forEach(data => {
                let key = data.key;
                this.get_week_data(data,key,seven_Array,seven_Time,null,3);
                this.get_month_data(data,key,thirty_Array,thirty_Time,null,3);
                medicdata.push(data.val().Take);
                time.push(moment(key * 1000).format(this.time_format));
                return false;
            });
            if(type == "7"){
                this.Time_medic = seven_Time;
                this.medicdata = seven_Array;
            }else if(type == "30"){
                this.Time_medic = thirty_Time;
                this.medicdata = thirty_Array;
            }else{
                this.Time_medic = time;
                this.medicdata = medicdata;
            }

            console.log("success here!");
            console.log(this.lineCanvas_medic);
            this.lineChart_medic = new Chart(this.lineCanvas_medic.nativeElement, {           // show the data in the chat
                type: 'line',
                options:
                    {
                        legend: {
                            display: false
                        },
                        scales:
                            {
                                yAxes: [{
                                    ticks: {
                                        max: 1,
                                        min: 0,
                                        stepSize: 1
                                    }
                                }],
                                xAxes:[{
                                    display:this.detectmob() || this.detectOrient()
                                }]
                            },
                        title: {
                            display: true,
                            text: ' Medicine taken (1- yes, 0 - no)',
                            fontSize: 18
                        }
                    },
                data: {
                    labels: this.Time_medic,
                    datasets: [
                        {
                            backgroundColor: "rgba(75,192,192,0.4)",
                            borderColor: "rgba(75,192,192,1)",
                            pointBorderColor: "rgba(75,192,192,1)",
                            pointBackgroundColor: "#fff",
                            pointHoverBackgroundColor: "rgba(75,192,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            data: this.medicdata,
                            spanGaps: true
                        }
                    ]
                }
            });
        });
    }

    ionViewDidLoad() {

        this.createChart(this.data_viewed);
        this.loadExercise();

        this.userRef_pain.on('value',data=>{
            let dateTime = new Date();
            let timestamp = Math.floor(dateTime.getTime() / 1000);
            let date = timestamp;
            let pain =[];

            data.forEach(data => {
                let key = data.key;
                let hours = Math.floor(((date-key)*1000)/(3600));
                // console.log("minutes: "+hours);
                if((hours <= this.period) && (data.val().painLevel >= 7)){
                    console.log();
                    pain.push(data.val().painLevel);
                }

                return false;
            });

            let len = pain.length;
            console.log(len);
            console.log(pain);
            if ((pain[len -1] >=7) && (pain[len-2]>=7)){
                alert("Continue high pain Level");
            }
        });


        console.log(this.userID);
        if(this.surgeonCheck) {
            this.afDatabase.object(`Exercise/${this.userID}/${this.today}`).subscribe(data => {
                if (data) {
                    this.exercise = data;
                }
            });
        }
        // let array = [];
        // this.rest.getCONSTData(this.token, '/Medication_Name').subscribe(data =>{
        //     console.log(data);
        //     array.push(data);
        //     this.medication_Name = array;
        // });

        if(this.surgeonCheck) {
            this.user_step = this.afDatabase.database.ref(`Steps/${this.userID}`);

            this.user_step.on('value', data => {
                let timeArray = [];
                let stepArray = [];
                data.forEach(data => {
                    let key = data.key;
                    let key_time = moment(key * 1000).format(this.time_format);
                    timeArray.push(key_time);
                    stepArray.push(data.val().steps);
                    return false;
                });
                this.step_time = timeArray;
                this.steps = stepArray;
                this.barChart_step = new Chart(this.barCanvas_step.nativeElement, {
                    type: 'bar',
                    options: {
                        legend: {
                            display: false
                        }
                    },
                    data: {
                        datasets: [
                            {
                                fillColor: "rgba(220,220,220,0.5)",
                                strokeColor: "rgba(220,220,220,1)",
                                data: this.steps,
                            }
                        ],
                        labels: this.step_time
                    }
                });
            });
        };


        if(this.surgeonCheck) {
            this.user_knee = this.afDatabase.database.ref(`knee/${this.userID}`);

            this.user_knee.on('value', data => {
                let timeArray = [];
                let kneeArray = [];
                data.forEach(data => {
                    let key = data.key;
                    let key_time = moment(key * 1000).format(this.time_format);
                    timeArray.push(key_time);
                    kneeArray.push(data.val().downdegree - data.val().updegree);
                    return false;
                });
                this.knee_time = timeArray;
                this.knees = kneeArray;
                this.barChart_knee = new Chart(this.barCanvas_knee.nativeElement, {
                    type: 'bar',
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                              display: true,
                              ticks: {
                                  beginAtZero: true,
                                  steps: 8,
                                  stepValue: 15,
                                  max: 120
                              }
                          }]
                          }
                    },
                    data: {
                        datasets: [
                            {
                                fillColor: "rgba(220,220,220,0.5)",
                                strokeColor: "rgba(220,220,220,1)",
                                data: this.knees,
                            }
                        ],
                        labels: this.knee_time
                    }
                });
            });
        };
    }

    getWeek(day){
        let date = new Date();
        let today = date.getDay();
        let stepSunday = -today +1;
        if (today == 0){
            stepSunday = -7;
        }
        let stepMonday = 7 - today;
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        let time = date.getTime();

        let monday = Math.floor((time + stepSunday * 24 * 3600 * 1000)/1000);
        let sunday = Math.floor((time + stepMonday * 24 * 3600 * 1000)/1000);
        if(day == 'Start'){
            return monday;
        }else if (day == 'End'){
            return sunday;
        }else {
            return null;
        }
    }

    getMonth(month){
        let firstDate = new Date();
        firstDate.setDate(1);
        let endDate = new Date(firstDate);
        endDate.setMonth(firstDate.getMonth()+1);
        endDate.setDate(0);

        firstDate.setHours(0);
        firstDate.setMinutes(0);
        firstDate.setSeconds(0);
        firstDate.setMilliseconds(0);

        endDate.setHours(0);
        endDate.setMinutes(0);
        endDate.setSeconds(0);
        endDate.setMilliseconds(0);

        let start = firstDate.getTime();
        let end = endDate.getTime();

        start = Math.floor(start/1000);
        end = Math.floor(end/1000);

        if(month == "Start"){
            return start;
        }
        if(month == "End"){
            return end;
        }
    }

    seven_day_data(){
        let type = "7";
        this.data_viewed = type;
        if(this.lineChart_pain != null)
        //this.lineChart_pain.destroy();
        //this.lineChart_medic.destroy();
        this.createChart(type);
    }

    thirty_day_data(){
        let type = "30";
        this.data_viewed = type;
        //this.lineChart_pain.destroy();
        //this.lineChart_medic.destroy();
        this.createChart(type);
    }

    all_data(){
        let type = "all";
        this.data_viewed = type;
        //this.lineChart_pain.destroy();
        //this.lineChart_medic.destroy();
        this.createChart(type);
    }

    get_week_data(data,key,array,time,pain,num){
        if(num == 3) {
            if (key >= this.getWeek('Start') && key <= this.getWeek('End')) {
                array.push(data.val().Take);
                time.push(moment(key * 1000).format(this.time_format));
            }
        }
        if(num == 5){
                if(key >= this.getWeek('Start') &&  key <= this.getWeek('End')){
                    array.push(key);
                    time.push(moment(key * 1000).format(this.time_format));
                    pain.push(data.val().painLevel)
                }
        }
    }

    get_month_data(data,key,array,time,pain,num){
        if(num ==3 ) {
            if (key >= this.getMonth('Start') && key <= this.getMonth('End')) {
                array.push(data.val().Take);
                time.push(moment(key * 1000).format(this.time_format));
            }
        }
        if(num ==5) {
            if (key >= this.getMonth('Start') && key <= this.getMonth('End')) {
                array.push(key);
                time.push(moment(key * 1000).format(this.time_format));
                pain.push(data.val().painLevel)
            }
        }
    }
    detectmob() {
        if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ){
            return false;
        }
        else {
            return true;
        }
    }
    detectOrient(){
        if(window.innerHeight >= window.innerWidth) {
            return false;
        }else {

            return true;
        }
    }

    loadExercise(){
        this.userRef_exe.on('value', data => {                       //read data from firebase
            let Array =[];
            let time =[];
            // let Array = [0,0,0,0,0];
            // let H_post_c = 0;
            // let H_pre_c = 0;
            // let K_pre_c = 0;
            // let K_post_c = 0;
            // let Reco = 0;
            data.forEach(data => {
                let key = data.key;
                let val = data.val().Times;
                Array.push(val);
                time.push(moment(key * 1000).format('DD/MM/YYYY'));
                // if(val == this.exercise_Name[0]){
                //     H_post_c = H_post_c +1;
                //     Array[0] = H_post_c;
                // }else if(val == this.exercise_Name[1]){
                //     H_pre_c = H_pre_c +1;
                //     Array[1] = H_pre_c;
                // }else if(val == this.exercise_Name[2]){
                //     K_pre_c = K_pre_c +1;
                //     Array[2] = K_pre_c;
                // }else if(val == this.exercise_Name[3]){
                //     K_post_c = K_post_c + 1;
                //     Array[3] = K_post_c;
                // }else if(val == this.exercise_Name[4]){
                //     Reco = Reco +1;
                //     Array[4] = Reco;
                // }
                // console.log(Array);
                return false;
            });
            this.exedata = Array;

            this.histChart_exercise = new Chart(this.HistCanvas.nativeElement, {// show the data in the chat
                type: 'bar',
                options: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Exercises completed',
                        fontSize: 18
                    },
                    scales:
                        {
                            xAxes:[{
                                display: this.detectmob() || this.detectOrient()
                            }],
                            yAxes: [{
                                ticks: {
                                    stepSize: 1
                                }
                            }],
                        }
                },
                data: {
                    datasets: [
                        {
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.6)',
                                'rgba(255, 206, 86, 0.6)',
                                'rgba(75, 192, 192, 0.6)',
                                'rgba(153, 102, 255, 0.6)',
                                'rgba(255, 159, 64, 0.6)'
                            ],
                            data: this.exedata,
                        }
                    ],
                    labels: time
                }

            });

        });
    }

    change(bar){
        if(this.painlevel.painLevel >= 7 ){
            bar.color = "danger";
        } else if (this.painlevel.painLevel >= 4){
            bar.color = "lyellow";
        }else{
            bar.color = "secondary";
        }
        
    }

    trueCheck(){
        this.medication.Take = true;
    }

    falseCheck(){
        this.medication.Take = false;
    }



}
