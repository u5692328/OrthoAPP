import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {AngularFireDatabase} from "angularfire2/database";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import { ToastController } from 'ionic-angular';

import { ModalController, AlertController } from 'ionic-angular';
import moment from 'moment';
import { AppointmentRequest } from '../../model/appointment';
import {User} from "../../model/user";
import {AppointmentListPage} from "../appointment_list/appointment_list";
/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 * //reference: The calendar modular is from https://github.com/twinssbc/Ionic2-Calendar under MIT license
 */

@IonicPage()
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointment.html',
})
export class AppointmentPage {

  eventSource = [];
  viewTitle: string;
  selectedDay = new Date();

  appointment = {} as AppointmentRequest; 
  surgeon: string;
  userid: any;
 
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  constructor(private alertCtrl: AlertController, private modalCtrl: ModalController,public toastCtrl: ToastController, public rest :RestProvider,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {
  

  }

  ionViewDidLoad() {
    this.userid = this.afAuth.auth.currentUser.uid;
    let temp = this.afDatabase.database.ref(`profile/user/${this.userid}`);
    temp.on('value', function(data){
      
      localStorage.setItem('surgeon',data.val().Surgeon);
      localStorage.setItem('name',data.val().Name);
    });
    
    console.log('ionViewDidLoad kneePage');
  }

  addEvent() {
    let modal = this.modalCtrl.create('EventModalPage', {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;
 
        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);
        //eventData.title = eventData.title +  " | Waiting for submit";
 
        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];


        let start = moment(data.startTime).format('LLLL');
        let end = moment(data.endTime).format('LLLL');
    
        let time = 'Time: ' + start;
        // + ' To: ' + end;
        this.appointment.time = time;
        this.appointment.title = data.title;
        this.appointment.description = data.description;
        this.appointment.confirm = "No";
        this.appointment.surgeon = localStorage.getItem('surgeon');
        this.appointment.name = localStorage.getItem('name');
  
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }
 
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
 
  onEventSelected(event) {
    let start = moment(event.startTime).format('LLLL');
    let end = moment(event.endTime).format('LLLL');
    
    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'Time: ' + start,
      //+ '<br>To: ' + end,
      buttons: ['OK']
    })
    alert.present();

  }
 
  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }

  upload(){

    //var appointment = new Array();
    //appointment['title'] = this.title;
    //appointment['start'] = this.start;
    //appointment['end'] = this.end;

    console.log("event " + JSON.stringify(this.eventSource));
  
    

    console.log("appointment: " + JSON.stringify(this.appointment));


    this.afAuth.authState.take(1).subscribe(auth => {

      //this.afDatabase.object(`appointment_request/${auth.uid}`).set(appointment);
      this.afDatabase.object(`appointment_request/${auth.uid}`).set(this.appointment);
    })

    const toast = this.toastCtrl.create({
      message: 'Your data has been submited, thank you.',
      duration: 1000,
      position: 'top'
    });
    toast.present();

    this.navCtrl.push(AppointmentListPage);

  }



}
