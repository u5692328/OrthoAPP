import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentPage } from './appointment';
import { NgCalendarModule  } from 'ionic2-calendar';

//reference: The basic code strucure is from ionic framework
//reference: The calendar modular is from https://github.com/twinssbc/Ionic2-Calendar under MIT license

@NgModule({
  declarations: [
    AppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentPage),
    NgCalendarModule,
  ],
})
export class AppointmentPageModule {}
