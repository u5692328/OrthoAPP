import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ChartPage} from "../chart/chart";
import {SurgeonHomePage} from "../surgeon-home/surgeon-home";
import {SurgeonAppointmentPage} from "../surgeon-appointment/surgeon-appointment";

@IonicPage()
@Component({
  selector: 'page-surgeon-home-tabs',
  templateUrl: 'surgeon-home-tabs.html'
})
export class SurgeonHomeTabsPage {

  homeRoot = SurgeonHomePage;
  surgeon_appointment = SurgeonAppointmentPage
  chartRoot = ChartPage;
  myIndex: number;


    constructor(navParams: NavParams) {
        this.myIndex = 0;

    }

}
