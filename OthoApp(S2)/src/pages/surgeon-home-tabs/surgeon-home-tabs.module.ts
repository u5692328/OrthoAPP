import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonHomeTabsPage } from './surgeon-home-tabs';

@NgModule({
  declarations: [
    SurgeonHomeTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonHomeTabsPage),
  ]
})
export class SurgeonHomeTabsPageModule {}
