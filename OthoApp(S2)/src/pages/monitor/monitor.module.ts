import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Monitor } from './monitor';

//reference: The basic code strucure is from ionic framework

@NgModule({
  declarations: [
    Monitor,
  ],
  imports: [
    IonicPageModule.forChild(Monitor),
  ],
})
export class MonitorModule {}
