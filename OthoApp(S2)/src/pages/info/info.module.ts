import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Info } from './info';

//reference: The basic code strucure is from ionic framework

@NgModule({
  declarations: [
    Info,
  ],
  imports: [
    IonicPageModule.forChild(Info),
  ],
})
export class InfoModule {}
