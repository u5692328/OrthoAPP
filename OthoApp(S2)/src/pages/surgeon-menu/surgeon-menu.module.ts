import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeonMenuPage } from './surgeon-menu';

@NgModule({
  declarations: [
    SurgeonMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(SurgeonMenuPage),
  ],
})
export class SurgeonMenuPageModule {}
