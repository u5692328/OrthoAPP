import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfterSurgeryPage } from './after-surgery';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    AfterSurgeryPage,
  ],
  imports: [
    IonicPageModule.forChild(AfterSurgeryPage),
      PipesModule
  ],
})
export class AfterSurgeryPageModule {}
