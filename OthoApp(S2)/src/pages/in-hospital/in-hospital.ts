import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the InHospitalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-in-hospital',
  templateUrl: 'in-hospital.html',
})
export class InHospitalPage {

    videos_inhospital: any[] = [
        {
            title: 'Hospital Admission Process',
            video: 'https://www.youtube.com/embed/WRWZ4LALP_w?rel=0',

        },
    ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InHospitalPage');
  }

}
