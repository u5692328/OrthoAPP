import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the BeforeSurgeryTabsPage tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */

@IonicPage()
@Component({
  selector: 'page-before-surgery-tabs',
  templateUrl: 'before-surgery-tabs.html'
})
export class BeforeSurgeryTabsPage {

  videoRoot = 'BeforeSurgeryPage'
  docRoot = 'DocPage'


  constructor(public navCtrl: NavController) {}

}
