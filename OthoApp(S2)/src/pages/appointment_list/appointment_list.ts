import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {AngularFireDatabase} from "angularfire2/database";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import { ToastController } from 'ionic-angular';
import {KneeCounter} from "../../model/knee";
import * as moment from 'moment';
import {Chart} from "chart.js";
import {AppointmentPage} from "../appointment/appointment";
/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 */

@IonicPage()
@Component({
  selector: 'page-appointment-list',
  templateUrl: 'appointment_list.html',
})
export class AppointmentListPage {

  title: any;
  description: any;
  time: any;
  surgeon: any;
  confirm: any;


  constructor(public toastCtrl: ToastController, public rest :RestProvider,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {

  }
  

  ionViewDidLoad() {
    this.title = "No Appointment";
    this.description = "No Appointment Description";
    this.time = "No Appointment Session";


    let userID = this.afAuth.auth.currentUser.uid;
    var temp = this.afDatabase.database.ref(`appointment_request/${userID}`);

    temp.on('value', function(data){
      if(data.exists()){
        let appointmentData = data.val();
        localStorage.setItem('exist', 'true');
        localStorage.setItem('title', appointmentData.title);
        localStorage.setItem('description', appointmentData.description);
        localStorage.setItem('time', appointmentData.time);
        localStorage.setItem('surgeon', appointmentData.surgeon);
        localStorage.setItem('confirm', appointmentData.confirm);
      }

    });

    if(localStorage.getItem('exist') === 'true'){
      this.title = localStorage.getItem('title');
      this.description = localStorage.getItem('description');
      this.time = localStorage.getItem('time');
      this.surgeon = localStorage.getItem('surgeon');
      this.confirm = localStorage.getItem('confirm');
    }

  

  }
  /*

  email(){

     let email = {
      to: 'jonathanjy@outlook.com',
      cc: '',
      bcc: ['',''],
      attachments: [],
      subject: 'Cordova Icons',
      body: 'How are you? Nice greetings from Ortho',
      isHtml: true
    };
    
    // Send a text message using default options
    this.emailComposer.open(email);

  }
   */

  edit(){
    this.navCtrl.push(AppointmentPage);

  }
}