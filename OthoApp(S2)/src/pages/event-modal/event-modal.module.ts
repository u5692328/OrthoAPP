import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalPage } from './event-modal';
//reference: The calendar modular is from https://github.com/twinssbc/Ionic2-Calendar under MIT license

@NgModule({
  declarations: [
    EventModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalPage),
  ],
})
export class EventModalPageModule {}
