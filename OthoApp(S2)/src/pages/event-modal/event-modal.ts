//reference: The calendar modular is from https://github.com/twinssbc/Ionic2-Calendar under MIT license
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import * as moment from 'moment';
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";
import { ToastController } from 'ionic-angular';
import { ModalController, AlertController } from 'ionic-angular';
import { AppointmentRequest } from '../../model/appointment';
import {User} from "../../model/user";
import {AppointmentListPage} from "../appointment_list/appointment_list";

@IonicPage()
@Component({
    selector: 'page-event-modal',
    templateUrl: 'event-modal.html',
})
export class EventModalPage {



    //event = { startTime: new Date().toISOString()};
    event = {description: new String(), title: new String(), startTime: new Date().toISOString(), endTime: new Date().toISOString()};
    minDate = new Date().toISOString();
    appointment = {} as AppointmentRequest;


    constructor(private alertCtrl: AlertController,public toastCtrl: ToastController, private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, private navParams: NavParams, public viewCtrl: ViewController) {
        let preselectedDate = moment(this.navParams.get('selectedDay')).format();
        this.event.startTime = preselectedDate;
        this.event.endTime = preselectedDate;
        this.event.title = "";
        this.event.description = "";
    }

    cancel() {
        this.viewCtrl.dismiss();
    }

    save() {

        this.appointment.time = this.event.startTime;
        this.appointment.title = this.event.title;
        this.appointment.description = this.event.description;
        this.appointment.confirm = "No";
        //this.appointment.surgeon = localStorage.getItem('surgeon');
        //this.appointment.name = localStorage.getItem('name');


        this.afAuth.authState.take(1).subscribe(auth => {

            //this.afDatabase.object(`appointment_request/${auth.uid}`).set(appointment);
            this.afDatabase.object(`appointment_request/${auth.uid}`).set(this.appointment);
        })

        const toast = this.toastCtrl.create({
            message: 'Your data has been submited, thank you.',
            duration: 1000,
            position: 'top'
        });
        toast.present();

        //this.navCtrl.push(AppointmentListPage);

        this.viewCtrl.dismiss(this.event);
    }

}