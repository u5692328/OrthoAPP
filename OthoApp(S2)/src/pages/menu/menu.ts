import {Component, ViewChild} from '@angular/core';
import {IonicPage, MenuController, Nav, NavController, NavParams} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {HomePage} from "../home/home";
import {BeforeSurgeryTabsPage} from "../before-surgery-tabs/before-surgery-tabs";
import {NavSummaryPage} from "../nav-summary/nav-summary";
import {StepCounterPage} from "../stepCounter/stepCounter";
import {SummaryPage} from "../summary/summary";
import {BeforeSurgeryPage} from "../before-surgery/before-surgery";
import {ContactPage} from "../painLevel/contact";
import { AlertController } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";

export interface PageInterface{
    title: string;
    pageName?: any;
    tabComponent?:any;
    index?: number;
    icon: string;
}
@IonicPage()
@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html',
})
export class MenuPage {

    rootPage = TabsPage;

    @ViewChild(Nav) nav: Nav;

    pages: PageInterface[] =[
        {title: 'Home' , pageName: TabsPage,tabComponent:'HomePage',icon:'custom-home'},
        //{title: 'Before Surgery' , pageName: BeforeSurgeryTabsPage,tabComponent:'BeforeSurgeryPage',icon:'custom-videocamera'},

        //TODO: need an icon for summaries
        {title: 'Information Summary' , pageName: 'SummaryPage', tabComponent: 'SummaryPage', icon: 'custom-doc'},

        {title: 'Pre-surgery Exercises' , pageName: 'BeforeSurgeryPage',tabComponent:'BeforeSurgeryPage',icon:'custom-videocamera'},

        //before surgery documentation here
        {title: 'Before Surgery Info' , pageName: 'Summary1Page', tabComponent: 'Summary1Page', icon: 'custom-beforesurgeryinfo'},

        //Anaesthetist FAQ video
        {title: 'Anaesthetist FAQ' , pageName: 'AnaesthetistFaqPage',tabComponent:'AnaesthetistFaqPage', icon:'custom-videocamera'},

        {title: 'Hospital Admission' , pageName: 'InHospitalPage',tabComponent:'InHospitalPage', icon:'custom-videocamera'},

        //in hospital documentation here
        {title: 'In Hospital Info' , pageName: 'Summary2Page', tabComponent: 'Summary2Page', icon: 'custom-hospital'},

        {title: 'Post-surgery Exercises' , pageName: 'AfterSurgeryPage',tabComponent:'AfterSurgeryPage',icon:'custom-videocamera'},

        //after surgery documentation here
        {title: 'Going Home Info' , pageName: 'Summary3Page', tabComponent: 'Summary3Page', icon: 'custom-aftersurgery'},


        {title: 'Pre-surgery To Do List' , pageName: 'TodoPage',tabComponent:'TodoPage',icon:'custom-todo'},

        {title: 'Step Counter' , pageName: StepCounterPage,tabComponent:'StepCounterPage',icon:'custom-stepcounter'},

        {title: 'Pain Level' , pageName: ContactPage,tabComponent:'ContactPage',icon:'custom-painlevel'},

        //{title: 'Nav Summary (test)' , pageName: 'NavSummaryPage', tabComponent: 'NavSummaryPage', icon: 'custom-navsummary'}
        // {title: 'Personal Profile' , pageName: 'ProfilePage',tabComponent:'ProfilePage',icon:'list-box'}
        // {title: 'Log-out' , pageName: 'LogOutPage', tabComponent: 'LogOutPage', icon: 'custom-logout'},
    ];

    msgTitle : any;
    msgContent : any;

    constructor(private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public menu: MenuController) {
        this.menu.enable(true);
    }

    openPage(page: PageInterface) {
        let params = {};
        this.nav.setRoot(page.pageName, params);
    }

    isActive(page: PageInterface) {

        let childNav = this.nav.getActiveChildNav();

        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }

        if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
            return 'primary';
        }
        return;
    }

    ionViewDidLoad(){
        this.presentConfirm();

    }

    presentConfirm() {
        let userID = this.afAuth.auth.currentUser.uid;
        let retrieve = this.afDatabase.database.ref(`message/${userID}`);

        retrieve.on('value', data=>{
            localStorage.setItem("has", "false");
            
            if(data.val()!=null){
                localStorage.setItem("has", "true");
                console.log("data : " + data);

                localStorage.setItem("title", data.val().title);
                localStorage.setItem("content", data.val().content);
            }

        });


        if(localStorage.getItem("has")=="true"){

            let alert = this.alertCtrl.create({
                title: localStorage.getItem("title"),
                message: localStorage.getItem("content"),
                buttons: [
                  {
                    text: 'Delete',
                    role: 'cancel',
                    handler: () => {
      
                      let remove = this.afDatabase.database.ref(`message/${userID}`).remove();
      
                    }
                  },
                  {
                    text: 'Remind Me Next Time',
                    handler: () => {
                      
                    }
                  }
                ]
              });
              alert.present();
            }

        }



}
