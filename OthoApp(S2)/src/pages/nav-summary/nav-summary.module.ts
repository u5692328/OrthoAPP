import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavSummaryPage } from './nav-summary';

@NgModule({
  declarations: [
    NavSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(NavSummaryPage),
  ],
})
export class NavSummaryModule {}
