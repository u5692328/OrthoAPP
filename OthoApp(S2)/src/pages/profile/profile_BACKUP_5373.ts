import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {AngularFireDatabase} from "angularfire2/database";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})

export class ProfilePage {
    profile = {} as Profile;
    profileData: any;
    profileNotEmpty : boolean = false;
    surgeon_name:any;
    subsData: any;

    Day:any;
    Month:any;
    Year:any;

    constructor(public rest :RestProvider,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {

    }

    //     this.profileData.subscribe(data => {
    //         if (this.rest.getProfileCache() ==null) {
    //             if ((data.firstName != null) && (data.lastName != null) && (data.Birthday != null)) {
    //                 this.profile = data;
    //                 this.profileNotEmpty = true;
    //                 this.rest.setProfileCache(data);
    //             }
    //             else {
    //                 console.log("empty");
    //             }
    //         }else {
    //             this.profile = this.rest.getProfileCache()
    //         }
    //     });
    // }


    createProfile() {
        this.afAuth.authState.take(1).subscribe(auth => {
<<<<<<< HEAD
            if (this.profileNotEmpty&&this.checkMonthInValid(this.profile.SurgeryMonth)&&this.checkMonthInValid(this.profile.BirthMonth)){
                this.profile.Name = this.profile.firstName + " " + this.profile.lastName;
                this.afDatabase.object(`profile/user/${auth.uid}`).update(this.profile).then(()=>this.navCtrl.setRoot(MenuPage)).catch((e)=>console.log(e.message));
            }/*
            else {
=======
            if (this.profileNotEmpty&&this.checkMonthInValid(this.profile.SurgeryMonth)){
                this.profile.Name = this.profile.firstName + " " + this.profile.lastName;
                this.afDatabase.object(`profile/user/${auth.uid}`).update(this.profile).then(()=>this.navCtrl.setRoot(MenuPage)).catch((e)=>console.log(e.message));
            }
/*            else {
>>>>>>> 698ed6b75fd22b3cbdb00083aeba31396cebb8fb
                this.profile.Status = false;
                this.profile.Name = this.profile.firstName + " " + this.profile.lastName;
                this.afDatabase.object(`profile/user/${auth.uid}`).set(this.profile).then(()=>this.navCtrl.setRoot(MenuPage));
            }*/
        })


    }

    checkMonthInValid(mon){
<<<<<<< HEAD
        if (!(/\b(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\b/.test(mon))) {

                alert("please input the month in shortening\n e.g. Using Sep instead of 9 or September");
=======
        if (!(/[a-zA-Z]/.test(mon))) {
            alert("please input the month in correct format");
>>>>>>> 698ed6b75fd22b3cbdb00083aeba31396cebb8fb
            return false;
        }else
            return true;
    }

    ionViewDidLoad() {
        let userID = this.afAuth.auth.currentUser;
        console.log(userID.uid);
        console.log("user email is: " + userID.email);

<<<<<<< HEAD
=======

>>>>>>> 698ed6b75fd22b3cbdb00083aeba31396cebb8fb
        this.subsData = this.afDatabase.database.ref(`surgeonList/name`).on('value', data => {
            let array = [];
            data.forEach(data => {
                let key = data.key;
                array.push(key);
                return false;
            });
            this.surgeon_name = array;
        });

        // this.profileData = this.afDatabase.object(`profile/user/${userID.uid}`);

        // if (this.rest.getProfileCache() == null) {

            this.afAuth.auth.currentUser.getIdToken(true).then(idToken => {
                this.profileData = this.rest.getData(userID.uid, idToken, '/profile/user/').subscribe(data => {
                    if ((data != null)&&(data.firstName != null) && (data.lastName != null) && (data.BirthDay != null)&& (data.BirthMonth!= null)&& (data.BirthYear != null)) {
                        this.profile = data;
                        this.profileNotEmpty = true;
                        this.rest.setProfileCache(data);
                        console.log('data!!!!' + data);
                    }
                    else {
                        console.log("empty");
                    }
                });
            });
        // } else {
        //     this.profile = this.rest.getProfileCache();
        //     console.log(this.profile);
        //     console.log("get from cache");
        // }
    }

    ngOnDestroy() {
        this.profileData.unsubscribe();
    }
}
