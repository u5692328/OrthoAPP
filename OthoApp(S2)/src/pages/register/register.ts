import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from "../../model/user";
import {AngularFireAuth} from "angularfire2/auth";
import {LoginPage} from "../login/login";


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user={} as User;

  constructor(private afAuth: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {
  }

  async register(user: User){
    try{
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email,user.password).catch((error)=>{
          let errorMessage = error.message;
          alert(errorMessage);
        });
      const currentUser = this.afAuth.auth.currentUser;
      currentUser.sendEmailVerification();
      this.navCtrl.setRoot(LoginPage);
    }catch(e){
      console.error(e);
    }
  }
}
