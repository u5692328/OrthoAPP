import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Message} from './message';

//reference: The basic code strucure is from ionic framework

@NgModule({
  declarations: [
    Message,
  ],
  imports: [
    IonicPageModule.forChild(Message),
  ],
})
export class MessageModule {}
