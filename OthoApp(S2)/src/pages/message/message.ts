import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FirebaseObjectObservable} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {AngularFireDatabase} from "angularfire2/database";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import { ToastController } from 'ionic-angular';


/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class Message {
  ids : any;
  people : any;
  title : any;
  content : any;


  constructor(public toastCtrl: ToastController, public rest :RestProvider,private afAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, public afDatabase: AngularFireDatabase) {
    this.ids = navParams.get('data');
    this.people = navParams.get('people');
  }

  ionViewDidLoad() {

  }

  send(){

    for(let i=0;i< this.ids.length;i++ ){

      this.afAuth.authState.take(1).subscribe(auth => {
        var msg = {title:this.title, content:this.content};
        this.afDatabase.object(`message/${this.ids[i]}`).update(msg);
    });

    }

    const toast = this.toastCtrl.create({
      message: 'Message sent successfully',
      duration: 1000,
      position: 'top'
    });
    toast.present();

  }
}