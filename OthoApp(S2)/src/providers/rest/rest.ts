import { Injectable } from '@angular/core';
import { Http,Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {API_ADDR} from "../../app/apiAddr";

/*
  Generated class for the RestProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RestProvider {

  private apiURL = API_ADDR.firebaseAPI;
  private idToken: String;
  private cache: any = null;

  constructor(public http: Http) {
    console.log('Hello RestProvider Provider');
  }
    getData(uid,token,route){
            console.log(this.apiURL +route + uid + '.json?auth='+token);

            return this.http.get( this.apiURL +route + uid + '.json?auth='+token)
                    .map((res:Response)=> res.json());
                }

    setToken(token){
        this.idToken = token;
    }

    getToken(){
        return this.idToken;
    }

    setProfileCache(content){
        this.cache = content;
    }

    getProfileCache(){
        return this.cache;
    }

    emptyProfileCache(){
        this.cache = null;
    }


}
