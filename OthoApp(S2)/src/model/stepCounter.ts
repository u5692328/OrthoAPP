export interface StepCounter {
    date: number;
    steps: number;
}