export interface Profile{
  firstName : string;
  lastName  : string;
  Name      : string;
  // gender    : boolean;
  BirthDay  : any;
  BirthMonth  : any;
  BirthYear  : any;

    SurgeryDay: any;
  SurgeryMonth:any;
  SurgeryYear: any;

  Status    : boolean;
  Surgeon   : string;
  operationType : string;
  operationSide : string;
  PrimaryOrRevision : string;
}
