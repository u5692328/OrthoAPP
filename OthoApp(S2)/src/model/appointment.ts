export interface AppointmentRequest {
    title: String;
    patient: String;
    time: String;
    description: String;
    surgeon: String;
    confirm: String;
    name: String;
}