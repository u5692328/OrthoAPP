﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace orthoApp
{
    public class DataRefreshService : HostedService
    {
        private readonly PushService _pushService;

        public DataRefreshService(PushService pushService)
        {
            _pushService = pushService;
        }
        
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await _pushService.PushNotification(cancellationToken);

                //the program will run push notification service (PushService.cs) once a day
                await Task.Delay(TimeSpan.FromDays(1), cancellationToken);
            }
        }
    }
}