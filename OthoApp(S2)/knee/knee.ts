import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Profile} from "../../model/profile";
import {AngularFireDatabase} from "angularfire2/database";
import {MenuPage} from "../menu/menu";
import 'rxjs/add/operator/take';
import {RestProvider} from "../../providers/rest/rest";
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the Calendar page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * reference: The basic code strucure is from ionic framework
 */

@IonicPage()
@Component({
  selector: 'page-knee',
  templateUrl: 'knee.html',
})
export class KneePage {

  images:string[] = Array("../../assets/image/0d.png", 
                          "../../assets/image/15d.png",
                          "../../assets/image/30d.png",
                          "../../assets/image/45d.png",
                          "../../assets/image/60d.png",
                          "../../assets/image/75d.png",
                          "../../assets/image/90d.png",
                          "../../assets/image/105d.png",
                          "../../assets/image/120d.png");
  pointer:number = 0;
  picToView:string="../../assets/image/0d.png";

  degree:number = 0;

  constructor(public toastCtrl: ToastController, public rest :RestProvider,private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad kneePage');

    this.afAuth.authState.take(1).subscribe(auth => {

      var starCountRef = this.afDatabase.database.ref('knee/' + auth.uid);
      starCountRef.on('value', function(data){
        console.log("       ------" + data.val());
        localStorage.setItem('degree', data.val());
      })
    })

    this.degree = parseInt(localStorage.getItem('degree')); 
    this.pointer = this.degree/15;

    console.log(this.pointer);

    this.picToView = this.images[this.pointer];
  }

  Up(){
    
    this.pointer--;
    this.degree-=15;
    if(this.pointer<0){
      this.pointer = 8;
      this.degree = 120;
    }
    this.picToView= this.images[this.pointer];
  }

  Down(){

    this.pointer++;
    this.degree+=15;
    if(this.pointer >8){
      this.pointer = 0;
      this.degree = 0;
    }
    this.picToView= this.images[this.pointer];

  }

  upload(){

    this.afAuth.authState.take(1).subscribe(auth => {

      this.afDatabase.object(`knee/${auth.uid}`).set(this.degree);
    })

    const toast = this.toastCtrl.create({
      message: 'Your data has been submited, thank you.',
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }

}
