import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KneePage } from './knee';

//reference: The basic code strucure is from ionic framework

@NgModule({
  declarations: [
    KneePage,
  ],
  imports: [
    IonicPageModule.forChild(KneePage),
  ],
})
export class KneePageModule {}
