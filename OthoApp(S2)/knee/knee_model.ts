export interface KneeCounter {
    date: number;
    knee: number;
}