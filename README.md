# Reference: 

All the things below are from the previous team;
The work our team produced is available at OthoApp(S2) repo


# Project Background

Over one million Australians have hip or knee surgery each year. People’s experiences in hospital and after discharge home can be challenging and it is estimated that just under 
20, 000 of these people return to hospital unnecessarily each year. 


#  Client Vision and Objective

This project aims to further develop the web portal which currently has the following features: -
1.	pre and post-surgical information - what to expect, including a recorded Q&A with an anaesthetist
2.	video recorded prehabilitation and rehabilitation with a physiotherapist 
3.	capacity to record pain levels and pain medication use, and exercise 
4.	keeping a record of daily/ weekly weight, fluid intake and exercise

A large component of the project will involve working with the Digital Health Agency to create an interface with My Health Record (Australia’s national electronic health record). 





