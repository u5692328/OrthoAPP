# Reference: 

All the things below are from the previous team;
The work our team produced is available at OthoApp(S2) repo





# OrthoApp_S1_2018 #
*Enabling orthopaedic patients to better understand and be in control of their journey through the entire surgical process (ANU TechLauncher Project)*


Welcome to the Landing Page for OrthoApp (Semester 1 2018). This page will be your reference to explore OrthoApp (web portal), a project coordinated under the domain of ANU Techlauncher.

Our objective is to provide a tracebable and logically structured repository to ensure all viewers can easily gain simple and rapid access to relevant documentation.
For your convenience, all documentation and relevant files have been categorised into folders (accessable via the links below) with major headings.
 
 If anything is unclear or you require assistance to navigate the repository, please feel free to contact:
 Ali Bulbul - u5714795@anu.edu.au

# Quick Links
* [Prototype Link](https://orthoapp-4a103.firebaseapp.com/) To begin navigate OrthoApp, please use the following guest log-in credentials.
    Patient Portal Username: szh65536@gmail.com<br>
    Patient Portal Password: 123456 <br>
    Surgeon Portal Username: 383391349@qq.com <br>
    Surgeon Portal Password: 1234567 <br>
* [Audit Resources](https://drive.google.com/drive/folders/1aedLolEjhv8vxHhi8v6Fejl5dNCF4A97?ths=true)
* [Client Sourced Files](https://drive.google.com/drive/folders/1cFlAYrE_-bEjtPM3N504Fm7AFxRB1KSj?ths=true)
* [Meeting Minutes](https://drive.google.com/drive/folders/17aDBkTwuGLN4dSb3tsR-1H_knovw61rz?ths=true)
* [Project Deliverables](https://drive.google.com/drive/folders/1a9QYYLfFC4MmU8N-gKsdSoL9WgprbGnf?ths=true)
* [Project Handover] (https://drive.google.com/drive/folders/1mQ0IurQsmdatU8vDzG0kwodetdUoxexC)
* [Project Management](https://drive.google.com/drive/folders/1jhe_AauuFf81NBJWtLmDJEaXYRXe-WHP?ths=true)
* [Reflective Cycle](https://drive.google.com/drive/folders/1P0PN4yYQbSY4M5RtnyndvkETCoq6FrFw?ths=true)
* [Showcase Poster](https://drive.google.com/drive/folders/1XHZGLZHdhl8BnKkNelHdkLlHgMajHwXs?ths=true)
* [Stakeholders](https://drive.google.com/drive/folders/17KMBC3X4tSLRH9bBw1tVKAvji3fwo6WJ?ths=true)
* [Team Details](https://drive.google.com/drive/folders/1-CDmo0eEe1gl0qcHdaCUIDntiT3gKLvk?ths=true)
* [Technical Deliverables](https://drive.google.com/drive/folders/11FOeMDGoVw9Cn6M_qN29ZW08AlUrG4dN?ths=true)
 

## Our Team

  **The team members:**
  
    1. Ali Bulbul
    2. Kumar Gajmer
    3. Yuqiang Li
    4. Zehao Shi
    5. Haoran Yang

  **Client Focal Person:**
  
    Jane Desborough

  **Course Convener**
  
    Charles Gretton
    
  **Tutor**
  
    Richard Jones
 
                           
# GitLab Organisation

All corresponding documentation is organised in Google Drive. The links to key files are provided in relevant sections on this page (see above).
To ensure direct access to multiple folders, Google Drive was opted to allow for multiple collaborators to easily work on various documents concurrently. 

The technical codes are in GitLab repository under [Ortho](https://gitlab.cecs.anu.edu.au/u6462802/OrthoAPP_2018_S1/tree/master/Ortho) directory.

The repository from the pervious semester can be found here [OrthoApp_2017_S2](https://gitlab.cecs.anu.edu.au/u5693650/OrthoApp). The contents in the previous repository are provided for reference (archived). 
For the sake of convenience and clarity, select documents from the S2 2017 repository have been (and continue to be) updated to be included in this (S1 2018) repository. 
*N.B: This is also to differentiate the efforts and input contributed by team members from this current semester (S1 2018). Therefore, all revised deliverables and documentation are present in the current repository.

# Team Organisation and Project Management
Semester 2, 2017 saw an agile management approach adopted by the team to coordinate the project (namely the use of rotational leadership). 
In Semester 1, 2018 the addition of a new team member (Ali Bulbul - Systems Engineer) yielded a transistion from 'rotational leadership' to the establishment of set roles and responsibilities to focus on.
Tasks are assigned to a members related field of expertise - see Weekly Task Delegation Breakdown Document in 'Project Deliverables' and 'Meeting Minutes - Team Minutes Google Drive Folder' for 'Sprint Targets'.
Ali's background in Systems Engineering (specifically his experience with Project Management and Coordination) has provided the team 'clarity', 'structure' and the freedom for the rest of the team members (Computer Scientists and Software Engineers) to focus on tasks and problems specifically relating to their discipline.
This method of coordination aims to ensure the highest quality of work and productivity within the team. 

* [Team Roles and Responsibilities](https://drive.google.com/drive/folders/1-CDmo0eEe1gl0qcHdaCUIDntiT3gKLvk?ths=true)


The progress of the project, the deliverables and milestones achieved in S1 2018 are outlined in the following link:
* [Project Timeline](https://drive.google.com/drive/folders/1_dowvHRLhLwKmfB3ynAA3h-PIM6GhEbd?ths=true)

# Meetings
Meetings are held routinely (see link below for specific dates) with key stakeholders affiliated to the development of OrthoApp, namely the client (focal person of interest), Tutor, Surgeons and Patients (organised by the client).
On the critical decisions taken and the deliverables outlined and achived, the minutes of each particular meeting (in addition to the use of the Issue Tracker) serve as a reference log. 
Meeting minutes are categorised into three folders, (Client, Team, Tutor) and can be viewed via the following link.
* [Meeting Minutes](https://drive.google.com/drive/folders/17aDBkTwuGLN4dSb3tsR-1H_knovw61rz?ths=true)

# Communication Channel
Slack (software) is the primary means for team member and client-based communication with one another (namely for resource sharing and ensuring feedback on various project related matters are provided in a timely manner).

# Decision and Milestone Tracking
The Project Timeline, Issues Tracker and Task Register (Weekly Breakdown) all serve as key team references for the following reasons:
- Streamlined monitoring of 'all major issues' affecting the development of OrthoApp (Issue Tracker)
- [Issues] (https://gitlab.cecs.anu.edu.au/u6462802/OrthoAPP_2018_S1/boards)

- Outlines the key decisions made and corresponding actions-on' on a weekly basis (Task Register - Team member specific)
- Summary of milestones achieved to illustrate project growth over time (Project Timeline)

# Prototype Testing
Periodic feedback provided from all OrthoApp stakeholders regarding the useability, features and aesthetics of the web portal culminated in the launch of a user testing trial period. 
Note, correspondings modifications made from the feedback provided by said Stakeholders aimed to improve the standard of the product to a level which the client was 
happy to broadcast OrthoApp for real-world use (albeit to a small sample size of users). 

OrthoApp User Trial

Start Date: 1 May 18

# Project Handover
Moving into Semester 2, 2018 and beyond, 'Larger scale testing' (an increased trial sample size for both patients and surgeons) and systematic testing measures (using an existing Unit Testing Framework) to yield results for purposes of handover and analysis are both part of a series of further objectives to address.
The Semester 1, 2018 team will note these targets (mutually agreed with the client) in the 'Project Handover' documentation for the next incoming team to address and solve.
Further information can be found in the 'Project Handover' Google Folder (see below link).
* [Project Handover] (https://drive.google.com/drive/folders/1mQ0IurQsmdatU8vDzG0kwodetdUoxexC)

# Project Deliverables
The deliverables are divided into two major categories: Technical (Portal-specific) and Project-specific. Files can be accessed via the following links.
* [Project Deliverables](https://drive.google.com/drive/folders/1a9QYYLfFC4MmU8N-gKsdSoL9WgprbGnf?ths=true)
* [Technical Deliverables](https://drive.google.com/drive/folders/11FOeMDGoVw9Cn6M_qN29ZW08AlUrG4dN?ths=true)

# Audit 
A series of three presentations are coordinated and presented to summarise and outline key remarks pertaining to the evolution of OrthoApp over Semester 1, 2018.
Each audit serves to provide the audience a snapshot of the progress made at specific time instances whilst highlighting the next points of action (instilling a state of continutity and tracebaility for the viewer to be informed on OrthoApp's growth).
Presentation Slides can be found in the following link below.

* [Audit Resources](https://drive.google.com/drive/folders/1aedLolEjhv8vxHhi8v6Fejl5dNCF4A97?ths=true)





