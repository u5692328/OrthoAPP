This is the prototype of OrthoApp

## How to run

*You need to install node.js and ionic first*. Check tutorial here.[Ionic](http://ionicframework.com/getting-started/).

### Install ionic and move into the project folder

```bash
$ sudo npm install -g ionic cordova
$ cd ./OrthoApp/Ortho
```

### Then run it,

```bash
$ ionic serve --l
```

