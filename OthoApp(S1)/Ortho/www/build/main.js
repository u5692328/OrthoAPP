webpackJsonp([10],{

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AfterSurgeryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AfterSurgeryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AfterSurgeryPage = /** @class */ (function () {
    function AfterSurgeryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.videos_post = [
            {
                title: 'Post operative Hip Exercises',
                video: 'https://www.youtube.com/embed/UBPyDzvqTV8?rel=0',
            },
            {
                title: 'Post operative Knee Exercises',
                video: 'https://www.youtube.com/embed/D3YDXM0SQjI?rel=0',
            }
        ];
    }
    AfterSurgeryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AfterSurgeryPage');
    };
    AfterSurgeryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-after-surgery',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/after-surgery/after-surgery.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Post Surgery Exercises</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card *ngFor="let video of videos_post">\n    <div id="videonameafter">\n      <ion-card-header text-wrap>\n        {{video.title}}\n      </ion-card-header>\n    </div>\n\n    <ion-card-content>\n    <iframe width="100%" height="200" [src]="video.video | youtube" frameborder="0" allowfullscreen="true"></iframe>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/after-surgery/after-surgery.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], AfterSurgeryPage);
    return AfterSurgeryPage;
}());

//# sourceMappingURL=after-surgery.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__menu_menu__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_take__ = __webpack_require__(608);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_take___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_take__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfilePage = /** @class */ (function () {
    function ProfilePage(rest, afAuth, afDatabase, navCtrl, navParams) {
        this.rest = rest;
        this.afAuth = afAuth;
        this.afDatabase = afDatabase;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profile = {};
        this.profileNotEmpty = false;
    }
    //     this.profileData.subscribe(data => {
    //         if (this.rest.getProfileCache() ==null) {
    //             if ((data.firstName != null) && (data.lastName != null) && (data.Birthday != null)) {
    //                 this.profile = data;
    //                 this.profileNotEmpty = true;
    //                 this.rest.setProfileCache(data);
    //             }
    //             else {
    //                 console.log("empty");
    //             }
    //         }else {
    //             this.profile = this.rest.getProfileCache()
    //         }
    //     });
    // }
    ProfilePage.prototype.createProfile = function () {
        var _this = this;
        this.afAuth.authState.take(1).subscribe(function (auth) {
            if (_this.profileNotEmpty) {
                _this.profile.Name = _this.profile.firstName + " " + _this.profile.lastName;
                _this.afDatabase.object("profile/user/" + auth.uid).update(_this.profile).then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__menu_menu__["a" /* MenuPage */]); }).catch(function (e) { return console.log(e.message); });
            }
            else {
                _this.profile.Status = false;
                _this.profile.Name = _this.profile.firstName + " " + _this.profile.lastName;
                _this.afDatabase.object("profile/user/" + auth.uid).set(_this.profile).then(function () { return _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__menu_menu__["a" /* MenuPage */]); });
            }
        });
    };
    ProfilePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var userID = this.afAuth.auth.currentUser;
        console.log(userID.uid);
        this.subsData = this.afDatabase.database.ref("surgeonList/name").on('value', function (data) {
            var array = [];
            data.forEach(function (data) {
                var key = data.key;
                array.push(key);
                return false;
            });
            _this.surgeon_name = array;
        });
        // this.profileData = this.afDatabase.object(`profile/user/${userID.uid}`);
        // if (this.rest.getProfileCache() == null) {
        this.afAuth.auth.currentUser.getIdToken(true).then(function (idToken) {
            _this.profileData = _this.rest.getData(userID.uid, idToken, '/profile/user/').subscribe(function (data) {
                if ((data != null) && (data.firstName != null) && (data.lastName != null) && (data.Birthday != null)) {
                    _this.profile = data;
                    _this.profileNotEmpty = true;
                    _this.rest.setProfileCache(data);
                    console.log('data!!!!' + data);
                }
                else {
                    console.log("empty");
                }
            });
        });
        // } else {
        //     this.profile = this.rest.getProfileCache();
        //     console.log(this.profile);
        //     console.log("get from cache");
        // }
    };
    ProfilePage.prototype.ngOnDestroy = function () {
        this.profileData.unsubscribe();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/profile/profile.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button menuToggle>\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title> Profile </ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n    <ion-content>\n        <ion-item-divider><ion-list-header>Background information</ion-list-header></ion-item-divider>\n      <ion-item>\n        <ion-label floating>FirstName</ion-label>\n        <ion-input [(ngModel)]="profile.firstName" ></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>LastName</ion-label>\n        <ion-input [(ngModel)]="profile.lastName"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Date of birth</ion-label>\n        <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="profile.Birthday"></ion-datetime>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>Surgery Day</ion-label>\n        <ion-datetime displayFormat="DD/MM/YYYY"[(ngModel)]="profile.SurgeryDay"></ion-datetime>\n      </ion-item>\n\n\n        <ion-item-divider><ion-list-header>Surgery Details</ion-list-header></ion-item-divider>\n            <ion-item>\n            <ion-label> Surgeon </ion-label>\n            <ion-select [(ngModel)]="profile.Surgeon" required>\n                <ion-option *ngFor="let s_name of surgeon_name" [value]="s_name">{{s_name}}</ion-option>\n            </ion-select>\n            </ion-item>\n            <ion-item>\n                <ion-label> Operation Types</ion-label>\n                <ion-select [(ngModel)]="profile.operationType" required>\n                    <ion-option value="hip">Hip</ion-option>\n                    <ion-option value="knee">Knee</ion-option>\n                </ion-select>\n            </ion-item>\n            <ion-item>\n                <ion-label> Operation Side</ion-label>\n                <ion-select [(ngModel)]="profile.operationSide">\n                    <ion-option value="left">Left</ion-option>\n                    <ion-option value="right">Right</ion-option>\n                    <ion-option value="Both">Both</ion-option>\n                </ion-select>\n            </ion-item>\n            <ion-item>\n                <ion-label> Primary or Revision</ion-label>\n                <ion-select [(ngModel)]="profile.PrimaryOrRevision">\n                    <ion-option value="primary">Primary</ion-option>\n                    <ion-option value="Revision">Revision</ion-option>\n                </ion-select>\n            </ion-item>\n\n\n        <button ion-button clear block color="lyellow" (click)="createProfile()"><b> Submit </b></button>\n\n\n    </ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import {MenuPage} from "../menu/menu";
//import 'rxjs/add/operator/take';
//import {Profile} from "../../model/profile";
//import {SurgeonMenuPage} from "../surgeon-menu/surgeon-menu";
var TodoPage = /** @class */ (function () {
    function TodoPage(afAuth, afDatabase, navCtrl, navParams) {
        this.afAuth = afAuth;
        this.afDatabase = afDatabase;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.todolist = {};
    }
    TodoPage.prototype.createTODO = function () {
        var _this = this;
        this.afAuth.authState.take(1).subscribe(function (auth) {
            _this.afDatabase.object("TodoChecklist/" + auth.uid).set(_this.todolist).then(function () { return alert("uploaded"); });
        });
    };
    TodoPage.prototype.ngOnDestroy = function () {
        this.subscribeData.unsubscribe();
    };
    TodoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.afAuth.authState.take(1).subscribe(function (auth) {
            _this.listData = _this.afDatabase.object("TodoChecklist/" + auth.uid);
            _this.subscribeData = _this.listData.subscribe(function (data) {
                _this.todolist = data;
            });
        });
    };
    TodoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-todo',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/todolist/todolist.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button menuToggle>\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>To Do List</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n    <ion-content>\n        <div id="todolist">\n        <ion-grid>\n            <ion-item-divider>HIP & KNEE Surgery</ion-item-divider>\n            <ion-row>\n                    <ion-item text-wrap id="tdinfo">\n                        <ion-label><p>Information packed</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl1" ></ion-checkbox>\n\n                    </ion-item>\n\n            </ion-row>\n            <ion-row>\n                    <ion-item text-wrap>\n                        <ion-label><p>Clexane fact sheet</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl2" ></ion-checkbox>\n                    </ion-item>\n\n            </ion-row>\n            <ion-row>\n                <ion-item text-wrap>\n                    <ion-label><p>Triclosan wash</p></ion-label>\n                    <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl3"></ion-checkbox>\n                </ion-item>\n\n            </ion-row>\n            <ion-item-divider text-wrap><h2>Register for Joint Information Session:</h2></ion-item-divider>\n            <div id="todolistdate">\n            <ion-row>\n                <ion-col col-md-6>\n                    <ion-item>\n                        <ion-label floating>Date:</ion-label>\n                        <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="todolist.infoSessionDay"></ion-datetime>\n                    </ion-item>\n                </ion-col>\n                <ion-col col-md-6>\n                    <ion-item>\n                        <ion-label floating>Time: </ion-label>\n                        <ion-datetime displayFormat="hh:mm A" [(ngModel)]="todolist.infoSessionTime"></ion-datetime>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n            </div>\n            <ion-row>\n                    <ion-item text-wrap>\n                        <ion-label><p>Appointment for pre surgery physiotherapy to increase strength/conditioning</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl4"></ion-checkbox>\n\n                    </ion-item>\n\n            </ion-row>\n            <ion-row>\n                <ion-item text-wrap>\n                    <ion-label><p>Arrange equipment, including over-the-toilet-char, shower chair and crutches – physio can discuss this with you at your pre surgery review</p></ion-label>\n                    <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl5"></ion-checkbox>\n\n                </ion-item>\n\n        </ion-row>\n            <ion-row>\n                    <ion-item text-wrap>\n                        <ion-label><p>Chair / bed height checked</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl6"></ion-checkbox>\n\n                    </ion-item>\n\n            </ion-row>\n            <ion-item-divider text-wrap><h2>Aware of what to bring into hospital</h2></ion-item-divider>\n            <ion-row>\n                <ion-col>\n                    <ion-item text-wrap>\n                        <ion-label><p>Medications</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl7"></ion-checkbox>\n\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item text-wrap>\n                        <ion-label><p>Scans / X-rays</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl8"></ion-checkbox>\n\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item text-wrap>\n                        <ion-label><p>Clothing</p></ion-label>\n                        <ion-checkbox color="linegreen" [(ngModel)]="todolist.chkl9"></ion-checkbox>\n\n                    </ion-item>\n                </ion-col>\n\n            </ion-row>\n            <ion-item-divider>Surgery Day</ion-item-divider>\n            <div id="todolistdate2">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                    <ion-label floating>Date</ion-label>\n                    <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="todolist.postOpSurgeonDay"></ion-datetime>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n            </div>\n\n        </ion-grid>\n\n\n        <button ion-button clear block color="lyellow" (click)="createTODO()"><b>update</b></button>\n    </div>\n\n    </ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/todolist/todolist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], TodoPage);
    return TodoPage;
}());

//# sourceMappingURL=todolist.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Summary2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Summary2Page = /** @class */ (function () {
    //infoId = null;
    function Summary2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        //this.infoId = this.navParams.get('infoId');
    }
    Summary2Page.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    Summary2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-summary-2',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-2/summary-2.html"*/'<!--\n  Generated template for the SummaryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<style>\n    ion-card-header {color: royalblue;\n        font-size: 2.5rem;\n        font-weight: bold}\n    h1   {color: black;\n        font-size: 3.5rem;\n        font-weight: bold;}\n    h2   {color: royalblue;\n        font-size: 2.5rem;\n        font-weight: bold}\n    p    {font-size: 1.8rem;\n        line-height: 1.6}\n    ul {font-size: 1.8rem;\n        line-height: 1.6;}\n    div {\n        margin:auto;\n        max-width: 100rem;\n    }\n    .highlight {color: royalblue;\n        font-weight: bold}\n    .titleHighlight {\n        color: cadetblue;\n        font-weight: bold;\n\n    }\n    .bold      {font-weight: bold}\n    .centre    {display: block;\n        margin-left: auto;\n        margin-right: auto;\n    }\n</style>\n\n\n\n<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button menuToggle>\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>In Hospital - Info</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding style="font-family: SansSerif; background: ghostwhite">\n<div>\n\n    <h1>IN HOSPITAL PRE-OPERATION</h1>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Make good use of pain level and medication tracker of the app to keep track of your recovery!</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n    <br>\n    <h2>YOUR HOSPITAL JOURNEY WILL GO SOMETHING LIKE THIS</h2>\n    <img src="../assets/image/inhos1.png" class="centre">\n\n\n\n    <br>\n    <br>\n    <h2>ANAESTHETIST </h2>\n    <p>You will be seen by your Anaesthetist to discuss any concerns relating to your: </p>\n    <p>• <span class="highlight">Medical history</span> </p>\n    <p>• <span class="highlight">Anaesthetic</span> </p>\n    <p>• <span class="highlight">Pain control</span> </p>\n    <p>This is the best time to let the anaesthetist know if you have a preference for pain relief or have had problems\n        with certain medications in the past, as he/ she will write up your pain relieving medications while you are having the operation.</p>\n\n\n\n    <br>\n    <h2>TRIFLOW</h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• If you did not receive a triflow on admission to the ward, please ask for one.</p>\n        </ion-card-content>\n    </ion-card>\n    <img src="../assets/image/inhos2.png" class="centre">\n    <p>In admission or back in the ward you will be given a Triflow machine. This is designed to encourage effective\n        deep breathing and lung expansion. If you are not given this on admission to the ward please ask for one.\n        Practise using it before you go to theatre and use hourly once awake after surgery. </p>\n    <p>The goal with this aid is to float all 3 balls at the top of the columns for 4 seconds as you breathe in.\n        <span class="highlight">Repeat 4-5 times every hour</span> when awake.</p>\n\n\n\n    <br>\n    <h2>MONITORING AFTER SURGERY </h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• The use of <span class="highlight">ice packs</span> is very important. The application of an ice pack is\n                recommended for <span class="highlight">15 minutes every 2 hours</span> to the front and back of your knee.</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n\n    <br>\n    <h2>INTRAVENOUS DRIP (IV) </h2>\n    <p>An IV maintains venous fluid levels and provides access for antibiotics and other medications.\n        This usually remains in place for 2 days, or until: </p>\n    <p>• You are eating and drinking well, </p>\n    <p>• The antibiotics are stopped – this is usually for 1 to 2 days; for revision surgery this can be for up to 5 days; and/or </p>\n    <p>• Your haemoglobin blood level has been checked to ensure you do not need a blood transfusion. </p>\n\n\n\n    <br>\n    <h2>WOUND DRAIN </h2>\n    <p>A wound drain may be placed in the wound during surgery to drain blood from your wound.\n        This is usually removed the day after your operation. </p>\n\n\n\n    <br>\n    <h2>BLADDER CATHETER </h2>\n    <p>A catheter is inserted whilst in theatre and used after surgery to accurately measure your urine output.\n        The bladder catheter is usually in place for 1 to 2 days until you can get out of bed to go to the toilet.</p>\n\n\n\n    <br>\n    <h2>PAIN CONTROL </h2>\n    <img src="../assets/image/inhos3.png" class="centre">\n    <p>Pain management is important and may be administered in a variety of ways, such as: </p>\n    <p>• <span class="highlight">PCA</span> (Patient Control Analgesia) using drugs like Morphine or Fentanyl that are run through an IV when you press the control button. It is very important that only you press the control button as when you are sleepy from the pain medication you will stop pressing the button and this prevents overdosing; </p>\n    <p>• <span class="highlight">Femoral Nerve blocks</span> (local anaesthetic around the femoral nerve) - may be used as a once-only injection in surgery or as an ongoing infusion for 1 to 2 days; </p>\n    <p>• <span class="highlight">Epidural or Spinal</span> (local anaesthetic around the spinal nerves) - may be used as a once-only injection in surgery or as an ongoing infusion for 1 to 2 days; </p>\n    <p>• <span class="highlight">Intravenous (IV) or intramuscular (IMI) injections</span> - given when you ask for them; </p>\n    <p>• <span class="highlight">Oral medications</span> - either given regularly (such as paracetamol) or given when you ask for them.</p>\n\n\n\n    <br>\n    <h2>EQUIPMENT used following a hip replacement</h2>\n    <img src="../assets/image/inhos4.png" class="centre">\n    <p>• An <span class="highlight">abduction pillow</span> may be used for hip replacements to prevent crossing of the legs.</p>\n    <p>• A <span class="highlight">hip roller</span> may be used to assist you in rolling onto your side for the first 24 hours after surgery</p>\n\n\n\n    <br>\n    <h2>POSTOPERATIVE EMOTIONS </h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Bad feelings will pass, things will get better!</p>\n            <p>• Initially limit visitors to immediate family or carers and encourage friends to visit when you return home.</p>\n        </ion-card-content>\n    </ion-card>\n\n\n    <br>\n    <p>It is important to accept that you might feel quite incapacitated during the <span class="highlight">first two\n        to three postoperative days</span>. The anaesthetic and pain relieving medications can have a big effect on how\n        you are feeling. This is all normal. Some patients feel quite euphoric, others nauseated. Some patients can feel teary and inadequate. </p>\n    <p>It is important to know that <span class="highlight">these feelings will pass</span> and that\n        <span class="highlight">things will get better!</span> </p>\n    <p>Your hospitalisation is a process, not an end in itself! Keeping a positive attitude really makes a difference to your outcome!</p>\n\n\n\n    <br>\n    <h2>POSTOPERATIVE MANAGEMENT </h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Do not cross your legs, this <span class="highlight">will slow down</span> your blood flow.</p>\n            <p>• Do not have a pillow positioned under your knee after knee replacement surgery,\n                you <span class="highlight">will have trouble</span> straightening your knee</p>\n            <p>• Rate your pain from <span class="highlight">0 to 10</span>, 0 is no pain, 10 is the worst pain ever.</p>\n            <p>• Pain level is for staff to help you appropriately.</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n\n    <br>\n    <h2>OBSERVATIONS </h2>\n    <p>• Regular observation of your vital signs is normal. Your blood pressure, pulse rate and respiratory rate will be\n        monitored frequently following your surgery. </p>\n    <p>• Checking of dressings, drain output and equipment will also occur often. </p>\n\n\n\n    <br>\n    <h2>RATING OF PAIN LEVEL </h2>\n    <p>You will be asked to rate your pain level from <span class="highlight">0 to 10</span>: 0 being no pain and ten,\n        meaning that you are comparing your pain to the worst pain you have experienced in your life.</p>\n    <p>Each person’s pain is an <span class="highlight">individual experience</span> and can only be judged by that person.\n        By putting a number value on your pain level you are helping the staff to provide you with an appropriate form\n        and strength of pain relief. If this pain relief is not effective please let the staff know. </p>\n\n\n\n    <br>\n    <h2>MOVEMENT </h2>\n    <p>You will be encouraged to complete the following movements to improve your breathing and circulation: </p>\n    <p>• <span class="highlight">Regular deep breathing and coughing (DB&C)</span> and use of the Triflow. </p>\n    <p>• <span class="highlight">Ankle pump exercises</span> (moving the foot at the ankle backward and forwards) to\n        encourage blood flow through the calves, which prevents slowing of the blood through the veins and so reduces the chance of blood clots. </p>\n    <p>• <span class="highlight">Limb movement</span> (moving, bending and straightening your knees and hips).\n        It is important from the moment you wake up after surgery to start these exercises.</p>\n    <p>Please <span class="highlight">do not</span> have a pillow positioned under your knee after knee replacement surgery.\n        This may feel more comfortable but will mean that you will have trouble straightening your knee. If your heels\n        become sore ask for a folded up towel or mattress cover to be placed under your leg.</p>\n\n\n\n    <br>\n    <h2>ANTICOAGULANTS </h2>\n    <p>Due to the increased risk of Deep Vein Thrombosis(DVT) /Pulmonary Embolus (PE), on your discharge from hospital\n        you will be sent home with either <span class="highlight">a course of Clexane injections</span>, or\n        <span class="highlight">an oral medication for 2 to 4 weeks</span>. This will be dependent on your surgeons preference. </p>\n\n\n\n\n    <br>\n    <h1>DAY-BY-DAY PROGRESS</h1>\n\n    <br>\n    <h2>DAY 1</h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Light diet, no juice, no shower.</p>\n            <p>• Ice pack is recommended</p>\n            <p>• Checkups and reviews</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n    <br>\n    <p>• A <span class="highlight">light diet</span> is recommended. Due to its acidic content please\n        <span class="highlight">avoid drinking orange or tropical juice</span> the first day after surgery as it will\n        make you feel sick. It is fine to drink juice after the first day as long as you do not feel sick.</p>\n    <p>• A <span class="highlight">blood test</span> is taken to check if you need a blood transfusion. </p>\n    <p>• An <span class="highlight">X-ray</span> is taken to confirm the position of the joint replacement.\n        Alternatively some surgeons may request this on day 2. </p>\n    <p>• Remove <span class="highlight">wound drain</span>. </p>\n    <p>• Review by:</p>\n    <ul style="list-style-type:square">\n        <li>Physio: to start exercises and get you out of bed, at least to a standing position. Do not expect to feel well enough to have a shower on this day;</li>\n        <li>Surgeon;</li>\n        <li>Anaesthetist;</li>\n    </ul>\n\n\n    <p>• Regular <span class="highlight">paracetamol</span> (as ordered by your anaesthetist) is given to compliment the other pain relieving medications. </p>\n    <p>• Be sure to ask for <span class="highlight">ice packs</span> every 2 hours which should be applied for\n        <span class="highlight">10 to 15 minutes only</span>. Ice packs, particularly for knees, help reduce swelling and pain.</p>\n\n\n\n    <br>\n    <h2>DAY 2</h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Continue diet, <span class="highlight">drink fluids</span>, shower is possible</p>\n            <p>• Pain relief every 4 to 6 hours</p>\n            <p>• Ice packs</p>\n            <p>• Checkups and reviews</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n    <br>\n    <p>• Diet as tolerated; keep <span class="highlight">drinking fluids</span> to help avoid constipation! Many\n        anaesthetists now order regular laxatives from the day of your surgery. </p>\n    <p>• Check <span class="highlight">X-ray</span> to confirm the position of the joint replacement (for those not done on day 1) </p>\n    <p>• Removal of: </p>\n    <ul>\n        <li>Bladder catheter;</li>\n        <li>PCA and/ or</li>\n        <li>Femoral Nerve Block / Epidural / Spinal.</li>\n    </ul>\n    <p>• Be sure to ask for <span class="highlight">pain relief regularly</span>, which means every\n        <span class="highlight">4 to 6 hours</span>. If the nurses ask you if you need pain relief move your\n        leg and then tell them. Most patients have little pain when lying still. </p>\n    <p>• Continue to ask for <span class="highlight">ice packs every 2 hours</span> which should be applied for 10 to 15 minutes only. </p>\n\n    <p>• Review by: </p>\n    <ul>\n        <li>Physiotherapist;</li>\n        <li>Surgeon;</li>\n    </ul>\n    <p>• Short shower sitting down on a chair. </p>\n    <p>• Increased periods of sitting out of bed for 30 to 60 minutes at a time.</p>\n\n\n\n    <br>\n    <h2>DAY 3 ONWARDS </h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Increased independence</p>\n            <p>• Regular pain relief and ice packs</p>\n            <p>• Checkups and reviews</p>\n            <p>• Prevent constipation</p>\n        </ion-card-content>\n    </ion-card>\n\n\n    <br>\n    <p>• <span class="highlight">Increased independence</span> is your goal. This will be encouraged through: </p>\n    <ul style="list-style-type:square">\n        <li>Mobilisation;</li>\n        <li>Showering;</li>\n        <li>Sitting out of bed; and</li>\n        <li>Activities of daily living.</li>\n    </ul>\n\n    <p>Ensure you move, sit and lie down regularly during the day. Doing any of these activities for long periods will\n        cause stiffness or excessive tiredness. </p>\n    <p>• Ongoing <span class="highlight">education from staff</span> regarding your daily injection of Clexane. </p>\n    <p>• Continue to ask for <span class="highlight">regular pain relief and ice packs</span>. </p>\n    <p>• <span class="highlight">Prevent constipation</span>. It is important to remember that you may suffer from\n        constipation due to your decreased mobility and because of the pain relieving medications you are taking.\n        Let your nurse know if you have not had a bowel movement by this day and need some extra medications to help treat constipation. </p>\n    <p>• Physiotherapist review: </p>\n    <ul style="list-style-type:square">\n        <li>Stairs education and practice;</li>\n        <li>Hiring of equipment; and</li>\n        <li>Precautions reinforced.</li>\n    </ul>\n\n    <p>• <span class="highlight">Dressings are only changed as needed</span>. With knee replacements the dressing is\n        changed as your knee bending (flexion) increases. The dressing should always be put on with your knee bent as\n        much as you can manage. With hip replacements wound drainage is expected for 2 to 3 days. Dressing pads are\n        changed as required. The dressing will be changed again before your discharge to a waterproof dressing.</p>\n\n\n\n    <br>\n    <h2>END OF IN HOSPITAL INSTRUCTIONS</h2>\n    <p>Hope you are recovering well from the surgery, and the discharge date will be decided very soon, depending on the progress of your recovery.</p>\n\n</div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-2/summary-2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], Summary2Page);
    return Summary2Page;
}());

//# sourceMappingURL=summary-2.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Summary3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Summary3Page = /** @class */ (function () {
    function Summary3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.infoId = null;
        this.infoId = this.navParams.get('infoId');
    }
    Summary3Page.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    Summary3Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-summary-3',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-3/summary-3.html"*/'<!--\n  Generated template for the SummaryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<style>\n    ion-card-header {color: royalblue;\n        font-size: 2.5rem;\n        font-weight: bold}\n    h1   {color: black;\n        font-size: 3.5rem;\n        font-weight: bold;}\n    h2   {color: royalblue;\n        font-size: 2.5rem;\n        font-weight: bold}\n    p, ul    {font-size: 1.8rem;\n        line-height: 1.6}\n\n    div {\n        margin:auto;\n        max-width: 100rem;\n    }\n    .highlight {color: royalblue;\n        font-weight: bold}\n    .titleHighlight {\n        color: cadetblue;\n        font-weight: bold;\n\n    }\n    .bold      {font-weight: bold}\n    .centre    {display: block;\n        margin-left: auto;\n        margin-right: auto;\n    }\n</style>\n\n\n<ion-header>\n    <ion-navbar>\n        <ion-buttons left>\n            <button ion-button menuToggle>\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>Going Home - Info</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding style="font-family: SansSerif; background: ghostwhite">\n<div>\n\n\n\n    <h1>AFTER SURGERY - GOING HOME</h1>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Discharge date depends on many factors, be patient :)</p>\n            <p>• Essentially <span class="highlight">90 degrees</span> of knee bend, and other goals</p>\n            <p>• Bring your belongings, medication, Clexane pack and triflow</p>\n            <p>• Remember your <span class="highlight">next appointment, waterproof dressing</span> is kept until then.</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n\n    <br>\n    <h2>DISCHARGE </h2>\n    <p>Your discharge date will depend on many factors. Due to better discharge planning/education and advances in\n        technique/materials, patients are allowed earlier rehabilitation. Consequently, there are less postoperative\n        complications such as pressure sores or pneumonia. As a guide: </p>\n    <ul style="list-style-type:square">\n    <li>Discharge is usually on Day 5 after your joint replacement surgery; </li>\n    <li>Unicompartment (half) knee replacements usually go home earlier. </li>\n    <li>Revision surgery patients may need extra time. </li>\n    <li>Patients having both sides done at the same time may stay for <span class="highlight">up to 7 days</span>. </li>\n    </ul>\n\n\n    <br>\n    <h2>DISCHARGE GOALS </h2>\n    <p>Discharge is goal orientated. You can be discharged home when you are able to: </p>\n    <ul style="list-style-type:square">\n    <li>Get in and out of bed <span class="highlight">independently</span>; </li>\n        <li><span class="highlight">Shower</span> with minimal assistance; </li>\n    <li><span class="highlight">Walk up and down</span> the corridor using crutches or a wheelie frame; and </li>\n        <li>Manage to go up and down a few <span class="highlight">stairs</span>. </li></ul>\n    <p>Knee replacement patients need to have approximately <span class="highlight">90 degrees of knee bend</span>\n        before discharge. Working on knee straightening (extension) is equally important.</p>\n\n\n\n    <br>\n    <h2>THINGS TO REMEMBER </h2>\n    <p>Upon discharge remember to take: </p>\n    <ul style="list-style-type:square">\n    <li>Your personal belongings (including X-rays); </li>\n    <li>Pain relieving medications or a script; </li>\n    <li>Your Clexane pack; </li>\n    <li>Your Triflow breathing device; Ice gel inserts if you purchased a knee wrap; </li>\n    <li>Information about your follow up appointment. </li>\n    </ul>\n    <p>Your <span class="highlight">wound</span> must be checked and the <span class="highlight">waterproof dressing\n        replaced</span> before you leave the hospital. This dressing should then be left in place until your follow up\n        appointment. It would only need to be changed if you had further wound ooze or if water had seeped into the dressing at home.</p>\n\n\n\n    <br>\n    <h2>FOLLOW UP CARE </h2>\n    <p>• You should have an appointment either at your surgeon’s rooms or with your GP for\n        <span class="highlight">removal of clips 10 to 14 days post-op</span>. </p>\n    <p>• <span class="highlight">Continue with the exercise program</span> your physiotherapist taught you while in hospital.\n        You should see your own physiotherapist within 2 weeks of going home for ongoing assessment and treatment. </p>\n    <p>• Most surgeons require you to have an <span class="highlight">X-ray at your 6 to 8 week post surgery appointment</span>.\n        This will be arranged for you, or you will be provided a referral so you may organise yourself. </p>\n    <p>• <span class="highlight">Hydrotherapy</span> is recommended; however your wound should be fully healed prior to\n        commencing any form of hydrotherapy, usually after one month.</p>\n\n\n\n    <br>\n    <h2>EXPECTATIONS AFTER DISCHARGE </h2>\n    <p>• <span class="highlight">Take regular pain relief</span>, as it will make the recovery process easier!\n        When you require more prescriptions for pain relief medication please contact your GP so that he/she can\n        assess your needs. Please don’t leave this until you run out of tablets! </p>\n    <p>Discussing this with your GP before admission is a good idea but <span class="highlight">do not arrange a\n        script for the drugs</span> as you may not go home on that medication. </p>\n    <p>• <span class="highlight">Avoid constipation</span>! Taking pain medications does greatly increase the chance of\n        constipation. This can be avoided by drinking more fluids and increasing fibre (fruit & vegetables) in your diet.\n        Medications (laxatives) are available over-the-counter from your local pharmacist if you have not had a bowel\n        action for 3 days. Fibre laxatives (such as Metamucil) may worsen the constipation as they increase the size of the stool. </p>\n    <p>• <span class="highlight">Rest and elevation!</span> Remember that you will feel tired after going home from hospital.\n        Rest and elevation of your leg are just as important as your exercises. You should discuss with your GP,\n        or Specialist, if tiredness is a problem for more than a month.</p>\n\n\n\n    <br>\n    <h2>THINGS TO WATCH OUT FOR </h2>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• If pain goes worse, contact your surgeon’s <span class="highlight">Clinic Nurse Manager or surgeon’s Rooms</span>,\n                Outside of office hours ring your GP, the after hours medical service (CALMS) in the ACT on\n                <span class="highlight">1300 422 567</span> or go to the nearest <span class="highlight">Emergency Department</span>.</p>\n            <p>• If you develop <span class="highlight">chest pain, chest tightness or shortness of breath</span> please call\n                an <span class="highlight">ambulance</span></p>\n        </ion-card-content>\n    </ion-card>\n\n\n    <br>\n    <p>• It is normal for your leg to <span class="highlight">swell on and off for about 3 months after surgery</span>.\n        The more you are up and around during the day the more it will swell. The only way to settle the swelling is to\n        lie down with your leg up on a pillow 2 to 3 times each day. </p>\n    <p>For knee replacement patients - you may put a pillow under your leg for short periods during the day but you\n        should not sleep at night with a pillow under your leg. </p>\n\n    <p>• The swelling in your leg should be less in the morning when you get up than it was the night before. However,\n        it may not be the same size as your other leg. If it is as swollen, or more swollen, or if you have an area that\n        is painful to touch in the back of your calf, you will need to have an <span class="highlight">ultrasound</span>\n        to check that you do not have a clot (Deep Vein Thrombosis or DVT). </p>\n    <p>• If you develop <span class="highlight">chest pain, chest tightness or shortness of breath</span> please call an\n        <span class="highlight">ambulance</span> as you may have a clot in your lungs (Pulmonary Embolus or PE). </p>\n    <p>• If you experience an <span class="highlight">increase in pain in your leg</span> or notice\n        <span class="highlight">redness, increased heat or swelling</span>, or a\n        <span class="highlight">discharge from your wound</span> either contact your surgeon’s Clinic Nurse Manager or\n        ring surgeon’s Rooms. Outside of office hours ring your GP, the after hours medical service (CALMS) in the\n        ACT on <span class="highlight">1300 422 567</span> or go to the nearest <span class="highlight">Emergency Department</span>. </p>\n    <p>Accept the limitations this operation will initially have on your life, and that of your partner or carer.\n        You may all feel frustrated at times so it is important to talk about it! <span class="highlight">This too will get easier, with time</span>.</p>\n\n\n\n\n\n    <h1>LONG TERM CARE</h1>\n    <br>\n    <ion-card>\n        <ion-card-header>TIP:</ion-card-header>\n        <ion-card-content>\n            <p>• Joint replacement is a long term effort, keep in contact and make sure you have the best outcome</p>\n            <p>• Joint replacement information session is held every month, including the content in this booklet/ app and live demonstrations.</p>\n        </ion-card-content>\n    </ion-card>\n\n\n\n\n    <br>\n    <h2>JOINT REPLACEMENT OUTCOMES </h2>\n    <p>As a valued patient of Orthopaedics ACT the long term monitoring of your joint replacement is important to us. </p>\n    <p>Orthopaedics ACT has established a surveillance database which allows us to track the performance of your implant.\n        This is undertaken using an international standard clinical scoring system in conjunction with x-ray reviews. </p>\n    <p>The programme requires participants to complete <span class="highlight">questionnaires</span> at the time your\n        surgery is booked to set a base line. Your surgeon completes and reviews data at various intervals;\n        <span class="highlight">your initial consultation, in surgery, and 6 weeks post-op</span>. </p>\n    <p>Thereafter you can expect to be contacted by the Joint Replacement outcomes team at <span class="highlight">1, 4, 7 and 10 years</span>. </p>\n    <p>Depending on the results of your questionnaire and x-ray you may be asked to meet with your GP or surgeon. </p>\n    <p>If you have any questions concerning the surveillance process please contact Joint Replacement Outcomes on <span class="highlight">6221 9341</span>.</p>\n\n\n\n    <br>\n    <h2>COMMUNITY COMMITMENT </h2>\n    <p>Each of our doctors commits time in support of the local community by providing cover on the Orthopaedic On Call\n        roster for the Emergency and Trauma Department of The Canberra Hospital </p>\n    <p>In addition each doctor will frequently donate their time and expertise to travel to remote areas of Australia or\n        third world countries to provide well needed surgical care. </p>\n\n\n\n    <br>\n    <h2>EMPOWERING YOU </h2>\n    <p>At Orthopaedics ACT we believe that, by educating you and those who will assist you through your surgery and\n        recovery, we empower you to positively influence your surgical outcome. </p>\n    <p>With this goal in mind we provide: </p>\n    <p>• A website with detailed notes and videos of procedures </p>\n    <p>• An extensive range of printed handouts </p>\n    <p>• A complimentary 2 hour hip and knee joint replacement information session for you and your carer (run every 4 weeks)</p>\n\n\n\n    <br>\n    <h2>JOINT REPLACEMENT INFORMATION SESSION </h2>\n    <p>Each month our Clinic Nurses, in partnership with administrative staff and orthopaedically experienced physiotherapists,\n        provide a 2-hour briefing for patients scheduled for a hip or knee joint replacement. </p>\n    <p>The session is divided into three sections: </p>\n    <p>- <span class="highlight">How to prepare before surgery</span> </p>\n    <p>- <span class="highlight">What to expect in hospital</span> </p>\n    <p>- <span class="highlight">How to manage when back at home</span> </p>\n    <p>In support of our belief that knowledge is empowering we encourage patients to bring along those who will be\n        providing care and support as you prepare and recover from your surgery. </p>\n    <p>Following the session we offer afternoon tea (sponsored by Calvary John James Hospital) and the opportunity to\n        speak with those present to answer any personal questions. </p>\n    <p>Please contact your surgeon’s rooms if you have any other questions before or after your operation.</p>\n\n\n</div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-3/summary-3.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], Summary3Page);
    return Summary3Page;
}());

//# sourceMappingURL=summary-3.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Summary1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Summary1Page = /** @class */ (function () {
    function Summary1Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Summary1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Summary_1Page');
    };
    Summary1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-summary-1',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-1/summary-1.html"*/'<!--\n  Generated template for the SummaryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<style>\n  ion-card-header {color: royalblue;\n    font-size: 2.5rem;\n    font-weight: bold}\n  h1   {color: black;\n    font-size: 3.5rem;\n    font-weight: bold;}\n  h2   {color: royalblue;\n    font-size: 2.5rem;\n    font-weight: bold}\n  p    {font-size: 1.8rem;\n    line-height: 1.6}\n  div {\n    margin:auto;\n    max-width: 100rem;\n  }\n  .highlight {color: royalblue;\n    font-weight: bold}\n  .titleHighlight {\n    color: cadetblue;\n    font-weight: bold;\n\n  }\n  .bold      {font-weight: bold}\n  .centre    {display: block;\n    margin-left: auto;\n    margin-right: auto;\n  }\n</style>\n\n\n<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Before Surgery - Info</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding style="font-family: SansSerif; background: ghostwhite">\n\n<div>\n\n  <h1>BEFORE YOUR SURGERY</h1>\n\n\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• <span class="highlight">Patient Checklist</span>: Go to Checklist which will help to ensure you are well prepared for your surgery</p>\n      <p>• <span class="highlight">Chest X-ray (CXR)</span>: Please ensure that you pick up your CXR and bring it with you on the day of surgery</p>\n    </ion-card-content>\n  </ion-card>\n\n\n  <br>\n  <h2>PRE ADMISSION PROCESS </h2>\n  <p>Prior to surgery you will face an assortment of paperwork, all designed to ensure your medical care is customised\n    to your personal needs and provided to you at the highest possible level. The following identifies important pieces\n    of paperwork that form part of the pre-admission process:</p>\n\n\n\n  <br>\n  <h2>Orthopaedics ACT </h2>\n  <p><span class="highlight"> Health Assessment Form:</span> Designed to identify medical issues which may require management prior to your surgery.</p>\n\n\n\n  <br>\n  <h2>YOUR HOSPITAL </h2>\n  <p>Please ensure that you have completed and returned your <span class="highlight">“Hospital Admission Package”</span>\n    directly to the hospital at which you are having your surgery. It is a good idea to have a family member check the\n    paperwork to ensure nothing has been missed. Once the hospital has received your completed paperwork you will be\n    contacted about your <span class="highlight">Admission Date, Fasting Time and Pre Admission Clinic.</span> </p>\n\n  <br>\n  <p>You may ring the hospital one to <span class="highlight">two days prior</span> to your admission to reconfirm your\n    admission and fasting times. The hospital documentation includes a “<span class="highlight">Health Assessment form</span>”\n    which is designed to identify medical issues that may require management prior to your surgery.</p>\n\n\n\n  <br>\n  <h2>MEDICAL TESTS </h2>\n  <p>Medical tests will be <span class="highlight">required prior to surgery</span>. The type of test will be confirmed\n    by your surgeon’s rooms and the appropriate forms provided. The following list provides examples of some of the\n    tests you may be asked to undertake.</p>\n\n\n\n  <br>\n  <h2>TESTS TO BE DONE <span class="titleHighlight">14 DAYS</span> PRIOR TO SURGERY </h2>\n  <p><span class="bold">• ECG</span>: To check your heart rhythm </p>\n  <p><span class="bold">• Urinalysis</span>: To check for bladder infection </p>\n  <p><span class="bold">• Blood pathology</span>: To assess your general health, in particular liver and kidney\n    function and haemoglobin levels</p>\n  <p><span class="bold">• Group & Hold (G&H) or X-match</span>: This is done <span class="highlight">in case a blood transfusion is required</span>\n    after your surgery. The blood specimen can only be processed in Canberra. If you live outside of the Canberra area\n    you will need to go to one of the collection centres listed on the back of your Pathology Request Form or come to\n    Canberra the day before your operation to have this done. You can have the specimen taken up to\n    <span class="highlight">28 days</span> before your surgery date. The specimen must be collected and processed\n    <span class="highlight">before your admission</span> to hospital to ensure it is safe to start your surgery. </p>\n\n  <p>The <span class="bold">Woden Collection Centre</span> closes at 7.30pm Monday to Friday, 2pm on Saturday and 1pm on Sunday.</p>\n\n\n\n  <br>\n  <h2>TESTS TO BE DONE <span class="titleHighlight">7 DAYS</span> PRIOR TO SURGERY </h2>\n  <p><span class="bold">• A Chest X-ray (CXR)</span>: To check that your <span class="highlight">lungs are clear</span>\n    (not all patients need a CXR before surgery. You will only be given a form if this is needed). </p>\n\n\n\n\n  <br>\n  <h1>PREPARING</h1>\n\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• The <span class="highlight">Independent Living Centre</span>: located at Weston, has a wide variety of equipment\n      on display and is an excellent source of information. They can be contacted on <span class="highlight">6205 1900</span> to arrange an appointment\n      if you would like personal advice about products and resources which can be used in your home.</p>\n      <p>• Places like <span class="highlight">“ACT Foam”</span> and <span class="highlight">“Clark Rubber”</span> can be\n        contacted if foam wedges are needed for cars and other places.</p>\n      <p>• Ensure that you have someone with you</p>\n    </ion-card-content>\n  </ion-card>\n\n\n\n\n  <br>\n  <h2>PREPARING YOURSELF </h2>\n  <p>Getting yourself as fit as possible prior to surgery will aid your recovery. </p>\n  <p>We recommend <span class="highlight">working with a physiotherapist well before surgery</span>. They will provide\n    a range of exercises which will aid muscle conditioning, upper body strengthening and teach you the correct way to\n    use your mobility aides.</p>\n\n\n\n  <br>\n  <h2>PREPARING YOUR HOME </h2>\n  <p>Arrangements for going home, and your care at home, need to be planned well before your admission.</p>\n\n\n\n  <br>\n  <h2>EQUIPMENT </h2>\n  <p>There is a range of equipment that you will find useful as you recover. We recommend that you organise your\n    equipment <span class="highlight">prior to</span> your hospital admission. </p>\n  <p>• Check <span class="highlight">chair and bed heights</span> at home to ensure your\n    <span class="highlight">knees are lower than your hips</span> when sitting (very important for hip replacements\n    patients but also makes it easier to stand up after a knee replacement). It may be necessary to: </p>\n  <p>&emsp;• place a cushion or foam wedge on the chair and </p>\n  <p>&emsp;• add another mattress to the bed or </p>\n  <p>&emsp;• place a riser under the legs of your bed or chair.</p>\n\n  <br>\n  <p>• Obtain an <span class="highlight">over-toilet chair</span> to make it safer to get down to and up from the toilet. </p>\n  <img src="../assets/image/pre1.jpg" class="centre">\n\n  <br>\n  <p>• A <span class="highlight">shower chair</span>, is an aid that may not be required by everyone. Deemed helpful if\n    you feel unsteady on your feet. </p>\n  <img src="../assets/image/pre2.png" class="centre">\n\n  <br>\n  <p>• An <span class="highlight">extend-a-hand</span> (available from larger chemists or Bunnings) or a\n    <span class="highlight">long pair of BBQ tongs</span> will make picking up light objects from ground level easier\n    and can also be helpful for dressing. </p>\n\n  <br>\n  <p>• A pair of <span class="highlight">Canadian crutches</span>, or suitable walking aid is required whilst in\n    hospital and will aid your mobility once home. Available from Orthopaedics ACT, Mobility Matters and many large chemists.</p>\n  <img src="../assets/image/pre3.png" class="centre">\n\n\n\n  <br>\n  <h2>A SAFE ENVIRONMENT </h2>\n  <p>• Make your home as <span class="highlight">clutter-free</span> as possible. You will be using crutches, or some\n    sort of walking aid, for about 6 weeks so ensure you have clear pathways around your house. </p>\n  <p>• <span class="highlight">Take up mats to avoid tripping</span>. </p>\n  <p>• Remove, or tape down, any <span class="highlight">trailing cords</span> (electrical or telephone). </p>\n  <p>• Ensure your <span class="highlight">bathroom is clean</span> to reduce the risk of infection.</p>\n\n\n\n  <br>\n  <h2>STOCKING-UP </h2>\n  <p>• Stock up on <span class="highlight">easy to prepare food</span> such as frozen meals. </p>\n  <p>• Stock up on <span class="highlight">projects to keep you occupied</span> during your recovery (e.g. DVDs /\n    videos, organising photo albums). You will be limited in your ability to travel without support and may become bored\n    if you have not planned ahead. </p>\n\n\n\n  <br>\n  <h2>MINOR MODIFICATIONS </h2>\n  <p>• Consider <span class="highlight">shower cubicle</span>: A hand-held shower hose may be easier to use; or a wall\n    affixed hand rail may provide security. </p>\n  <img src="../assets/image/pre4.png" class="centre">\n\n  <br>\n  <p>• Consider any <span class="highlight">internal or external stairs</span>. Do these have\n    <span class="highlight">rails?</span></p>\n  <img src="../assets/image/pre5.png" class="centre">\n\n\n\n  <br>\n  <h2>CARER RESPONSIBILITIES </h2>\n  <p>• If you are the Carer for another person please ensure you <span class="highlight">have plans in place</span>\n    for the support of that person both whilst you are in hospital and after discharge while you are convalescing\n    from your surgery. You will be the one needing assistance and must <span class="highlight">focus on your own needs</span>. </p>\n\n  <br>\n  <p>For a hip replacement: </p>\n  <p>• To reduce the risk of dislocation when you sit down, it is important that your\n    <span class="highlight">knee is at a lower level than your hip</span> i.e. that the angle between your body and your\n    thigh is more than 90 degrees.</p>\n  <p>• It is also essential that the <span class="highlight">chair you sit on is stable and has firm arms</span>\n    to lean on when getting out of the chair. This means that you can push up from the chair using your arms rather than leaning forward. </p>\n  <p>• Some patients find placing a foam wedge, covered in a satin pillow case or plastic bag, on their car seat helpful\n    also. Places like “<span class="highlight">ACT Foam</span>” and “<span class="highlight">Clark Rubber</span>”\n    can be contacted for further information.</p>\n\n\n\n  <br>\n  <h2>ARRANGING SUPPORT AT HOME </h2>\n  <p>Please explore all options to <span class="highlight">ensure that you have someone with you.</span>\n    Ideally, this should be day and night for about 2 weeks and then at night time only until 6 weeks after surgery.\n    Failing this, you should at least have someone to stay overnight for a minimum of two weeks.</p>\n\n\n\n  <br>\n  <h2>STANDBY ASSISTANCE WITH SHOWERING </h2>\n  <p>It is expected that upon discharge, all patients are able to shower independently. However, you can ask\n    <span class="highlight">someone nearby (family member/carer)</span> to help you dry your feet and body if necessary.</p>\n\n\n\n  <br>\n  <h2>HOUSEWORK AND LAUNDRY </h2>\n  <p>Vacuuming, cleaning floors or bathrooms, changing beds and hanging out loads of washing will be very difficult,\n    if not impossible, to do safely after going home. Depending on your circumstances subsidised assistance may be\n    available. <span class="highlight">Private agencies</span> are available to assist. Please ask your hospital’s\n    <span class="highlight">Pre Admission Clinic or Discharge Planner</span> for information if you are interested in this.</p>\n\n\n\n  <br>\n  <h2>MEAL PREPARATION </h2>\n  <p>Assisting with meal preparation is a key reason that we recommend you have someone stay with you. </p>\n  <p>• You cannot carry plates and other crockery while on 2 crutches. </p>\n  <p>• Standing at the kitchen bench to prepare and eat food is tiring. </p>\n  <p>• Hip replacement patients need to be very careful sitting down and standing up if planning to use a kitchen / bar\n    stool to sit at the kitchen bench for meals. </p>\n  <p>• The person staying with you could leave <span class="highlight">sandwiches and a thermos</span> within easy reach for you for lunch time. </p>\n  <p>• <span class="highlight">Microwave ovens</span> that are on the bench top (not on shelves) can be used for re heating pre-prepared food. </p>\n  <p>• There are Gourmet Dinner Services which will <span class="highlight">deliver prepared food</span> or\n    Meals-on-Wheels may be available to you.</p>\n  <p>• Home delivery of groceries is available with most super markets or local grocery stores.</p>\n\n\n\n  <br>\n  <h2>PETS </h2>\n  <p>Caring for your pet when you return home may also be difficult as your ability to bend down is limited. </p>\n  <p>• Cats are easier than dogs to look after as they can be taught to jump on washing machines to access their food\n    and water bowls. Assistance will be needed when changing the litter tray due to the risk of acquiring an infection. </p>\n  <p>• Care should be taken to ensure that dogs will not jump up and knock you over. Assistance will probably be required for feeding them.</p>\n\n\n\n  <br>\n  <h2>SUPPORT AFTER DISCHARGE </h2>\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• Post-surgery rehabilitation or home care programs are available for your convenience.</p>\n      <p>• Make arrangements for someone to <span class="highlight">collect you</span> on your day of discharge.</p>\n      <p>• You should not drive for <span class="highlight">6 weeks</span> after your surgery.</p>\n    </ion-card-content>\n  </ion-card>\n\n\n  <br>\n  <p>If you require Community Services or have any concerns about support after discharge please contact your hospital’s\n    <span class="highlight">Pre Admission Clinic or Discharge Planner</span> prior to your admission.</p>\n  <p>• Calvary John James Hospital, National Capital Private Hospital, Calvary Bruce Hospital and Canberra Private Hospital\n    all offer <span class="highlight">rehabilitation programs</span> that provide both inpatient and outpatient treatment\n    services. If you are interested in accessing these services, please discuss the referral process with your hospital’s\n    <span class="highlight">Pre Admission Clinic or Discharge Planner</span>. It is a good idea to check with your\n    <span class="highlight">Health Fund</span> that you are covered for these services. </p>\n  <p>• <span class="highlight">Convalescent care</span> for private patients in the ACT is limited.\n    Private hospitals may not be able to arrange referral to Burrangiri. It is best to discuss this with your GP to arrange a referral. </p>\n  <p>• Some non-ACT residents may be able to be transferred to their local hospitals for step-down care.\n    Please consult your GP about this prior to surgery. </p>\n  <p>• Some <span class="highlight">community support</span> is available for assistance with showering and other\n    activities of daily living. Most people are eligible for <span class="highlight">subsidised assistance</span>, depending on their circumstances. </p>\n  <p>• There are <span class="highlight">private agencies</span> available to assist with a range of activities from\n    personal care to dog walking (check the Yellow Pages under Support Groups).</p>\n\n\n  <br>\n  <h2>TRANSPORT </h2>\n  <p>You will be unable to drive yourself home. Please make arrangements for\n    <span class="highlight">someone to collect you</span> on your day of discharge. </p>\n  <p>The current recommendation by the Australian Orthopaedic Association is that you <span class="highlight">should\n    not drive for 6 weeks</span> after your surgery regardless of the type of surgery or the vehicle you drive.</p>\n\n\n  <br>\n  <h2>MINIMISING RISK OF CANCELLATION OF YOUR SURGERY </h2>\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• Do not inject Clexane within <span class="highlight">24 hours</span> of your operation.</p>\n    </ion-card-content>\n  </ion-card>\n\n\n\n  <br>\n  <h2>MEDICATIONS TO CEASE </h2>\n  <p>In consultation with your GP or physician please <span class="highlight">cease any blood thinning and\n    anti-inflammatory medications</span>, including gels and ointments, 7 to 10 days prior to your surgery. </p>\n  <p>It is also important to check with your GP, or Rheumotologist, if you are taking specialised medications such as\n    <span class="highlight">Methotrexate or Salazopyrin</span>. </p>\n  <p>You are more inclined to bleed if you stay on these medications. It is important that you ask your GP or physician\n    when you should resume these medications, reminding your GP that your surgeon will prescribe\n    <span class="highlight">Clexane 40mg injections for twenty days</span> after your discharge. </p>\n  <p>You should also stop taking certain over-the-counter medications such as\n    <span class="highlight">fish oil / Omega 3, flax seed oil, St John’s Wort, high doses of Vitamin E</span>,\n    and any other ‘natural’ health medications. </p>\n  <p>If you are taking <span class="highlight">Warfarin</span> it is <span class="highlight">VERY important</span>\n    that you discuss the management of this with your GP or Cardiologist. Your surgeon usually suggests that you should\n    stop taking Warfarin <span class="highlight">5 days before your surgery</span> date. Your GP will order a\n    <span class="highlight">blood test (INR) to be done 2 to 3 days later</span>. Once your INR falls below the\n    therapeutic level your GP may arrange for you to have Clexane injections into your abdomen up until, but not\n    including the day of your surgery.</p>\n\n\n\n  <br>\n  <h2>AVOIDING SKIN DAMAGE PRE-SURGERY </h2>\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• Don’t do activities that may damage your skin prior to the surgery!</p>\n      <p>• Avoid activities that may damage your skin <span class="highlight">3 weeks</span> prior to surgery!</p>\n    </ion-card-content>\n  </ion-card>\n\n\n  <br>\n  <p>Patients have had their surgery postponed because of skin tears, mouth ulcers or scratches which can increase the\n    <span class="highlight">risk of infection</span>. Activities such as gardening, including pruning roses, should be\n    avoided for at least <span class="highlight">three weeks</span> prior to your surgery. </p>\n  <p>Any skin problems should be checked by your GP. Please make sure your surgeon’s rooms are aware of any issues and\n    the treatment, in case you need to be reviewed prior to your admission. </p>\n\n\n\n  <br>\n  <h2>TRICLOSAN </h2>\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• If you did not receive a bottle please contact your surgeon’s rooms.</p>\n    </ion-card-content>\n  </ion-card>\n\n\n\n  <br>\n  <p>You will be given a small bottle of <span class="highlight">Triclosan Shower Wash</span> to use the night before\n    and the morning of surgery. It is a mild antiseptic wash which is effective against bacteria, fungus and viruses.\n    Please <span class="highlight">do not use</span> this wash on your head, face or areas of broken skin including dermatitis and eczema. </p>\n\n\n\n  <br>\n  <h2>DENTAL </h2>\n  <p>If you have had ongoing problems with your teeth or gums please check with your dentist that you do not have an\n    infection prior to your surgery.</p>\n\n\n\n  <br>\n  <h2>HOSPITAL ADMISSION </h2>\n  <br>\n  <ion-card>\n    <ion-card-header>TIP:</ion-card-header>\n    <ion-card-content>\n      <p>• In a <span class="highlight">named bag</span>: night wear, large slipper, notebook, watch, medication, and crutches.</p>\n      <p>• Go to hospital reception on the day of surgery.</p>\n    </ion-card-content>\n  </ion-card>\n\n\n\n\n  <br>\n  <h2>WHEN AND WHERE </h2>\n  <p>Hospital Admission is usually on the day of surgery unless medical investigations are required prior. If required\n    you will be advised by your surgeon or physician. On your day of surgery, go to the\n    <span class="highlight">reception desk located at the main entrance</span> of your hospital. </p>\n\n\n\n  <br>\n  <h2>WHAT TO BRING </h2>\n  <p>In addition to personal clothing and toiletries you should bring the following items to hospital, in a clearly\n    <span class="highlight">named bag</span>: </p>\n  <p>• Loose fitting, comfortable <span class="highlight">night wear</span>. If wearing pyjamas, short leg ones are\n    recommended due to post-operative swelling; </p>\n  <p>• If you want to wear slippers do not bring new ones as they will be too tight due to the leg swelling caused by\n    the operation. If you would like to purchase new ones, <span class="highlight">a size larger than normal</span>\n    is recommended to accommodate any post operative swelling; </p>\n  <p>• A <span class="highlight">note book</span> to write down any questions and to keep a record of the pain\n    medications you are taking and when; </p>\n  <p>• A <span class="highlight">travel clock or watch</span>, as there are no clocks in many of the patient’s rooms; </p>\n  <p>• <span class="highlight">Regular medications</span> in the original packaging to enable correct charting of\n    medication times and doses. If possible a medication list from your pharmacist is also useful if you take a lot of medications; </p>\n  <p>• Some patients like to bring their own coffee cup and tea bags; </p>\n  <p>• Dry biscuits to take with your pain medication; and </p>\n  <p>• Crutches for use on day 2 or 3.</p>\n\n\n\n  <br>\n  <h2>FASTING TIMES AND MEDICATIONS</h2>\n  <p>Fasting times will be advised when the hospital confirms your admission time.</p>\n  <p>As a general guide:</p>\n  <p>• For a <span class="highlight">6.30am to 7.30am admission</span> - you will be asked to have nothing to eat or\n    drink (fast) from <span class="highlight">12 midnight</span>. Do not take your blood pressure medications on the\n    morning of surgery but bring them into hospital with you. When you are seen by the anaesthetist they may want you to take them then.</p>\n  <p>• For a <span class="highlight">10.00am admission</span> - you will be asked to fast\n    <span class="highlight">from 7am</span>. You can take your medications with your breakfast.</p>\n\n\n\n  <br>\n  <h2>END OF PRE SURGERY PREPARATION</h2>\n  <p>If you have finished preparing your home and yourself following the instructions in this section and filling up the\n    To-Do list, you should be ready for the surgery now, please move on and read the next section about In Hospital Actions and Precautions.</p>\n  <h3 class="highlight">Good Luck For Your Surgery!</h3>\n\n\n</div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-1/summary-1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], Summary1Page);
    return Summary1Page;
}());

//# sourceMappingURL=summary-1.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SummaryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SummaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SummaryPage = /** @class */ (function () {
    function SummaryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SummaryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SummaryPage');
    };
    SummaryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-summary',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary/summary.html"*/'<!--\n  Generated template for the SummaryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<style>\n  ion-card-header {color: royalblue;\n    font-size: 2.5rem;\n    font-weight: bold}\n  h1   {color: royalblue;\n    font-size: 3.5rem;}\n  h2   {color: royalblue;\n    font-size: 2.5rem;\n    font-weight: bold}\n  p    {font-size: 1.8rem;\n    line-height: 1.6}\n  div {\n    margin:auto;\n    max-width: 100rem;\n  }\n  .highlight {color: royalblue;\n    font-weight: bold}\n  .titleHighlight {\n    color: cadetblue;\n    font-weight: bold;\n\n  }\n  .bold      {font-weight: bold}\n  .centre    {display: block;\n    margin-left: auto;\n    margin-right: auto;\n  }\n</style>\n\n\n\n<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Information Summary - Introduction</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding style="font-family: SansSerif; background: ghostwhite">\n  <div>\n\n\n  <h1>Welcome to OrthoApp</h1>\n\n  <br>\n  <!--<img src="../assets/image/1_intro.png" class="centre">-->\n  <p>The information in this web portal is designed to provide a general overview of <span class="highlight">procedures,\n    treatments and activities</span> which will occur before, during and after your surgery.</p>\n\n  <p>We anticipate that this may lead to questions <span class="highlight">best answered by your physiotherapist and/or\n    anaesthetist</span>. The data you have submitted through OrthoApp will be useful for physiotherapists and surgeons\n    to keep track of your preparation and recovery throughout the process.</p>\n\n  <br>\n\n  <h4 class="bold">Acknowledgements</h4>\n  <p>OrthoApp was developed as a collaboration between the:<br>\n    1. Department of Health Services Research and Policy, Research School of Population Health, College of Health and\n    Medicine (CHM), Australian National University (ANU),<br>\n    2. TechLauncher Programme, College of Engineering and Computer Studies, ANU<br>\n    3. Orthopaedics ACT<br>\n    4. Canberra Hip and Knee Replacement<br>\n    5. ANU Medical School, CHM, ANU<br>\n    6. Academic Unit of General Practice, ACT Health<br>\n    7. Health Care Consumer Association, ACT.<br>\n  </p>\n\n  <p>\n    We are very grateful to the patients who worked closely with us in co-designing this tool, which is designed to enable\n    patients to understand and manage their health before and following hip and knee arthroplasty.\n  </p>\n\n  <p>\n    We would also like to acknowledge the contributions of Ms Corinne Coulter, physiotherapist, and Dr David Reiner, anaesthetist.\n  </p>\n\n  <p>\n    The written information in OrthoApp was adapted in agreement with Orthopaedics ACT and contributors from the Joint\n    Replacement Information Session Handout, developed by Orthopaedics ACT. Contributions from Elaine Sainsbury,\n    Mary Jane Moran, Elizabeth Tuohy and Christine Winchester are acknowledged and appreciated.\n  </p>\n\n  <br>\n\n  <h4 class="bold">Disclaimer</h4>\n  <p>This is an educational resource only and should be used in conjunction with the advice and any additional information\n    (e.g reading material, website) provided by your surgeon or a licensed healthcare provider.</p>\n\n  <br>\n\n  <h4 class="bold">Copyright</h4>\n  <p>Reproduction of material in this web portal is prohibited without the consent of Orthopaedics ACT (© 2013 Orthopaedics ACT) and the Australian National University.</p>\n\n\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary/summary.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SummaryPage);
    return SummaryPage;
}());

//# sourceMappingURL=summary.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ChartPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ChartPage = /** @class */ (function () {
    function ChartPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChartPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChartPage');
    };
    ChartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chart',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/chart/chart.html"*/'<!--\n  Generated template for the ChartPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Chart</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/chart/chart.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ChartPage);
    return ChartPage;
}());

//# sourceMappingURL=chart.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__surgeon_menu_surgeon_menu__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__menu_menu__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__profile_profile__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LoadingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoadingPage = /** @class */ (function () {
    function LoadingPage(app, afAuth, afDatabase, navCtrl, navParams, loadingCtrl) {
        this.app = app;
        this.afAuth = afAuth;
        this.afDatabase = afDatabase;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.profile = {};
        this.presentLoading();
    }
    LoadingPage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 1000
        });
        loader.present();
    };
    LoadingPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var userID = this.afAuth.auth.currentUser;
        console.log(userID.uid);
        this.profileRef$ = this.afDatabase.object("profile/user/" + userID.uid);
        this.subscribeData = this.profileRef$.subscribe(function (data) {
            if ((data.firstName != null) && (data.lastName != null) && (data.Birthday != null)) {
                if (data.Status) {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__surgeon_menu_surgeon_menu__["a" /* SurgeonMenuPage */]);
                }
                else {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__menu_menu__["a" /* MenuPage */]).catch(function (e) {
                        var error = e.errorMessage;
                        console.log(error);
                    });
                }
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__profile_profile__["a" /* ProfilePage */]).catch(function (e) {
                    console.log(e.message);
                });
            }
        });
    };
    LoadingPage.prototype.ngOnDestroy = function () {
        this.subscribeData.unsubscribe();
    };
    LoadingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-loading',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/loading/loading.html"*/'<!--\n  Generated template for the LoadingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/loading/loading.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LoadingPage);
    return LoadingPage;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurgeonMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__surgeon_home_tabs_surgeon_home_tabs__ = __webpack_require__(167);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SurgeonMenuPage = /** @class */ (function () {
    function SurgeonMenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__surgeon_home_tabs_surgeon_home_tabs__["a" /* SurgeonHomeTabsPage */];
        this.pages = [
            { title: 'Home', pageName: __WEBPACK_IMPORTED_MODULE_2__surgeon_home_tabs_surgeon_home_tabs__["a" /* SurgeonHomeTabsPage */], tabComponent: 'SurgeonHomePage', icon: 'home' },
        ];
    }
    SurgeonMenuPage.prototype.openPage = function (page) {
        var params = {};
        this.nav.setRoot(page.pageName, params);
    };
    SurgeonMenuPage.prototype.isActive = function (page) {
        var childNav = this.nav.getActiveChildNav();
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }
        if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
            return 'primary';
        }
        return;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], SurgeonMenuPage.prototype, "nav", void 0);
    SurgeonMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-surgeon-menu',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/surgeon-menu/surgeon-menu.html"*/'<ion-menu [content]="Content">\n  <ion-header>\n\n    <ion-navbar>\n      <ion-title>Menu</ion-title>\n    </ion-navbar>\n\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button ion-item menuClose color="lyellow" *ngFor="let p of pages" (click)="openPage(p)">\n        <ion-icon item-start [name]="p.icon" [color]="isActive(p)"></ion-icon>\n        {{p.title}}\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n<ion-nav [root]="rootPage" #Content swipeBackEnabled="false"></ion-nav>\n\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/surgeon-menu/surgeon-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SurgeonMenuPage);
    return SurgeonMenuPage;
}());

//# sourceMappingURL=surgeon-menu.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurgeonHomeTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chart_chart__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__surgeon_home_surgeon_home__ = __webpack_require__(168);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SurgeonHomeTabsPage = /** @class */ (function () {
    function SurgeonHomeTabsPage(navParams) {
        this.homeRoot = __WEBPACK_IMPORTED_MODULE_3__surgeon_home_surgeon_home__["a" /* SurgeonHomePage */];
        this.chartRoot = __WEBPACK_IMPORTED_MODULE_2__chart_chart__["a" /* ChartPage */];
        this.myIndex = 0;
    }
    SurgeonHomeTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-surgeon-home-tabs',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/surgeon-home-tabs/surgeon-home-tabs.html"*/'<ion-tabs>\n    <ion-tab [root]="homeRoot" tabTitle="Home" tabIcon="custom-home"></ion-tab>\n    <!--<ion-tab [root]="chartRoot" tabTitle="Chart" tabIcon="custom-aftersurgery"></ion-tab>-->\n</ion-tabs>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/surgeon-home-tabs/surgeon-home-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SurgeonHomeTabsPage);
    return SurgeonHomeTabsPage;
}());

//# sourceMappingURL=surgeon-home-tabs.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurgeonHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__painLevel_contact__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SurgeonHomePage = /** @class */ (function () {
    function SurgeonHomePage(app, afAuth, navCtrl, navParams, afDatabase) {
        var _this = this;
        this.app = app;
        this.afAuth = afAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDatabase = afDatabase;
        var currentUser = afAuth.auth.currentUser;
        var name;
        this.userRef = afDatabase.database.ref("profile/user/");
        this.profileRef$ = this.afDatabase.object("profile/user/" + currentUser.uid);
        this.subscribeData = this.profileRef$.subscribe(function (data) {
            name = data.Name;
            _this.userRef.on('value', function (People) {
                var patientArray = [];
                People.forEach(function (user) {
                    if (user.val().Surgeon == name) {
                        patientArray.push(user.val());
                        return false;
                    }
                });
                _this.People = patientArray;
                _this.loadedPatientList = patientArray;
            });
        });
    }
    SurgeonHomePage.prototype.initializeItems = function () {
        this.People = this.loadedPatientList;
    };
    SurgeonHomePage.prototype.showProfile = function (patient) {
        var _this = this;
        this.userRef.on('value', function (People) {
            var Id;
            People.forEach(function (user) {
                if (user.val().firstName == patient.firstName && user.val().lastName == patient.lastName && user.val().Birthday == patient.Birthday) {
                    var key = user.key;
                    Id = key;
                    return false;
                }
            });
            _this.UID = Id;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__painLevel_contact__["a" /* ContactPage */], {
                userid: _this.UID
            });
        });
    };
    SurgeonHomePage.prototype.getItems = function (searchbar) {
        // Reset items back to all of the items
        this.initializeItems();
        // set q to the value of the searchbar
        var q = searchbar.srcElement.value;
        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }
        this.People = this.People.filter(function (v) {
            if (q) {
                if (v.Name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        });
        console.log(q, this.People.length);
    };
    SurgeonHomePage.prototype.ngOnDestroy = function () {
        this.subscribeData.unsubscribe();
    };
    SurgeonHomePage.prototype.logout = function () {
        var _this = this;
        this.afAuth.auth.signOut().then(function () { return _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]); });
    };
    SurgeonHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-surgeon-home',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/surgeon-home/surgeon-home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>SurgeonHome</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n      <ion-searchbar (ionInput)="getItems($event)">\n      </ion-searchbar>\n\n  <ion-list>\n    <ion-card *ngFor="let patient of People"  (click)="showProfile(patient)">\n      <ion-card-content>\n        <h2>{{ patient.firstName }} {{ patient.lastName }}</h2>\n        <p>\n         Birthday: {{ patient.Birthday }}\n        </p>\n        <p>\n         Surgery Day: {{ patient.SurgeryDay }}\n        </p>\n        <p>\n          Operation Details: {{patient.PrimaryOrRevision}} {{patient.operationType}} ({{patient.operationSide}})\n        </p>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n\n  <button ion-button clear block color="lyellow" (click)="logout()"><b>Log Out</b></button>\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/surgeon-home/surgeon-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], SurgeonHomePage);
    return SurgeonHomePage;
}());

//# sourceMappingURL=surgeon-home.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var RegisterPage = /** @class */ (function () {
    function RegisterPage(afAuth, navCtrl, navParams) {
        this.afAuth = afAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
    }
    RegisterPage.prototype.register = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result, currentUser, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password).catch(function (error) {
                                var errorMessage = error.message;
                                alert(errorMessage);
                            })];
                    case 1:
                        result = _a.sent();
                        currentUser = this.afAuth.auth.currentUser;
                        currentUser.sendEmailVerification();
                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/register/register.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>register</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding style="background-color: #1b2b68">\n  <br><br>\n  <ion-item style="background-color: transparent">\n    <ion-label floating color="light">Email Address</ion-label>\n    <ion-input type="text" style="color: #f4f4f4" [(ngModel)]="user.email" ></ion-input>\n  </ion-item>\n\n  <ion-item style="background-color: transparent">\n    <ion-label floating color="light">Password</ion-label>\n    <ion-input type="password" style="color: #f4f4f4" [(ngModel)]="user.password"></ion-input>\n  </ion-item>\n\n  <button ion-button block outline color="light" (click)="register(user)"><b>Sign Up</b></button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 176:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 176;

/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/after-surgery/after-surgery.module": [
		218
	],
	"../pages/anaesthetist-faq/anaesthetist-faq.module": [
		639,
		0
	],
	"../pages/before-surgery-tabs/before-surgery-tabs.module": [
		640,
		9
	],
	"../pages/before-surgery/before-surgery.module": [
		219
	],
	"../pages/chart/chart.module": [
		641,
		8
	],
	"../pages/in-hospital/in-hospital.module": [
		221
	],
	"../pages/loading/loading.module": [
		642,
		7
	],
	"../pages/login/login.module": [
		643,
		6
	],
	"../pages/menu/menu.module": [
		644,
		5
	],
	"../pages/nav-summary-details/nav-summary-details.module": [
		387
	],
	"../pages/nav-summary/nav-summary.module": [
		389
	],
	"../pages/profile/profile.module": [
		391
	],
	"../pages/register/register.module": [
		645,
		4
	],
	"../pages/summary-1/summary-1.module": [
		392
	],
	"../pages/summary-2/summary-2.module": [
		393
	],
	"../pages/summary-3/summary-3.module": [
		394
	],
	"../pages/summary-4/summary-4.module": [
		395
	],
	"../pages/summary/summary.module": [
		397
	],
	"../pages/surgeon-home-tabs/surgeon-home-tabs.module": [
		646,
		3
	],
	"../pages/surgeon-home/surgeon-home.module": [
		647,
		2
	],
	"../pages/surgeon-menu/surgeon-menu.module": [
		648,
		1
	],
	"../pages/todolist/todolist.module": [
		398
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 217;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AfterSurgeryPageModule", function() { return AfterSurgeryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__after_surgery__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AfterSurgeryPageModule = /** @class */ (function () {
    function AfterSurgeryPageModule() {
    }
    AfterSurgeryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__after_surgery__["a" /* AfterSurgeryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__after_surgery__["a" /* AfterSurgeryPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], AfterSurgeryPageModule);
    return AfterSurgeryPageModule;
}());

//# sourceMappingURL=after-surgery.module.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeforeSurgeryPageModule", function() { return BeforeSurgeryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__before_surgery__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var BeforeSurgeryPageModule = /** @class */ (function () {
    function BeforeSurgeryPageModule() {
    }
    BeforeSurgeryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__before_surgery__["a" /* BeforeSurgeryPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__before_surgery__["a" /* BeforeSurgeryPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], BeforeSurgeryPageModule);
    return BeforeSurgeryPageModule;
}());

//# sourceMappingURL=before-surgery.module.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BeforeSurgeryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BeforeSurgeryPage = /** @class */ (function () {
    function BeforeSurgeryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.videos = [
            {
                title: 'Meet your physiotherapist',
                video: 'https://www.youtube.com/embed/_5TMhzq-oQ4?rel=0',
            },
            {
                title: 'Pre operative Hip Exercises',
                video: 'https://www.youtube.com/embed/NpcV2WFpm-w?rel=0',
            },
            {
                title: 'Pre operative Knee Exercises',
                video: 'https://www.youtube.com/embed/CBlovyJy_BE?rel=0',
            },
        ];
    }
    BeforeSurgeryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BeforeSurgeryPage');
    };
    BeforeSurgeryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-before-surgery',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/before-surgery/before-surgery.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Pre Surgery Exercises</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n<ion-card *ngFor="let video of videos" width="20%">\n\n  <div id="videoname">\n  <ion-card-header text-wrap>\n    {{video.title}}\n  </ion-card-header>\n  </div>\n  <ion-card-content>\n    <iframe class="youtube" width="100%" height="400" [src]="video.video | youtube" frameborder="0" allowfullscreen="true"></iframe>\n  </ion-card-content>\n\n</ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/before-surgery/before-surgery.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], BeforeSurgeryPage);
    return BeforeSurgeryPage;
}());

//# sourceMappingURL=before-surgery.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InHospitalPageModule", function() { return InHospitalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__in_hospital__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var InHospitalPageModule = /** @class */ (function () {
    function InHospitalPageModule() {
    }
    InHospitalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__in_hospital__["a" /* InHospitalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__in_hospital__["a" /* InHospitalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], InHospitalPageModule);
    return InHospitalPageModule;
}());

//# sourceMappingURL=in-hospital.module.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InHospitalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the InHospitalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var InHospitalPage = /** @class */ (function () {
    function InHospitalPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.videos_inhospital = [
            {
                title: 'Hospital Admission Process',
                video: 'https://www.youtube.com/embed/WRWZ4LALP_w?rel=0',
            },
        ];
    }
    InHospitalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InHospitalPage');
    };
    InHospitalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-in-hospital',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/in-hospital/in-hospital.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Hospital Admission</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card *ngFor="let video of videos_inhospital">\n    <div id="videonameinhospital">\n      <ion-card-header text-wrap>\n        {{video.title}}\n      </ion-card-header>\n    </div>\n\n    <ion-card-content>\n      <iframe width="100%" height="200" [src]="video.video | youtube" frameborder="0" allowfullscreen="true"></iframe>\n    </ion-card-content>\n\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/in-hospital/in-hospital.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], InHospitalPage);
    return InHospitalPage;
}());

//# sourceMappingURL=in-hospital.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return API_ADDR; });
var API_ADDR = {
    firebaseAPI: "https://orthoapp-4a103.firebaseio.com/"
};
//# sourceMappingURL=apiAddr.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stepCounter_stepCounter__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__painLevel_contact__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage(navParams, menu) {
        this.menu = menu;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__stepCounter_stepCounter__["a" /* StepCounterPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__painLevel_contact__["a" /* ContactPage */];
        this.menu.enable(true);
        this.myIndex = 0;
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/tabs/tabs.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>Menu\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-tabs [selectedIndex]="myIndex">\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="custom-home"></ion-tab>\n  <!--<ion-tab [root]="tab2Root" tabTitle="Step Counter" tabIcon="custom-stepcounter"></ion-tab>-->\n  <!--<ion-tab [root]="tab3Root" tabTitle="Pain Level" tabIcon="custom-painlevel"></ion-tab>-->\n</ion-tabs>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["g" /* MenuController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__todolist_todolist__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_apiAddr__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_rest_rest__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__summary_2_summary_2__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__summary_3_summary_3__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__menu_menu__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__summary_1_summary_1__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__summary_summary__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__before_surgery_before_surgery__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__in_hospital_in_hospital__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__after_surgery_after_surgery__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__stepCounter_stepCounter__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__painLevel_contact__ = __webpack_require__(69);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





















var HomePage = /** @class */ (function () {
    function HomePage(app, rest, afAuth, afDatabase, toast, navCtrl, menu) {
        this.app = app;
        this.rest = rest;
        this.afAuth = afAuth;
        this.afDatabase = afDatabase;
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.profile = {};
        this.user = {};
        this.data = null;
        this.URL = __WEBPACK_IMPORTED_MODULE_7__app_apiAddr__["a" /* API_ADDR */].firebaseAPI;
        this.menu.enable(true);
    }
    HomePage.prototype.editProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */]);
    };
    HomePage.prototype.infosummary = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__summary_summary__["a" /* SummaryPage */]);
    };
    HomePage.prototype.beforeSurgeryinfo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__summary_1_summary_1__["a" /* Summary1Page */]);
    };
    HomePage.prototype.beforeSurgeryvideo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__before_surgery_before_surgery__["a" /* BeforeSurgeryPage */]);
    };
    HomePage.prototype.inhospitalinfo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__summary_2_summary_2__["a" /* Summary2Page */]);
    };
    HomePage.prototype.inhospitalvideo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__in_hospital_in_hospital__["a" /* InHospitalPage */]);
    };
    HomePage.prototype.afterSurgeryinfo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__summary_3_summary_3__["a" /* Summary3Page */]);
    };
    HomePage.prototype.aftersurgeryvideo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__after_surgery_after_surgery__["a" /* AfterSurgeryPage */]);
    };
    HomePage.prototype.todoList = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__todolist_todolist__["a" /* TodoPage */]);
    };
    HomePage.prototype.stepcounter = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__stepCounter_stepCounter__["a" /* StepCounterPage */]);
    };
    HomePage.prototype.painlevel = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__painLevel_contact__["a" /* ContactPage */]);
    };
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.userid = this.afAuth.auth.currentUser.uid;
        if (this.rest.getProfileCache() == null) {
            this.afAuth.auth.currentUser.getIdToken(true).then(function (idToken) {
                _this.subscribeData = _this.rest.getData(_this.userid, idToken, '/profile/user/').subscribe(function (data) {
                    _this.profileData = data;
                    _this.rest.setProfileCache(data);
                });
                _this.rest.setToken(idToken);
            });
        }
        else {
            console.log('use cache');
            this.profileData = this.rest.getProfileCache();
        }
    };
    HomePage.prototype.logout = function () {
        var _this = this;
        if (this.rest.getProfileCache() == null) {
            this.subscribeData.unsubscribe();
        }
        else {
            this.rest.emptyProfileCache();
        }
        this.afAuth.auth.signOut().then(function () { return _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]); });
    };
    HomePage.prototype.showtoggle = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__menu_menu__["a" /* MenuPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/home/home.html"*/'  <ion-header>\n    <ion-navbar>\n      <ion-buttons left>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n\n  <ion-content padding>\n    <ion-card>\n        <ion-card-title align="center" color="light" style="background-color: #1b2b68"><b>Personal Information</b></ion-card-title>\n       <div id="commonfont">\n        <ion-card-content>\n            <img src="../assets/image/1_intro.png">\n            <ion-icon name="smallicon-me"></ion-icon>Name: <p>{{(profileData)?.firstName}} {{(profileData)?.lastName}}</p>\n         <ion-icon name="smallicon-surgeon"></ion-icon>Surgeon: <p>{{(profileData)?.Surgeon}}</p>\n         <ion-icon name="smallicon-surgerytype"></ion-icon>Operation Type: <p>{{(profileData)?.PrimaryOrRevision}} {{(profileData)?.operationType}} ({{(profileData)?.operationSide}})</p>\n         <ion-icon name="smallicon-date"></ion-icon>Surgery Day: <p>{{(profileData)?.SurgeryDay}}</p>\n\n         <button ion-button clear block color="lyellow" icon-left (click)="editProfile()">\n             <ion-icon name="smallicon-edit"></ion-icon>Edit Profile\n         </button>\n\n       </ion-card-content>\n       </div>\n    </ion-card>\n      <div id="homebutton">\n              <button text-wrap ion-button color="lorange" (click)="infosummary()" style="width: 47.9%; height: 65px; font-size: 14px" >\n             <ion-icon name="custom-doc"></ion-icon><b>Welcome to OrthoApp</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="beforeSurgeryinfo()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-beforesurgeryinfo"></ion-icon><b>Before Surgery<br>- Info</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="beforeSurgeryvideo()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-videocamera"></ion-icon><b>Pre Surgery<br>Exercises</b>\n          </button>\n          <button ion-button color="lorange" (click)="inhospitalinfo()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-hospital"></ion-icon><b>In Hospital<br>- Info</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="inhospitalvideo()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-videocamera"></ion-icon><b>Hospital Admission Process</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="aftersurgeryvideo()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-videocamera"></ion-icon><b>Post Surgery<br>Exercises</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="afterSurgeryinfo()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-aftersurgery"></ion-icon><b>Going Home<br>- Info</b>\n          </button>\n          <button ion-button color="lorange" (click)="todoList()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-todo"></ion-icon><b>To Do<br>Pre-surgery</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="stepcounter()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-stepcounter"></ion-icon><b>Step Counter</b>\n          </button>\n          <button text-wrap ion-button color="lorange" (click)="painlevel()" icon-left style="width: 47.9%; height: 65px; font-size: 14px">\n              <ion-icon name="custom-painlevel"></ion-icon><b>Pain Level</b>\n          </button>\n      </div>\n\n      <button ion-button clear block color="lyellow" (click)="logout()"><b>Log Out</b></button>\n\n\n      <button ion-button clear block color="lyellow" (click)="showtoggle()">show menu toggle</button>\n\n      <div id="feedbacklink" style="text-align: center">\n          <a href="https://docs.google.com/forms/d/e/1FAIpQLSfu0_KHNWkrMp1rVONVWf4MD78n15ArsL1V5mZ1D7LmHWjpJw/viewform?usp=sf_link" class="button large hpbottom">Please provide feedback here</a>\n      </div>\n\n\n  </ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_8__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavSummaryDetailsModule", function() { return NavSummaryDetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nav_summary_details__ = __webpack_require__(388);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavSummaryDetailsModule = /** @class */ (function () {
    function NavSummaryDetailsModule() {
    }
    NavSummaryDetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__nav_summary_details__["a" /* NavSummaryDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__nav_summary_details__["a" /* NavSummaryDetailsPage */]),
            ],
        })
    ], NavSummaryDetailsModule);
    return NavSummaryDetailsModule;
}());

//# sourceMappingURL=nav-summary-details.module.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavSummaryDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NavSummaryDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var NavSummaryDetailsPage = /** @class */ (function () {
    function NavSummaryDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NavSummaryDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NavSummaryDetailsPage');
    };
    NavSummaryDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nav-summary-details',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/nav-summary-details/nav-summary-details.html"*/'<!--\n  Generated template for the NavSummaryDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>nav-summary-details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/nav-summary-details/nav-summary-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], NavSummaryDetailsPage);
    return NavSummaryDetailsPage;
}());

//# sourceMappingURL=nav-summary-details.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavSummaryModule", function() { return NavSummaryModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nav_summary__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavSummaryModule = /** @class */ (function () {
    function NavSummaryModule() {
    }
    NavSummaryModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__nav_summary__["a" /* NavSummaryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__nav_summary__["a" /* NavSummaryPage */]),
            ],
        })
    ], NavSummaryModule);
    return NavSummaryModule;
}());

//# sourceMappingURL=nav-summary.module.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavSummaryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NavSummaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var NavSummaryPage = /** @class */ (function () {
    function NavSummaryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NavSummaryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NavSummaryPage');
    };
    NavSummaryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nav-summary',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/nav-summary/nav-summary.html"*/'<!--\n  Generated template for the NavSummaryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Nav-Summary</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/nav-summary/nav-summary.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], NavSummaryPage);
    return NavSummaryPage;
}());

//# sourceMappingURL=nav-summary.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Summary1Module", function() { return Summary1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__summary_1__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Summary1Module = /** @class */ (function () {
    function Summary1Module() {
    }
    Summary1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__summary_1__["a" /* Summary1Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__summary_1__["a" /* Summary1Page */]),
            ],
        })
    ], Summary1Module);
    return Summary1Module;
}());

//# sourceMappingURL=summary-1.module.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Summary2Module", function() { return Summary2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__summary_2__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Summary2Module = /** @class */ (function () {
    function Summary2Module() {
    }
    Summary2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__summary_2__["a" /* Summary2Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__summary_2__["a" /* Summary2Page */]),
            ],
        })
    ], Summary2Module);
    return Summary2Module;
}());

//# sourceMappingURL=summary-2.module.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Summary3Module", function() { return Summary3Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__summary_3__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Summary3Module = /** @class */ (function () {
    function Summary3Module() {
    }
    Summary3Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__summary_3__["a" /* Summary3Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__summary_3__["a" /* Summary3Page */]),
            ],
        })
    ], Summary3Module);
    return Summary3Module;
}());

//# sourceMappingURL=summary-3.module.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Summary4Module", function() { return Summary4Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__summary_4__ = __webpack_require__(396);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Summary4Module = /** @class */ (function () {
    function Summary4Module() {
    }
    Summary4Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__summary_4__["a" /* Summary4Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__summary_4__["a" /* Summary4Page */]),
            ],
        })
    ], Summary4Module);
    return Summary4Module;
}());

//# sourceMappingURL=summary-4.module.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Summary4Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Summary4Page = /** @class */ (function () {
    function Summary4Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.infoId = null;
        this.infoId = this.navParams.get('infoId');
    }
    Summary4Page.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    Summary4Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-summary-4',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-4/summary-4.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <ion-title>Long Term Care</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    Info ID: {{ infoId }}\n    <button ion-button full (click)="goBack()">Go Back!</button>\n</ion-content>'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/summary-4/summary-4.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], Summary4Page);
    return Summary4Page;
}());

//# sourceMappingURL=summary-4.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SummaryModule", function() { return SummaryModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__summary__ = __webpack_require__(141);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SummaryModule = /** @class */ (function () {
    function SummaryModule() {
    }
    SummaryModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__summary__["a" /* SummaryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__summary__["a" /* SummaryPage */]),
            ],
        })
    ], SummaryModule);
    return SummaryModule;
}());

//# sourceMappingURL=summary.module.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoPageModule", function() { return TodoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__todolist__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TodoPageModule = /** @class */ (function () {
    function TodoPageModule() {
    }
    TodoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__todolist__["a" /* TodoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__todolist__["a" /* TodoPage */]),
            ],
        })
    ], TodoPageModule);
    return TodoPageModule;
}());

//# sourceMappingURL=todolist.module.js.map

/***/ }),

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BeforeSurgeryTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BeforeSurgeryTabsPage tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */
var BeforeSurgeryTabsPage = /** @class */ (function () {
    function BeforeSurgeryTabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.videoRoot = 'BeforeSurgeryPage';
        this.docRoot = 'DocPage';
    }
    BeforeSurgeryTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-before-surgery-tabs',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/before-surgery-tabs/before-surgery-tabs.html"*/'<ion-tabs>\n    <ion-tab [root]="videoRoot" tabTitle="Video" tabIcon="custom-videocamera"></ion-tab>\n    <ion-tab [root]="docRoot" tabTitle="Doc" tabIcon="custom-doc"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/before-surgery-tabs/before-surgery-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], BeforeSurgeryTabsPage);
    return BeforeSurgeryTabsPage;
}());

//# sourceMappingURL=before-surgery-tabs.js.map

/***/ }),

/***/ 443:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(444);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(465);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(629);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_stepCounter_stepCounter__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_painLevel_contact__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_before_surgery_tabs_before_surgery_tabs__ = __webpack_require__(442);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__app_firebase_config__ = __webpack_require__(638);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_register_register__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_surgeon_menu_surgeon_menu__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_surgeon_home_surgeon_home__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_surgeon_home_tabs_surgeon_home_tabs__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_chart_chart__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_loading_loading__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_profile_profile_module__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_todolist_todolist__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_after_surgery_after_surgery__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pipes_pipes_module__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_before_surgery_before_surgery_module__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_todolist_todolist_module__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_rest_rest__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_in_hospital_in_hospital_module__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_after_surgery_after_surgery_module__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_nav_summary_nav_summary_module__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_nav_summary_details_nav_summary_details_module__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_nav_summary_nav_summary__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_nav_summary_details_nav_summary_details__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_summary_summary_module__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_summary_summary__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_summary_1_summary_1_module__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_summary_1_summary_1__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_summary_2_summary_2_module__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_summary_3_summary_3_module__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_summary_4_summary_4_module__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_summary_2_summary_2__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_summary_3_summary_3__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_summary_4_summary_4__ = __webpack_require__(396);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























// import {InHospitalPage} from "../pages/in-hospital/in-hospital";




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_painLevel_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_stepCounter_stepCounter__["a" /* StepCounterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_before_surgery_tabs_before_surgery_tabs__["a" /* BeforeSurgeryTabsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_surgeon_menu_surgeon_menu__["a" /* SurgeonMenuPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_surgeon_home_surgeon_home__["a" /* SurgeonHomePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_surgeon_home_tabs_surgeon_home_tabs__["a" /* SurgeonHomeTabsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_chart_chart__["a" /* ChartPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_loading_loading__["a" /* LoadingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/after-surgery/after-surgery.module#AfterSurgeryPageModule', name: 'AfterSurgeryPage', segment: 'after-surgery', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/anaesthetist-faq/anaesthetist-faq.module#AnaesthetistFaqPageModule', name: 'AnaesthetistFaqPage', segment: 'anaesthetist-faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/before-surgery-tabs/before-surgery-tabs.module#BeforeSurgeryTabsPageModule', name: 'BeforeSurgeryTabsPage', segment: 'before-surgery-tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/before-surgery/before-surgery.module#BeforeSurgeryPageModule', name: 'BeforeSurgeryPage', segment: 'before-surgery', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chart/chart.module#ChartPageModule', name: 'ChartPage', segment: 'chart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/in-hospital/in-hospital.module#InHospitalPageModule', name: 'InHospitalPage', segment: 'in-hospital', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loading/loading.module#LoadingPageModule', name: 'LoadingPage', segment: 'loading', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nav-summary-details/nav-summary-details.module#NavSummaryDetailsModule', name: 'NavSummaryDetailsPage', segment: 'nav-summary-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nav-summary/nav-summary.module#NavSummaryModule', name: 'NavSummaryPage', segment: 'nav-summary', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/summary-1/summary-1.module#Summary1Module', name: 'Summary1Page', segment: 'summary-1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/summary-2/summary-2.module#Summary2Module', name: 'Summary2Page', segment: 'summary-2', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/summary-3/summary-3.module#Summary3Module', name: 'Summary3Page', segment: 'summary-3', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/summary-4/summary-4.module#Summary4Module', name: 'Summary4Page', segment: 'summary-4', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/summary/summary.module#SummaryModule', name: 'SummaryPage', segment: 'summary', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/surgeon-home-tabs/surgeon-home-tabs.module#SurgeonHomeTabsPageModule', name: 'SurgeonHomeTabsPage', segment: 'surgeon-home-tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/surgeon-home/surgeon-home.module#SurgeonHomePageModule', name: 'SurgeonHomePage', segment: 'surgeon-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/surgeon-menu/surgeon-menu.module#SurgeonMenuPageModule', name: 'SurgeonMenuPage', segment: 'surgeon-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/todolist/todolist.module#TodoPageModule', name: 'TodoPage', segment: 'todolist', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_16__app_firebase_config__["a" /* FIREBASE_CONFIG */]),
                __WEBPACK_IMPORTED_MODULE_5_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_23__pages_profile_profile_module__["ProfilePageModule"],
                __WEBPACK_IMPORTED_MODULE_26__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_27__pages_before_surgery_before_surgery_module__["BeforeSurgeryPageModule"],
                __WEBPACK_IMPORTED_MODULE_28__pages_todolist_todolist_module__["TodoPageModule"],
                __WEBPACK_IMPORTED_MODULE_30__pages_in_hospital_in_hospital_module__["InHospitalPageModule"],
                __WEBPACK_IMPORTED_MODULE_31__pages_after_surgery_after_surgery_module__["AfterSurgeryPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_nav_summary_nav_summary_module__["NavSummaryModule"],
                __WEBPACK_IMPORTED_MODULE_33__pages_nav_summary_details_nav_summary_details_module__["NavSummaryDetailsModule"],
                __WEBPACK_IMPORTED_MODULE_36__pages_summary_summary_module__["SummaryModule"],
                __WEBPACK_IMPORTED_MODULE_38__pages_summary_1_summary_1_module__["Summary1Module"],
                __WEBPACK_IMPORTED_MODULE_40__pages_summary_2_summary_2_module__["Summary2Module"],
                __WEBPACK_IMPORTED_MODULE_41__pages_summary_3_summary_3_module__["Summary3Module"],
                __WEBPACK_IMPORTED_MODULE_42__pages_summary_4_summary_4_module__["Summary4Module"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_painLevel_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_stepCounter_stepCounter__["a" /* StepCounterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_before_surgery_tabs_before_surgery_tabs__["a" /* BeforeSurgeryTabsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_surgeon_menu_surgeon_menu__["a" /* SurgeonMenuPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_surgeon_home_surgeon_home__["a" /* SurgeonHomePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_surgeon_home_tabs_surgeon_home_tabs__["a" /* SurgeonHomeTabsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_chart_chart__["a" /* ChartPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_loading_loading__["a" /* LoadingPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_todolist_todolist__["a" /* TodoPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_after_surgery_after_surgery__["a" /* AfterSurgeryPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_nav_summary_nav_summary__["a" /* NavSummaryPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_nav_summary_details_nav_summary_details__["a" /* NavSummaryDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_summary_summary__["a" /* SummaryPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_summary_1_summary_1__["a" /* Summary1Page */],
                __WEBPACK_IMPORTED_MODULE_43__pages_summary_2_summary_2__["a" /* Summary2Page */],
                __WEBPACK_IMPORTED_MODULE_44__pages_summary_3_summary_3__["a" /* Summary3Page */],
                __WEBPACK_IMPORTED_MODULE_45__pages_summary_4_summary_4__["a" /* Summary4Page */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_29__providers_rest_rest__["a" /* RestProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YoutubePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var YoutubePipe = /** @class */ (function () {
    function YoutubePipe(dom) {
        this.dom = dom;
    }
    YoutubePipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return this.dom.bypassSecurityTrustResourceUrl(value);
    };
    YoutubePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'youtube',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], YoutubePipe);
    return YoutubePipe;
}());

//# sourceMappingURL=youtube.js.map

/***/ }),

/***/ 590:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 267,
	"./af.js": 267,
	"./ar": 268,
	"./ar-dz": 269,
	"./ar-dz.js": 269,
	"./ar-kw": 270,
	"./ar-kw.js": 270,
	"./ar-ly": 271,
	"./ar-ly.js": 271,
	"./ar-ma": 272,
	"./ar-ma.js": 272,
	"./ar-sa": 273,
	"./ar-sa.js": 273,
	"./ar-tn": 274,
	"./ar-tn.js": 274,
	"./ar.js": 268,
	"./az": 275,
	"./az.js": 275,
	"./be": 276,
	"./be.js": 276,
	"./bg": 277,
	"./bg.js": 277,
	"./bn": 278,
	"./bn.js": 278,
	"./bo": 279,
	"./bo.js": 279,
	"./br": 280,
	"./br.js": 280,
	"./bs": 281,
	"./bs.js": 281,
	"./ca": 282,
	"./ca.js": 282,
	"./cs": 283,
	"./cs.js": 283,
	"./cv": 284,
	"./cv.js": 284,
	"./cy": 285,
	"./cy.js": 285,
	"./da": 286,
	"./da.js": 286,
	"./de": 287,
	"./de-at": 288,
	"./de-at.js": 288,
	"./de-ch": 289,
	"./de-ch.js": 289,
	"./de.js": 287,
	"./dv": 290,
	"./dv.js": 290,
	"./el": 291,
	"./el.js": 291,
	"./en-au": 292,
	"./en-au.js": 292,
	"./en-ca": 293,
	"./en-ca.js": 293,
	"./en-gb": 294,
	"./en-gb.js": 294,
	"./en-ie": 295,
	"./en-ie.js": 295,
	"./en-nz": 296,
	"./en-nz.js": 296,
	"./eo": 297,
	"./eo.js": 297,
	"./es": 298,
	"./es-do": 299,
	"./es-do.js": 299,
	"./es.js": 298,
	"./et": 300,
	"./et.js": 300,
	"./eu": 301,
	"./eu.js": 301,
	"./fa": 302,
	"./fa.js": 302,
	"./fi": 303,
	"./fi.js": 303,
	"./fo": 304,
	"./fo.js": 304,
	"./fr": 305,
	"./fr-ca": 306,
	"./fr-ca.js": 306,
	"./fr-ch": 307,
	"./fr-ch.js": 307,
	"./fr.js": 305,
	"./fy": 308,
	"./fy.js": 308,
	"./gd": 309,
	"./gd.js": 309,
	"./gl": 310,
	"./gl.js": 310,
	"./gom-latn": 311,
	"./gom-latn.js": 311,
	"./he": 312,
	"./he.js": 312,
	"./hi": 313,
	"./hi.js": 313,
	"./hr": 314,
	"./hr.js": 314,
	"./hu": 315,
	"./hu.js": 315,
	"./hy-am": 316,
	"./hy-am.js": 316,
	"./id": 317,
	"./id.js": 317,
	"./is": 318,
	"./is.js": 318,
	"./it": 319,
	"./it.js": 319,
	"./ja": 320,
	"./ja.js": 320,
	"./jv": 321,
	"./jv.js": 321,
	"./ka": 322,
	"./ka.js": 322,
	"./kk": 323,
	"./kk.js": 323,
	"./km": 324,
	"./km.js": 324,
	"./kn": 325,
	"./kn.js": 325,
	"./ko": 326,
	"./ko.js": 326,
	"./ky": 327,
	"./ky.js": 327,
	"./lb": 328,
	"./lb.js": 328,
	"./lo": 329,
	"./lo.js": 329,
	"./lt": 330,
	"./lt.js": 330,
	"./lv": 331,
	"./lv.js": 331,
	"./me": 332,
	"./me.js": 332,
	"./mi": 333,
	"./mi.js": 333,
	"./mk": 334,
	"./mk.js": 334,
	"./ml": 335,
	"./ml.js": 335,
	"./mr": 336,
	"./mr.js": 336,
	"./ms": 337,
	"./ms-my": 338,
	"./ms-my.js": 338,
	"./ms.js": 337,
	"./my": 339,
	"./my.js": 339,
	"./nb": 340,
	"./nb.js": 340,
	"./ne": 341,
	"./ne.js": 341,
	"./nl": 342,
	"./nl-be": 343,
	"./nl-be.js": 343,
	"./nl.js": 342,
	"./nn": 344,
	"./nn.js": 344,
	"./pa-in": 345,
	"./pa-in.js": 345,
	"./pl": 346,
	"./pl.js": 346,
	"./pt": 347,
	"./pt-br": 348,
	"./pt-br.js": 348,
	"./pt.js": 347,
	"./ro": 349,
	"./ro.js": 349,
	"./ru": 350,
	"./ru.js": 350,
	"./sd": 351,
	"./sd.js": 351,
	"./se": 352,
	"./se.js": 352,
	"./si": 353,
	"./si.js": 353,
	"./sk": 354,
	"./sk.js": 354,
	"./sl": 355,
	"./sl.js": 355,
	"./sq": 356,
	"./sq.js": 356,
	"./sr": 357,
	"./sr-cyrl": 358,
	"./sr-cyrl.js": 358,
	"./sr.js": 357,
	"./ss": 359,
	"./ss.js": 359,
	"./sv": 360,
	"./sv.js": 360,
	"./sw": 361,
	"./sw.js": 361,
	"./ta": 362,
	"./ta.js": 362,
	"./te": 363,
	"./te.js": 363,
	"./tet": 364,
	"./tet.js": 364,
	"./th": 365,
	"./th.js": 365,
	"./tl-ph": 366,
	"./tl-ph.js": 366,
	"./tlh": 367,
	"./tlh.js": 367,
	"./tr": 368,
	"./tr.js": 368,
	"./tzl": 369,
	"./tzl.js": 369,
	"./tzm": 370,
	"./tzm-latn": 371,
	"./tzm-latn.js": 371,
	"./tzm.js": 370,
	"./uk": 372,
	"./uk.js": 372,
	"./ur": 373,
	"./ur.js": 373,
	"./uz": 374,
	"./uz-latn": 375,
	"./uz-latn.js": 375,
	"./uz.js": 374,
	"./vi": 376,
	"./vi.js": 376,
	"./x-pseudo": 377,
	"./x-pseudo.js": 377,
	"./yo": 378,
	"./yo.js": 378,
	"./zh-cn": 379,
	"./zh-cn.js": 379,
	"./zh-hk": 380,
	"./zh-hk.js": 380,
	"./zh-tw": 381,
	"./zh-tw.js": 381
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 590;

/***/ }),

/***/ 629:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(441);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = 'LoginPage';
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 638:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CONFIG; });
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyAnyuUC9FacmUmoUIX2axmHhB2lpmuDwpg",
    authDomain: "orthoapp-4a103.firebaseapp.com",
    databaseURL: "https://orthoapp-4a103.firebaseio.com",
    projectId: "orthoapp-4a103",
    storageBucket: "orthoapp-4a103.appspot.com",
    messagingSenderId: "984859273022"
};
//# sourceMappingURL=app.firebase.config.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactPage = /** @class */ (function () {
    function ContactPage(rest, afAuth, afDatabase, navCtrl, narParams) {
        this.rest = rest;
        this.afAuth = afAuth;
        this.afDatabase = afDatabase;
        this.navCtrl = navCtrl;
        this.narParams = narParams;
        this.painlevel = {};
        this.medication = {};
        // feedBack={} as Feedback;
        this.exercise = {};
        this.shownGroup = null;
        this.showMe = false;
        this.data_viewed = "7";
        this.time_format = 'DD/MM/YY HH:mm';
        this.period = 4;
        this.exercise_times = [0, 1, 2, 3, 4, 5];
        this.medication.Take = false;
        this.token = this.rest.getToken();
        this.data = this.narParams.get('userid'); // get the user data passed from the surgeon page
        console.log("painLevel page" + this.data);
        var currentUser = this.afAuth.auth.currentUser;
        if (this.data == null) {
            this.userID = currentUser.uid;
            this.surgeonCheck = false;
        }
        else {
            this.userID = this.data;
            this.surgeonCheck = true;
        }
        console.log(this.userID);
        window.addEventListener('orientationchange', this.detectOrient);
    }
    ContactPage.prototype.submitExercise = function () {
        var _this = this;
        this.histChart_exercise.destroy();
        this.afAuth.authState.take(1).subscribe(function (auth) {
            var dateTime = new Date();
            dateTime.setSeconds(0);
            dateTime.setMilliseconds(0);
            dateTime.setMinutes(0);
            dateTime.setHours(0);
            var timestamp = Math.floor(dateTime.getTime() / 1000);
            var date = timestamp;
            _this.afDatabase.object("Exercise/" + _this.userID + "/" + date).set(_this.exercise);
        });
    };
    ContactPage.prototype.submit = function () {
        var _this = this;
        this.lineChart_pain.destroy();
        this.lineChart_medic.destroy();
        this.afAuth.authState.take(1).subscribe(function (auth) {
            var dateTime = new Date();
            dateTime.setSeconds(0);
            dateTime.setMilliseconds(0);
            console.log(dateTime);
            var timestamp = Math.floor(dateTime.getTime() / 1000);
            var date = timestamp;
            if (_this.medication.medicalName == null) {
                _this.medication.medicalName = 'None';
            }
            _this.afDatabase.object("PainLevel/" + auth.uid + "/" + date).set(_this.painlevel);
            _this.afDatabase.object("Medication/" + auth.uid + "/" + date).set(_this.medication); //write data into firebase
        });
    };
    // uploadFeedback(){
    //     this.afAuth.authState.take(1).subscribe(auth=>{
    //         this.afDatabase.object(`Feedback/${this.data}`).set(this.feedBack).then(()=>alert("Uploaded !"));
    //     })
    // }
    ContactPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    ;
    ContactPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    ContactPage.prototype.hide = function () {
        if (this.showMe == true) {
            this.showMe = false;
        }
        else {
            this.showMe = true;
        }
    };
    ContactPage.prototype.createChart = function (type) {
        var _this = this;
        this.userRef_pain = this.afDatabase.database.ref("PainLevel/" + this.userID);
        this.userRef_medic = this.afDatabase.database.ref("Medication/" + this.userID);
        this.userRef_exe = this.afDatabase.database.ref("Exercise/" + this.userID);
        this.Pain = this.afDatabase.list("PainLevel/" + this.userID);
        this.Medic = this.afDatabase.list("Medication/" + this.userID);
        this.Exe = this.afDatabase.list("Exercise/" + this.userID);
        // this.FeedBack = this.afDatabase.object(`Feedback/${this.userID}`);
        this.userRef_pain.on('value', function (data) {
            var Array = [];
            var paindata = [];
            var timeArray = [];
            var seven_arrary = [];
            var seven_pain = [];
            var seven_time = [];
            var thirty_arrary = [];
            var thirty_pain = [];
            var thirty_time = [];
            data.forEach(function (data) {
                var key = data.key;
                _this.get_week_data(data, key, seven_arrary, seven_time, seven_pain, 5);
                _this.get_month_data(data, key, thirty_arrary, thirty_time, thirty_pain, 5);
                var key_time = __WEBPACK_IMPORTED_MODULE_5_moment__["unix"](key).format(_this.time_format);
                console.log(key_time);
                console.log(key * 1000);
                Array.push(key);
                timeArray.push(key_time);
                paindata.push(data.val().painLevel);
                return false;
            });
            if (type == "7") {
                _this.Time_pain = seven_arrary;
                _this.paindata = seven_pain;
                _this.Time_on_chart = seven_time;
            }
            else if (type == "30") {
                _this.Time_pain = thirty_arrary;
                _this.paindata = thirty_pain;
                _this.Time_on_chart = thirty_time;
            }
            else {
                _this.Time_pain = Array;
                _this.paindata = paindata;
                _this.Time_on_chart = timeArray;
            }
            console.log("paindata =" + paindata);
            console.log("success here!");
            console.log(_this.lineCanvas_pain);
            _this.lineChart_pain = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](_this.lineCanvas_pain.nativeElement, {
                type: 'bar',
                options: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Pain Level',
                        fontSize: 18
                    },
                    scales: {
                        xAxes: [{
                                display: _this.detectmob() || _this.detectOrient()
                            }]
                    }
                },
                data: {
                    datasets: [
                        {
                            fillColor: "rgba(220,220,220,0.5)",
                            strokeColor: "rgba(220,220,220,1)",
                            data: _this.paindata,
                        }
                    ],
                    labels: _this.Time_on_chart
                }
            });
        });
        this.userRef_medic.on('value', function (data) {
            var medicdata = [];
            var time = [];
            var seven_Array = [];
            var seven_Time = [];
            var thirty_Array = [];
            var thirty_Time = [];
            data.forEach(function (data) {
                var key = data.key;
                _this.get_week_data(data, key, seven_Array, seven_Time, null, 3);
                _this.get_month_data(data, key, thirty_Array, thirty_Time, null, 3);
                medicdata.push(data.val().Take);
                time.push(__WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format(_this.time_format));
                return false;
            });
            if (type == "7") {
                _this.Time_medic = seven_Time;
                _this.medicdata = seven_Array;
            }
            else if (type == "30") {
                _this.Time_medic = thirty_Time;
                _this.medicdata = thirty_Array;
            }
            else {
                _this.Time_medic = time;
                _this.medicdata = medicdata;
            }
            console.log("success here!");
            console.log(_this.lineCanvas_medic);
            _this.lineChart_medic = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](_this.lineCanvas_medic.nativeElement, {
                type: 'line',
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    max: 1,
                                    min: 0,
                                    stepSize: 1
                                }
                            }],
                        xAxes: [{
                                display: _this.detectmob() || _this.detectOrient()
                            }]
                    },
                    title: {
                        display: true,
                        text: ' Medicine taken (1- yes, 0 - no)',
                        fontSize: 18
                    }
                },
                data: {
                    labels: _this.Time_medic,
                    datasets: [
                        {
                            backgroundColor: "rgba(75,192,192,0.4)",
                            borderColor: "rgba(75,192,192,1)",
                            pointBorderColor: "rgba(75,192,192,1)",
                            pointBackgroundColor: "#fff",
                            pointHoverBackgroundColor: "rgba(75,192,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            data: _this.medicdata,
                            spanGaps: true
                        }
                    ]
                }
            });
        });
    };
    ContactPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.createChart(this.data_viewed);
        this.loadExercise();
        this.userRef_pain.on('value', function (data) {
            var dateTime = new Date();
            var timestamp = Math.floor(dateTime.getTime() / 1000);
            var date = timestamp;
            var pain = [];
            data.forEach(function (data) {
                var key = data.key;
                var hours = Math.floor(((date - key) * 1000) / (3600));
                // console.log("minutes: "+hours);
                if ((hours <= _this.period) && (data.val().painLevel >= 7)) {
                    console.log();
                    pain.push(data.val().painLevel);
                }
                return false;
            });
            var len = pain.length;
            console.log(len);
            console.log(pain);
            if ((pain[len - 1] >= 7) && (pain[len - 2] >= 7)) {
                alert("Continue high pain Level");
            }
        });
        console.log(this.userID);
        if (this.surgeonCheck) {
            this.afDatabase.object("Exercise/" + this.userID + "/" + this.today).subscribe(function (data) {
                if (data) {
                    _this.exercise = data;
                }
            });
        }
        // let array = [];
        // this.rest.getCONSTData(this.token, '/Medication_Name').subscribe(data =>{
        //     console.log(data);
        //     array.push(data);
        //     this.medication_Name = array;
        // });
        if (this.surgeonCheck) {
            this.user_step = this.afDatabase.database.ref("Steps/" + this.userID);
            this.user_step.on('value', function (data) {
                var timeArray = [];
                var stepArray = [];
                data.forEach(function (data) {
                    var key = data.key;
                    var key_time = __WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format(_this.time_format);
                    timeArray.push(key_time);
                    stepArray.push(data.val().steps);
                    return false;
                });
                _this.step_time = timeArray;
                _this.steps = stepArray;
                _this.barChart_step = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](_this.barCanvas_step.nativeElement, {
                    type: 'bar',
                    options: {
                        legend: {
                            display: false
                        }
                    },
                    data: {
                        datasets: [
                            {
                                fillColor: "rgba(220,220,220,0.5)",
                                strokeColor: "rgba(220,220,220,1)",
                                data: _this.steps,
                            }
                        ],
                        labels: _this.step_time
                    }
                });
            });
        }
        ;
    };
    ContactPage.prototype.getWeek = function (day) {
        var date = new Date();
        var today = date.getDay();
        var stepSunday = -today + 1;
        if (today == 0) {
            stepSunday = -7;
        }
        var stepMonday = 7 - today;
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        var time = date.getTime();
        var monday = Math.floor((time + stepSunday * 24 * 3600 * 1000) / 1000);
        var sunday = Math.floor((time + stepMonday * 24 * 3600 * 1000) / 1000);
        if (day == 'Start') {
            return monday;
        }
        else if (day == 'End') {
            return sunday;
        }
        else {
            return null;
        }
    };
    ContactPage.prototype.getMonth = function (month) {
        var firstDate = new Date();
        firstDate.setDate(1);
        var endDate = new Date(firstDate);
        endDate.setMonth(firstDate.getMonth() + 1);
        endDate.setDate(0);
        firstDate.setHours(0);
        firstDate.setMinutes(0);
        firstDate.setSeconds(0);
        firstDate.setMilliseconds(0);
        endDate.setHours(0);
        endDate.setMinutes(0);
        endDate.setSeconds(0);
        endDate.setMilliseconds(0);
        var start = firstDate.getTime();
        var end = endDate.getTime();
        start = Math.floor(start / 1000);
        end = Math.floor(end / 1000);
        if (month == "Start") {
            return start;
        }
        if (month == "End") {
            return end;
        }
    };
    ContactPage.prototype.seven_day_data = function () {
        var type = "7";
        this.data_viewed = type;
        if (this.lineChart_pain != null)
            this.lineChart_pain.destroy();
        this.lineChart_medic.destroy();
        this.createChart(type);
    };
    ContactPage.prototype.thirty_day_data = function () {
        var type = "30";
        this.data_viewed = type;
        this.lineChart_pain.destroy();
        this.lineChart_medic.destroy();
        this.createChart(type);
    };
    ContactPage.prototype.all_data = function () {
        var type = "all";
        this.data_viewed = type;
        this.lineChart_pain.destroy();
        this.lineChart_medic.destroy();
        this.createChart(type);
    };
    ContactPage.prototype.get_week_data = function (data, key, array, time, pain, num) {
        if (num == 3) {
            if (key >= this.getWeek('Start') && key <= this.getWeek('End')) {
                array.push(data.val().Take);
                time.push(__WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format(this.time_format));
            }
        }
        if (num == 5) {
            if (key >= this.getWeek('Start') && key <= this.getWeek('End')) {
                array.push(key);
                time.push(__WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format(this.time_format));
                pain.push(data.val().painLevel);
            }
        }
    };
    ContactPage.prototype.get_month_data = function (data, key, array, time, pain, num) {
        if (num == 3) {
            if (key >= this.getMonth('Start') && key <= this.getMonth('End')) {
                array.push(data.val().Take);
                time.push(__WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format(this.time_format));
            }
        }
        if (num == 5) {
            if (key >= this.getMonth('Start') && key <= this.getMonth('End')) {
                array.push(key);
                time.push(__WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format(this.time_format));
                pain.push(data.val().painLevel);
            }
        }
    };
    // getExerciseData(val,Array,H_post_c,H_pre_c,K_post_c,K_pre_c,Reco){
    //     if(val == this.exercise_Name[0]){
    //         H_post_c = H_post_c +1;
    //         Array[0] = H_post_c;
    //     }else if(val == this.exercise_Name[1]){
    //         H_pre_c = H_pre_c +1;
    //         Array[1] = H_pre_c;
    //     }else if(val == this.exercise_Name[2]){
    //         K_pre_c = K_pre_c +1;
    //         Array[2] = K_pre_c;
    //     }else if(val == this.exercise_Name[3]){
    //         K_post_c = K_post_c + 1;
    //         Array[3] = K_post_c;
    //     }else if(val == this.exercise_Name[4]){
    //         Reco = Reco +1;
    //         Array[4] = Reco;
    //     }
    // }
    ContactPage.prototype.detectmob = function () {
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)) {
            return false;
        }
        else {
            return true;
        }
    };
    ContactPage.prototype.detectOrient = function () {
        if (window.innerHeight >= window.innerWidth) {
            return false;
        }
        else {
            return true;
        }
    };
    ContactPage.prototype.loadExercise = function () {
        var _this = this;
        this.userRef_exe.on('value', function (data) {
            var Array = [];
            var time = [];
            // let Array = [0,0,0,0,0];
            // let H_post_c = 0;
            // let H_pre_c = 0;
            // let K_pre_c = 0;
            // let K_post_c = 0;
            // let Reco = 0;
            data.forEach(function (data) {
                var key = data.key;
                var val = data.val().Times;
                Array.push(val);
                time.push(__WEBPACK_IMPORTED_MODULE_5_moment__(key * 1000).format('DD/MM/YYYY'));
                // if(val == this.exercise_Name[0]){
                //     H_post_c = H_post_c +1;
                //     Array[0] = H_post_c;
                // }else if(val == this.exercise_Name[1]){
                //     H_pre_c = H_pre_c +1;
                //     Array[1] = H_pre_c;
                // }else if(val == this.exercise_Name[2]){
                //     K_pre_c = K_pre_c +1;
                //     Array[2] = K_pre_c;
                // }else if(val == this.exercise_Name[3]){
                //     K_post_c = K_post_c + 1;
                //     Array[3] = K_post_c;
                // }else if(val == this.exercise_Name[4]){
                //     Reco = Reco +1;
                //     Array[4] = Reco;
                // }
                // console.log(Array);
                return false;
            });
            _this.exedata = Array;
            _this.histChart_exercise = new __WEBPACK_IMPORTED_MODULE_4_chart_js__["Chart"](_this.HistCanvas.nativeElement, {
                type: 'bar',
                options: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Exercises completed',
                        fontSize: 18
                    },
                    scales: {
                        xAxes: [{
                                display: _this.detectmob() || _this.detectOrient()
                            }],
                        yAxes: [{
                                ticks: {
                                    stepSize: 1
                                }
                            }],
                    }
                },
                data: {
                    datasets: [
                        {
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.6)',
                                'rgba(255, 206, 86, 0.6)',
                                'rgba(75, 192, 192, 0.6)',
                                'rgba(153, 102, 255, 0.6)',
                                'rgba(255, 159, 64, 0.6)'
                            ],
                            data: _this.exedata,
                        }
                    ],
                    labels: time
                }
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('painLevel'),
        __metadata("design:type", Object)
    ], ContactPage.prototype, "lineCanvas_pain", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('medic'),
        __metadata("design:type", Object)
    ], ContactPage.prototype, "lineCanvas_medic", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('steps_barChart'),
        __metadata("design:type", Object)
    ], ContactPage.prototype, "barCanvas_step", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('exercise_chart'),
        __metadata("design:type", Object)
    ], ContactPage.prototype, "HistCanvas", void 0);
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/painLevel/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title *ngIf="!surgeonCheck">Record your pain level</ion-title>\n    <ion-title *ngIf="surgeonCheck">Patient Pain Record</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card *ngIf="!surgeonCheck">\n    <div id="painlevel">\n      <ion-card-content>\n        <h2>Pain Level</h2>\n      <p>How much pain do you have now ?</p>\n      <ion-item>\n        <ion-range min="0" max="10" [(ngModel)]="painlevel.painLevel" color="danger" pin="true" snaps="true">\n          <ion-label  range-left>None</ion-label>\n          <ion-label range-right>Extreme</ion-label>\n        </ion-range>\n      </ion-item>\n    </ion-card-content>\n    </div>\n\n    <div id="medicine">\n      <h2>Medicine</h2>\n\n      <ion-grid fixed >\n        <ion-row align-items-start>\n          <ion-col><p>Have you take your prescribed pain killers</p></ion-col>\n        </ion-row>\n        <ion-row align-items-center >\n          <ion-col>\n            <ion-item text-wrap>\n              <ion-label><p>Yes</p></ion-label>\n              <ion-checkbox color="linegreen" [(ngModel)]="medication.Take"></ion-checkbox>\n            </ion-item>\n          </ion-col>\n          <!--</ion-col>-->\n          <!--<ion-col>Yes</ion-col>-->\n          <ion-col text-wrap>\n            <ion-item>\n              <ion-label><p>No</p></ion-label>\n              <ion-checkbox color="linegreen" ></ion-checkbox>\n            </ion-item>\n          </ion-col>\n          <!--</ion-col>-->\n          <!--<ion-col>No</ion-col>-->\n        </ion-row>\n      </ion-grid>\n    </div>\n\n\n\n\n\n    <!--<ion-item>-->\n    <!--<ion-label>Medicine Name</ion-label>-->\n    <!--<ion-select [(ngModel)]="medication.medicalName">-->\n    <!--<ion-option *ngFor="let med of medication_Name" [value]="med">{{med}}</ion-option>-->\n    <!--</ion-select>-->\n    <!--</ion-item>-->\n\n\n    <button ion-button clear block color="lyellow" (click)="submit()"><b>Submit</b></button>\n  </ion-card>\n\n  <div id="exercise">\n    <h2>Exercise</h2>\n    <ion-grid>\n      <ion-row align-items-start>\n        <ion-col><p>How many times have you done your pre-op/ post-op exercises today?</p></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-item>\n          <ion-select [(ngModel)]="exercise.Times">\n            <ion-option *ngFor="let exe of exercise_times" [value]="exe">{{exe}}</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-row>\n      <!--<ion-row align-items-start *ngIf="!surgeonCheck">-->\n      <!--<ion-col><p>Comment: </p></ion-col>-->\n      <!--<ion-col><ion-input type="text"  placeholder="write comment here" [(ngModel)]="exercise.comment" ></ion-input></ion-col>-->\n      <!--</ion-row>-->\n      <button *ngIf="!surgeonCheck" ion-button clear block color="lyellow" (click)="submitExercise()">\n        <b>Submit Exercise</b>\n      </button>\n      <!-- <ion-row *ngIf="surgeonCheck"><ion-col>Comment: {{exercise.comment}}</ion-col></ion-row>-->\n    </ion-grid>\n  </div>\n\n  <ion-item-divider></ion-item-divider>\n\n  <ion-item fixed>\n    <div><canvas #painLevel></canvas> </div>\n    <br/>\n  </ion-item>\n\n  <ion-item-divider>    <ion-row>\n    <ion-segment color="lyellow">\n      <ion-segment-button value="All" (click)="all_data()">\n        All\n      </ion-segment-button>\n      <ion-segment-button value="Month" (click)="thirty_day_data()">\n        Month\n      </ion-segment-button>\n      <ion-segment-button value="Week" (click)="seven_day_data()">\n        Week\n      </ion-segment-button>\n    </ion-segment>\n  </ion-row></ion-item-divider>\n\n  <ion-item>\n    <div><canvas #medic height="90%"></canvas> </div>\n  </ion-item>\n\n<ion-item-divider></ion-item-divider>\n\n  <ion-item>\n    <div><canvas #exercise_chart></canvas> </div>\n  </ion-item>\n\n  <ion-item *ngIf="surgeonCheck">\n    <ion-card-title>Steps</ion-card-title>\n    <div><canvas #steps_barChart></canvas> </div>\n  </ion-item>\n\n  <!--<ion-item>-->\n  <!--<ion-title>Feedback From Surgeon</ion-title>-->\n  <!--<br/>-->\n  <!--<br/>-->\n  <!--<ion-row><ion-col>{{(FeedBack | async)?.comment}}</ion-col></ion-row>-->\n  <!--<br/>-->\n  <!--<ion-input *ngIf="surgeonCheck" type="text"  placeholder="write feedback here" [(ngModel)]="feedBack.comment" ></ion-input>-->\n  <!--</ion-item>-->\n  <!--<button *ngIf="surgeonCheck" ion-button clear block color="lyellow" (click)="uploadFeedback()"><b>upload</b></button>-->\n\n  <!--<ion-grid >-->\n  <!--<ion-row *ngFor="let pain of Pain |async let i = index">-->\n  <!--<ion-col>{{Time_pain[i] * 1000  | date:\'yyyy-MM-dd HH:mm:ss\'}}</ion-col>-->\n  <!--<ion-col> Pain: {{pain.painLevel}}</ion-col>-->\n  <!--</ion-row>-->\n  <!--</ion-grid>-->\n\n  <div id="detail">\n    <ion-list>\n      <button ion-button clear block color="lyellow" (click)="hide()"><b>Show Detail Information</b></button>\n      <div *ngIf="showMe">\n        <ion-item *ngFor="let pain of Pain |async let i = index"  text-wrap (click)="toggleGroup(i)" [ngClass]="{active: isGroupShown(i)}">\n\n          <div id="div_painDetail">{{Time_pain[i] * 1000  | date:\'dd-MMM-yy HH:mm\'}}</div>\n          <h3>Pain level: {{pain.painLevel}}</h3>\n\n        </ion-item>\n      </div>\n    </ion-list>\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/painLevel/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_rest_rest__["a" /* RestProvider */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_apiAddr__ = __webpack_require__(384);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the RestProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var RestProvider = /** @class */ (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiURL = __WEBPACK_IMPORTED_MODULE_3__app_apiAddr__["a" /* API_ADDR */].firebaseAPI;
        this.cache = null;
        console.log('Hello RestProvider Provider');
    }
    RestProvider.prototype.getData = function (uid, token, route) {
        console.log(this.apiURL + route + uid + '.json?auth=' + token);
        return this.http.get(this.apiURL + route + uid + '.json?auth=' + token)
            .map(function (res) { return res.json(); });
    };
    RestProvider.prototype.setToken = function (token) {
        this.idToken = token;
    };
    RestProvider.prototype.getToken = function () {
        return this.idToken;
    };
    RestProvider.prototype.setProfileCache = function (content) {
        this.cache = content;
    };
    RestProvider.prototype.getProfileCache = function () {
        return this.cache;
    };
    RestProvider.prototype.emptyProfileCache = function () {
        this.cache = null;
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes_youtube_youtube__ = __webpack_require__(490);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__pipes_youtube_youtube__["a" /* YoutubePipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__pipes_youtube_youtube__["a" /* YoutubePipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__stepCounter_stepCounter__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__painLevel_contact__ = __webpack_require__(69);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MenuPage = /** @class */ (function () {
    function MenuPage(navCtrl, navParams, menu) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */];
        this.pages = [
            { title: 'Home', pageName: __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], tabComponent: 'HomePage', icon: 'custom-home' },
            //{title: 'Before Surgery' , pageName: BeforeSurgeryTabsPage,tabComponent:'BeforeSurgeryPage',icon:'custom-videocamera'},
            //TODO: need an icon for summaries
            { title: 'Information Summary', pageName: 'SummaryPage', tabComponent: 'SummaryPage', icon: 'custom-doc' },
            { title: 'Pre-surgery Exercises', pageName: 'BeforeSurgeryPage', tabComponent: 'BeforeSurgeryPage', icon: 'custom-videocamera' },
            //before surgery documentation here
            { title: 'Before Surgery Info', pageName: 'Summary1Page', tabComponent: 'Summary1Page', icon: 'custom-beforesurgeryinfo' },
            //Anaesthetist FAQ video
            { title: 'Anaesthetist FAQ', pageName: 'AnaesthetistFaqPage', tabComponent: 'AnaesthetistFaqPage', icon: 'custom-videocamera' },
            { title: 'Hospital Admission', pageName: 'InHospitalPage', tabComponent: 'InHospitalPage', icon: 'custom-videocamera' },
            //in hospital documentation here
            { title: 'In Hospital Info', pageName: 'Summary2Page', tabComponent: 'Summary2Page', icon: 'custom-hospital' },
            { title: 'Post-surgery Exercises', pageName: 'AfterSurgeryPage', tabComponent: 'AfterSurgeryPage', icon: 'custom-videocamera' },
            //after surgery documentation here
            { title: 'Going Home Info', pageName: 'Summary3Page', tabComponent: 'Summary3Page', icon: 'custom-aftersurgery' },
            { title: 'Pre-surgery To Do List', pageName: 'TodoPage', tabComponent: 'TodoPage', icon: 'custom-todo' },
            { title: 'Step Counter', pageName: __WEBPACK_IMPORTED_MODULE_3__stepCounter_stepCounter__["a" /* StepCounterPage */], tabComponent: 'StepCounterPage', icon: 'custom-stepcounter' },
            { title: 'Pain Level', pageName: __WEBPACK_IMPORTED_MODULE_4__painLevel_contact__["a" /* ContactPage */], tabComponent: 'ContactPage', icon: 'custom-painlevel' },
        ];
        this.menu.enable(true);
    }
    MenuPage.prototype.openPage = function (page) {
        var params = {};
        this.nav.setRoot(page.pageName, params);
    };
    MenuPage.prototype.isActive = function (page) {
        var childNav = this.nav.getActiveChildNav();
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }
        if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
            return 'primary';
        }
        return;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MenuPage.prototype, "nav", void 0);
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/menu/menu.html"*/'<ion-menu [content]="Content" persistent="true" id="main-menu">\n  <ion-header>\n    <ion-navbar>\n      <ion-buttons left>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title>Menu</ion-title>\n    </ion-navbar>\n  </ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item menuClose color="lyellow" *ngFor="let p of pages" (click)="openPage(p)">\n      <ion-icon item-start [name]="p.icon" [color]="isActive(p)"></ion-icon>\n      {{p.title}}\n    </button>\n\n  </ion-list>\n\n</ion-content>\n\n</ion-menu>\n<ion-nav [root]="rootPage" #Content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/menu/menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StepCounterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_chart_js__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StepCounterPage = /** @class */ (function () {
    function StepCounterPage(navCtrl, afAuth, afDatabase, rest) {
        this.navCtrl = navCtrl;
        this.afAuth = afAuth;
        this.afDatabase = afDatabase;
        this.rest = rest;
        this.step_counter = {};
        this.rangeofmotion = {};
        var dateTime = new Date();
        dateTime.setHours(0);
        dateTime.setMinutes(0);
        dateTime.setSeconds(0);
        dateTime.setMilliseconds(0);
        var timestamp = Math.floor(dateTime.getTime() / 1000);
        this.today = timestamp;
    }
    StepCounterPage.prototype.submit_steps = function () {
        var _this = this;
        this.afAuth.authState.take(1).subscribe(function (auth) {
            var date = _this.today;
            _this.afDatabase.object("Steps/" + auth.uid + "/" + date).update(_this.step_counter);
        });
    };
    StepCounterPage.prototype.submit_rangeofmotion = function () {
        var _this = this;
        this.afAuth.authState.take(1).subscribe(function (auth) {
            var date = _this.today;
            _this.afDatabase.object("RangeofMotion/" + auth.uid + "/" + date).set(_this.rangeofmotion).then(function () { return alert("uploaded"); });
        });
    };
    StepCounterPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var userID = this.afAuth.auth.currentUser.uid;
        this.user_step = this.afDatabase.database.ref("Steps/" + userID);
        this.user_step.on('value', function (data) {
            var timeArray = [];
            var stepArray = [];
            data.forEach(function (data) {
                var key = data.key;
                var key_time = __WEBPACK_IMPORTED_MODULE_6_moment__(key * 1000).format('DD/MM/YYYY');
                timeArray.push(key_time);
                stepArray.push(data.val().steps);
                return false;
            });
            _this.step_time = timeArray;
            _this.steps = stepArray;
            _this.barChart_step = new __WEBPACK_IMPORTED_MODULE_5_chart_js__["Chart"](_this.barCanvas_step.nativeElement, {
                type: 'bar',
                options: {
                    legend: {
                        display: false
                    }
                },
                data: {
                    datasets: [
                        {
                            fillColor: "rgba(220,220,220,0.5)",
                            strokeColor: "rgba(220,220,220,1)",
                            data: _this.steps,
                        }
                    ],
                    labels: _this.step_time
                }
            });
            // this.barChart_step = new Chart(ctx, {
            //     type: 'bar',
            //     data: data,
            //     options: options
            // });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('steps_barChart'),
        __metadata("design:type", Object)
    ], StepCounterPage.prototype, "barCanvas_step", void 0);
    StepCounterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-stepCounter',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/stepCounter/stepCounter.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Step Counter</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div id="stepcounter">\n  <ion-card>\n    <ion-card-header>Step Counter</ion-card-header>\n      <ion-item *ngIf="!surgeonCheck">\n        <ion-input placeholder="How many steps have you done today?" [(ngModel)]="step_counter.steps" type="number"></ion-input>\n      </ion-item>\n\n    <button ion-button clear block color="lyellow" (click)="submit_steps()"><b>Submit</b></button>\n  </ion-card>\n  </div>\n\n  <ion-item fixed>\n    <div><canvas #steps_barChart></canvas> </div>\n  </ion-item>\n\n  <!--<div id="rangeofmotion">-->\n    <!--<ion-grid>-->\n      <!--<ion-item-divider>Range of Motion</ion-item-divider>-->\n      <!--<h2>What is your range of motion?(For knee operation only)</h2>-->\n      <!--<ion-row>-->\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/0d.png"><p>0 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/15d.png"><p>15 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/30d.png"><p>30 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n      <!--</ion-row>-->\n\n      <!--<ion-row>-->\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/45d.png"><p>45 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/60d.png"><p>60 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/75d.png"><p>75 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n      <!--</ion-row>-->\n\n      <!--<ion-row>-->\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/90d.png"><p>90 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/105d.png"><p>105 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n\n        <!--<ion-col>-->\n          <!--<ion-item>-->\n            <!--<ion-label><img src="../assets/image/120d.png"><p>120 degree</p></ion-label>-->\n            <!--<ion-checkbox color="linegreen"></ion-checkbox>-->\n          <!--</ion-item>-->\n        <!--</ion-col>-->\n      <!--</ion-row>-->\n\n    <!--</ion-grid>-->\n\n    <!--<button ion-button clear block color="lyellow" (click)="submit_rangeofmotion()"><b>Submit</b></button>-->\n  <!--</div>-->\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/stepCounter/stepCounter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__providers_rest_rest__["a" /* RestProvider */]])
    ], StepCounterPage);
    return StepCounterPage;
}());

//# sourceMappingURL=stepCounter.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loading_loading__ = __webpack_require__(165);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var LoginPage = /** @class */ (function () {
    function LoginPage(afAuth, navCtrl, navParams) {
        var _this = this;
        this.afAuth = afAuth;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
        this.afAuth.auth.onAuthStateChanged(function (user) {
            if (user && user.emailVerified) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__loading_loading__["a" /* LoadingPage */]);
            }
        });
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.doLogin = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log("login" + this.user.email + this.user.password);
                        return [4 /*yield*/, this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password).then(function () {
                                if (_this.afAuth.auth.currentUser.emailVerified) {
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__loading_loading__["a" /* LoadingPage */]);
                                    //this.navCtrl.setRoot(MenuPage);
                                }
                                else {
                                    alert('Email not Verified');
                                }
                            }).catch(function (error) {
                                // Handle Errors here.
                                var errorMessage = error.message;
                                alert(errorMessage);
                                console.log(error);
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.passwordReset = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (user.email == null) {
                            alert("Please enter your Email First");
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.afAuth.auth.sendPasswordResetEmail(user.email).then(function () { return alert("Reset email sent, please check your email"); }).catch(function (error) {
                                var errorMessage = error.message;
                                alert(errorMessage);
                            })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.navCtrl.setRoot(LoginPage_1)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        console.error(e_2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/login/login.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Log In</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding style="background-color: #1b2b68">\n  <br>\n  <h1 align="center" class="iconfont1">OrthoApp</h1>\n  <ion-item style="background-color: transparent">\n    <ion-label floating color="light">Email Address</ion-label>\n    <ion-input type="text" style="color: #f4f4f4" [(ngModel)]="user.email" ></ion-input>\n  </ion-item>\n\n  <ion-item style="background-color: transparent">\n    <ion-label floating color="light">Password</ion-label>\n    <ion-input type="password" style="color: #f4f4f4" [(ngModel)]="user.password"></ion-input>\n  </ion-item>\n\n  <button ion-button block outline color="light" (click)="doLogin(user)"><b>Log-in</b></button>\n  <button ion-button block outline color="light" (click)="register()"><b>Register</b></button>\n  <button ion-button clear block color="light" (click)="passwordReset(user)" icon-left style="font-size: 13px" >\n    <ion-icon name="help-circle"></ion-icon>\n    Forget Password\n  </button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProjects/OrthoAPP_2018_S1/Ortho/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[443]);
//# sourceMappingURL=main.js.map