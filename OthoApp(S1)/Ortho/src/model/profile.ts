export interface Profile{
  firstName : string;
  lastName  : string;
  Name      : string;
  // gender    : boolean;
  Birthday  : any;
  SurgeryDay: any;
  Status    : boolean;
  Surgeon   : string;
  operationType : string;
  operationSide : string;
  PrimaryOrRevision : string;
}
