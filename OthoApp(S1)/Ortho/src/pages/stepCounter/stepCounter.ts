import {Component, ViewChild} from '@angular/core';
import { NavController } from 'ionic-angular';
import {StepCounter} from "../../model/stepCounter";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {RestProvider} from "../../providers/rest/rest";
import {Chart} from "chart.js";
import * as moment from 'moment';
import {auth} from "firebase/app";
import {RangeMotion} from "../../model/rangeMotion";


@Component({
  selector: 'page-stepCounter',
  templateUrl: 'stepCounter.html'
})
export class StepCounterPage {
    @ViewChild('steps_barChart') barCanvas_step;
    barChart_step: any;
    step_time: any;
    steps: any;
    user_step:any;


    step_counter = {} as StepCounter;
    today: number;
    rangeofmotion = {} as RangeMotion;

    constructor(public navCtrl: NavController, private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public rest: RestProvider) {
        let dateTime = new Date();
        dateTime.setHours(0);
        dateTime.setMinutes(0);
        dateTime.setSeconds(0);
        dateTime.setMilliseconds(0);
        const timestamp = Math.floor(dateTime.getTime() / 1000);
        this.today = timestamp;
    }

    submit_steps() {
        this.afAuth.authState.take(1).subscribe(auth => {
            const date = this.today;
            this.afDatabase.object(`Steps/${auth.uid}/${date}`).update(this.step_counter);
        });
    }

    submit_rangeofmotion() {
        this.afAuth.authState.take(1).subscribe(auth =>{
            const date = this.today;
            this.afDatabase.object(`RangeofMotion/${auth.uid}/${date}`).set(this.rangeofmotion).then(()=>alert("uploaded"));
        })
    }


    ionViewDidLoad() {

        let userID = this.afAuth.auth.currentUser.uid;
        this.user_step = this.afDatabase.database.ref(`Steps/${userID}`);

        this.user_step.on('value', data => {
            let timeArray = [];
            let stepArray = [];
            data.forEach(data => {
                let key = data.key;
                let key_time = moment(key * 1000).format('DD/MM/YYYY');
                timeArray.push(key_time);
                stepArray.push(data.val().steps);
                return false;
            });
            this.step_time = timeArray;
            this.steps = stepArray;
            this.barChart_step = new Chart(this.barCanvas_step.nativeElement,{
                type: 'bar',
                options: {
                    legend: {
                        display: false
                    }
                },
                data: {
                    datasets: [
                        {
                            fillColor : "rgba(220,220,220,0.5)",
                            strokeColor : "rgba(220,220,220,1)",
                            data: this.steps,
                        }
                    ],
                    labels: this.step_time
                }
            });


            // this.barChart_step = new Chart(ctx, {
            //     type: 'bar',
            //     data: data,
            //     options: options
            // });


        });


    }
}

