import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeforeSurgeryPage } from './before-surgery';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    BeforeSurgeryPage
  ],
  imports: [
    IonicPageModule.forChild(BeforeSurgeryPage),
    PipesModule
  ],
})
export class BeforeSurgeryPageModule {}
