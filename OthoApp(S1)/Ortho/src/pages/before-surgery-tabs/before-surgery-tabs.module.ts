import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeforeSurgeryTabsPage } from './before-surgery-tabs';

@NgModule({
  declarations: [
    BeforeSurgeryTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(BeforeSurgeryTabsPage),
  ]
})
export class BeforeSurgeryTabsPageModule {}
