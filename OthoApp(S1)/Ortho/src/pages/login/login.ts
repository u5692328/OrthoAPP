import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from "../../model/user";
import {AngularFireAuth} from "angularfire2/auth";
import {RegisterPage} from "../register/register";
// import {ProfilePage} from "../profile/profile";
import {FirebaseObjectObservable} from "angularfire2/database";
import {Profile} from "../../model/profile";
import {LoadingPage} from "../loading/loading";



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;
  profile : FirebaseObjectObservable<Profile>;


  constructor(private afAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams ) {

          this.afAuth.auth.onAuthStateChanged(user => {
              if (user && user.emailVerified) {
                  this.navCtrl.push(LoadingPage);
              }
          });

  }

    async doLogin(user: User) {
      try {
        console.log("login"+this.user.email+this.user.password);

        await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password).then(()=>{
            if (this.afAuth.auth.currentUser.emailVerified) {
                    this.navCtrl.push(LoadingPage);
                    //this.navCtrl.setRoot(MenuPage);
            }
            else {
                alert('Email not Verified');
            }
        }).catch((error)=> {
          // Handle Errors here.
          let errorMessage = error.message;
          alert(errorMessage);
          console.log(error);
        })
      } catch (e) {
        console.log(e);
      }
    }

    async passwordReset(user: User){
        try{
            if (user.email == null){
                alert("Please enter your Email First");
                return;
            }

            await this.afAuth.auth.sendPasswordResetEmail(user.email).then(()=>alert("Reset email sent, please check your email")).catch((error)=>{
                let errorMessage = error.message;
                alert(errorMessage);
            });
            await this.navCtrl.setRoot(LoginPage);
        }catch(e){
            console.error(e);
        }
    }



  register() {
    this.navCtrl.push(RegisterPage);
  }
}

