import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AfterSurgeryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-after-surgery',
  templateUrl: 'after-surgery.html',
})
export class AfterSurgeryPage {

    videos_post: any[] = [

        {
            title: 'Post operative Hip Exercises',
            video: 'https://www.youtube.com/embed/UBPyDzvqTV8?rel=0',
        },
        {
            title: 'Post operative Knee Exercises',
            video: 'https://www.youtube.com/embed/D3YDXM0SQjI?rel=0',
        }

    ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AfterSurgeryPage');
  }

}
