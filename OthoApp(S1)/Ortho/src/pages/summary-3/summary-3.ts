import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-summary-3',
    templateUrl: 'summary-3.html',
})
export class Summary3Page {
    infoId = null;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.infoId = this.navParams.get('infoId');
    }

    goBack() {
        this.navCtrl.pop();
    }

}