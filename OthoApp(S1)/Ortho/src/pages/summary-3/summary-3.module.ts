import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {Summary3Page} from "./summary-3";

@NgModule({
    declarations: [
        Summary3Page,
    ],
    imports: [
        IonicPageModule.forChild(Summary3Page),
    ],
})
export class Summary3Module {}
