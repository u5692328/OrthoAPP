import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InHospitalPage } from './in-hospital';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    InHospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(InHospitalPage),
    PipesModule
  ],
})
export class InHospitalPageModule {}
