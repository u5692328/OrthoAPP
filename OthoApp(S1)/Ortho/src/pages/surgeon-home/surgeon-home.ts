import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AngularFireDatabase, FirebaseObjectObservable} from "angularfire2/database";
import {Profile} from "../../model/profile";
import {AngularFireAuth} from "angularfire2/auth";
import {ContactPage} from "../painLevel/contact";
import {LoginPage} from "../login/login";


@IonicPage()
@Component({
  selector: 'page-surgeon-home',
  templateUrl: 'surgeon-home.html',
})
export class SurgeonHomePage {
    public userRef: any;
    public People:Array<any>;
    public UID: any;
    public loadedPatientList:Array<any>;
    profileRef$: FirebaseObjectObservable<Profile>;
    subscribeData : any;

  constructor(public app:App,private afAuth: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams,public afDatabase: AngularFireDatabase) {

      let currentUser =afAuth.auth.currentUser;
      let name;

      this.userRef = afDatabase.database.ref(`profile/user/`);
      this.profileRef$ = this.afDatabase.object(`profile/user/${currentUser.uid}`);

      this.subscribeData = this.profileRef$.subscribe(data=>{
          name = data.Name;
          this.userRef.on('value', People => {
              let patientArray = [];
              People.forEach( user => {
                  if(user.val().Surgeon == name) {
                      patientArray.push(user.val());
                      return false;
                  }

              });
              this.People = patientArray;
              this.loadedPatientList = patientArray;
          });
      }
  )

  }

    initializeItems(): void {
        this.People = this.loadedPatientList;
    }

  showProfile(patient){

      this.userRef.on('value', People => {
          let Id;
          People.forEach( user => {
              if(user.val().firstName == patient.firstName && user.val().lastName == patient.lastName && user.val().Birthday == patient.Birthday) {
                  let key = user.key;
                  Id = key;
                  return false;
              }

          });
          this.UID = Id;
          this.navCtrl.push(ContactPage,{
              userid : this.UID
          });

      });

  }

    getItems(searchbar) {
        // Reset items back to all of the items
        this.initializeItems();

        // set q to the value of the searchbar
        var q = searchbar.srcElement.value;


        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }

        this.People = this.People.filter((v) => {
            if(q) {
                if (v.Name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }

        });

        console.log(q, this.People.length);

    }
    ngOnDestroy() {
        this.subscribeData.unsubscribe();
    }
    logout(){
        this.afAuth.auth.signOut().then(()=>this.app.getRootNav().setRoot(LoginPage));
    }
}
