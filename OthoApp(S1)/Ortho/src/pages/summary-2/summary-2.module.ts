import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Summary2Page} from "./summary-2";

@NgModule({
  declarations: [
    Summary2Page,
  ],
  imports: [
    IonicPageModule.forChild(Summary2Page),
  ],
})
export class Summary2Module {}
