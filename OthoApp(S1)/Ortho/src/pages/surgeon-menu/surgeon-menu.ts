import {Component, ViewChild} from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {SurgeonHomePage} from "../surgeon-home/surgeon-home";
import {SurgeonHomeTabsPage} from "../surgeon-home-tabs/surgeon-home-tabs";

export interface PageInterface{
    title: string;
    pageName?: any;
    tabComponent?:any;
    index?: number;
    icon: string;

}
@IonicPage()
@Component({
  selector: 'page-surgeon-menu',
  templateUrl: 'surgeon-menu.html',
})
export class SurgeonMenuPage {

    rootPage = SurgeonHomeTabsPage;

    @ViewChild(Nav) nav: Nav;

    pages: PageInterface[] =[
        {title: 'Home' , pageName: SurgeonHomeTabsPage,tabComponent:'SurgeonHomePage',icon:'home'},
    ];

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    openPage(page: PageInterface) {
        let params = {};
        this.nav.setRoot(page.pageName, params);
    }
    isActive(page: PageInterface) {
        let childNav = this.nav.getActiveChildNav();

        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }

        if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
            return 'primary';
        }
        return;
    }

}
