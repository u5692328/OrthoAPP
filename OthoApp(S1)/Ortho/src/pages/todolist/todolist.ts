import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {Todos} from "../../model/todos";
import {AngularFireDatabase, FirebaseObjectObservable} from "angularfire2/database";
//import {MenuPage} from "../menu/menu";
//import 'rxjs/add/operator/take';
//import {Profile} from "../../model/profile";
//import {SurgeonMenuPage} from "../surgeon-menu/surgeon-menu";




@IonicPage()
@Component({
  selector: 'page-todo',
  templateUrl: 'todolist.html',
})
export class TodoPage {
    todolist = {} as Todos;
    listData : any;
    subscribeData:any;
    userID : any;


    constructor(private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {

    }

    createTODO() {

        this.afAuth.authState.take(1).subscribe(auth =>{

            this.afDatabase.object(`TodoChecklist/${auth.uid}`).set(this.todolist).then(()=>alert("uploaded"));
        })

    }
    ngOnDestroy() {
        this.subscribeData.unsubscribe();
    }
    ionViewDidLoad() {
        this.afAuth.authState.take(1).subscribe(auth =>{

            this.listData  = this.afDatabase.object(`TodoChecklist/${auth.uid}`);
            this.subscribeData = this.listData.subscribe(data => {
                this.todolist = data;
            })
        })
    }


}
