import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavSummaryDetailsPage } from './nav-summary-details';

@NgModule({
  declarations: [
    NavSummaryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(NavSummaryDetailsPage),
  ],
})
export class NavSummaryDetailsModule {}
